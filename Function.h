#pragma once
#include <array>
#include <cereal/cereal.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/types/array.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/vector.hpp>
#include <Eigen\Core>
#include <Eigen\Geometry>
#include <fstream>
#include <iostream>
#include <vector>

/*! @brief 極座標から直行座標に変換する関数
@param[in] Thetha 角度θ
@param[in] Phi 角度φ
@return std::array<double, 3> Cartesian
*/
std::array<double, 3> Polar2Cartesian(const double Thetha, const double Phi);

/*! @brief シグモイド関数
@param[in] x 入力x
@return double y
*/
double Sigmoid(const double x);

/*! @brief 線形関数
@param[in] x 入力x
@return double y
*/
double Linear(const double x);

/*! @brief 一番短い辺の長さを求める
@param[in] Sides 三辺の長さ
@return double MinSideLength
*/
double GetMinSideLength(std::array<double, 3> Sides);

/*! @brief 3次元上の2点間の距離を求める
@param[in] P1 点1の座標
@param[in] P2 点2の座標
@return double Distance
*/
double Distance3D(std::array<double, 3>P1, std::array<double, 3>P2);

/*! @brief ブール値を反転
@param[in] BoolValue ブール値
@return double FlipedValue
*/
bool FlipBoolValue(bool BoolValue);

/*! @brief 直方体同士が衝突しているかどうかを判定
@param[in] Box1Position 直方体1の位置
@param[in] Box2Position 直方体2の位置
@param[in] Box1Sides 直方体1の3辺の長さ
@param[in] Box2Sides 直方体2の3辺の長さ
@param[in] Box1AttitudeAngle 直方体1の姿勢角
@param[in] Box2AttitudeAngle 直方体2の姿勢角
@return bool AreTwoBoxesColliding
@details 2つの直方体が衝突していればtrueを、そうでなければfalseを返す
*/
bool VerifyCollisionOfBoxes(const std::array<double, 3> Box1Position, const std::array<double, 3>Box2Position,
	const std::array<double, 3> Box1Sides, const std::array<double, 3> Box2Sides,
	const std::array<double, 3> Box1AttitudeAngle, const std::array<double, 3> Box2AttitudeAngle);

/*! @brief 与えられた値が範囲外かどうか
@param[in] Value 値
@param[in] Min 値の下限
@param[in] Max 値の上限
@return T Value
@details 与えられた値が範囲外の場合例外が投げられ強制終了する
*/
template < typename  T >
T IsValueValid(const T Value, const T Min, const T Max) {
	try {
		if (Value < Min || Value > Max) {
			throw std::out_of_range("Given value is out of range");
		}
	}
	catch (std::out_of_range &ValueError) {
		std::cerr << ValueError.what() << std::endl;
		system("pause");
		exit(EXIT_FAILURE);
	}

	return Value;
}

/*! @brief 与えられた値が範囲外かどうか
@param[in] Value 値
@param[in] Min 値の下限
@return T Value
@details 与えられた値が範囲外の場合例外が投げられ強制終了する
*/
template < typename  T >
T IsValueValid(const T Value, const T Min) {
	try {
		if (Value < Min) {
			throw std::out_of_range("Given value is out of range");
		}
	}
	catch (std::out_of_range &ValueError) {
		std::cerr << ValueError.what() << std::endl;
		system("pause");
		exit(EXIT_FAILURE);
	}

	return Value;
}

/*! @brief 与えられた配列の値が範囲外かどうか
@param[in] Array 配列
@param[in] Min 配列の値の下限
@param[in] Max 配列の値の上限
@return std::array<T, N> Array
@details 与えられた配列の値が範囲外の場合例外が投げられ強制終了する
*/
template < typename  T, int N >
std::array<T, N> IsArrayValueValid(const std::array<T, N> Array, const T Min, const T Max) {
	for (auto a : Array) {
		IsValueValid(a, Min, Max);
	}

	return Array;
}

/*! @brief 与えられた配列の値が範囲外かどうか
@param[in] Array 配列
@param[in] Min 配列の値の下限
@return std::array<T, N> Array
@details 与えられた配列の値が範囲外の場合例外が投げられ強制終了する
*/
template < typename  T, int N >
std::array<T, N> IsArrayValueValid(const std::array<T, N> Array, const T Min) {
	for (auto a : Array) {
		IsValueValid(a, Min);
	}

	return Array;
}

/*! @brief ベクトルの要素を削除する
@param[in] Vector ベクトル
@param[in] Index 削除したい要素のインデックス
*/
template < typename  T >
void RemoveVectorElement(std::vector<T>& Vector, const unsigned int Index) {
	Vector.erase(Vector.begin() + Index);
}

/*! @brief ベクトルが空かどうか判定
@param[in] Vector ベクトル
@return std::vector<T> Vector
@details ベクトルが空の場合例外が投げられ強制終了する
*/
template < typename  T >
std::vector<T> IsVectorEmpty(std::vector<T> Vector) {
	try {
		if (Vector.empty()) {
			throw std::exception("Given vector is empty");
		}
	}
	catch (std::exception &Exception) {
		std::cerr << Exception.what() << std::endl;
		system("pause");
		exit(EXIT_FAILURE);
	}

	return Vector;
}

/*! @brief ベクトルの大きさが与えた大きさと一致しているか
@param[in] Vector ベクトル
@param[in] Size 満たして欲しいベクトルの大きさ
@return std::vector<T> Vector
@details ベクトルの大きさが与えた大きさと一致しない場合例外が投げられ強制終了する
*/
template < typename  T >
std::vector<T> IsVectorSizeValid(std::vector<T> Vector, unsigned int Size) {
	try {
		if (Vector.size() != Size) {
			throw std::exception("Given vector' size is invalid");
		}
	}
	catch (std::exception &Exception) {
		std::cerr << Exception.what() << std::endl;
		system("pause");
		exit(EXIT_FAILURE);
	}

	return Vector;
}

/*! @brief 2つのベクトルを結合する
@param[in] Vector1 ベクトル1
@param[in] Vector2 ベクトル2
@return std::vector<T> ConcatenatedVector
*/
template < typename  T >
std::vector<T> ConcatVector(std::vector<T> Vector1, std::vector<T> Vector2) {
	Vector1.insert(Vector1.end(), Vector2.begin(), Vector2.end());

	return Vector1;
}

/*! @brief 与えた値を範囲内に収める
@param[in] Value 値
@param[in] Min 値の下限
@param[in] Max 値の上限
@return T AdjustedValue
*/
template < typename  T >
T AdjustValue(T Value, const T Min, const T Max) {
	if (Value > Max) Value = Max;
	else if (Value < Min) Value = Min;

	return Value;
}

/*! @brief 与えた値を範囲内に収める
@param[in] Value 値
@param[in] Min 値の下限
@return T AdjustedValue
*/
template < typename  T >
T AdjustValue(T Value, const T Min) {
	if (Value < Min) Value = Min;

	return Value;
}

/*! @brief ベクトル内の値の合計値を求める
@param[in] Vector ベクトル
@return T Sum
*/
template < typename  T >
T SumVector(const std::vector<T> Vector) {
	T Sum = 0;
	for (auto v : Vector) {
		Sum += v;
	}

	return Sum;
}

//https://qiita.com/Egliss/items/cc281f724ebe7d05ff67
//https://qiita.com/Ushio/items/827cf026dcf74328efb7
/*! @brief クラスをシリアライズする関数
@param[in] Target シリアライズしたいクラス
@param[out] Path シリアライズを保存したい場所
@details 対象のクラスをJSON形式で任意の場所に保存する
*/
template < typename  T >
void Serialize(const T Target, const std::string Path) {
	//出力用のストリーム
	std::stringstream Stream;
	{
		//出力オプション
		cereal::JSONOutputArchive::Options Option(100, cereal::JSONOutputArchive::Options::IndentChar::space, 4);

		//出力用の型にストリームを登録
		cereal::JSONOutputArchive JsonOutArchive(Stream, Option);

		//ストリームにjsonを書き込み
		JsonOutArchive(cereal::make_nvp(string(typeid(T).name()).replace(0, 6, ""), Target));
	}

	//ファイル出力用ストリーム作成
	std::ofstream OutFile(Path, std::ios::out);

	//書き出す
	OutFile << Stream.str();

	//閉じる
	OutFile.close();
	Stream.clear();
}

//https://qiita.com/Egliss/items/cc281f724ebe7d05ff67
/*! @brief クラスをデシリアライズする関数
@param[in] Target デシリアライズしたいクラス
@param[in] Path シリアライズされたファイルが保存されている場所
@details JSON形式でシリアライズされた対象のクラスを読み込み復元する
*/
template < typename  T >
void Deserialize(T& Target, const std::string Path) {
	//入力用のストリーム
	std::stringstream Stream;

	//ファイル入力用ストリーム作成
	std::ifstream InFile(Path, std::ios::in);

	//入力データをストリームで受け取る
	Stream << InFile.rdbuf();

	//jsonをロード
	cereal::JSONInputArchive JsonInArchive(Stream);

	//デコードしたデータをインスタンスにセット
	JsonInArchive(cereal::make_nvp(string(typeid(T).name()).replace(0, 6, ""), Target));

	//閉じる
	InFile.close();
	Stream.clear();
}