#include "Parameter.h"

//ノード
const double MinSide = 0.2;
const double MaxSide = 1.0;
const unsigned int MaxRecursiveLimit = 5;

//コネクション
const double MinScale = 0.5;
const double MaxScale = 2.0;

//ニューラルネットワーク
const double BeamSensorLength = 1.0;
const unsigned int NeuralNetworkCycle = 1;
//const unsigned int HeadNodeSensorType = SensorType::Joint | SensorType::Touch | SensorType::Sound | SensorType::Eye;
//const unsigned int NormalNodeSensorType = SensorType::Joint | SensorType::Touch | SensorType::Sound | SensorType::Eye;

const unsigned int HeadNodeSensorType = SensorType::Joint | SensorType::Sound ;
const unsigned int NormalNodeSensorType = SensorType::Joint | SensorType::Sound;

//遺伝的アルゴリズム
const int RandomSeed = 0;
const unsigned int Population = 100;
const unsigned int Generation = 1000;
const bool OnlyNeuralNetworkEvolves = true;
const double CrossOverRate = 0.7;
const double MutationRate = 0.01;
const unsigned int EvaluationTime = 3000;

//物理エンジン
//物理エンジンのパラメータをここにまとめて書く