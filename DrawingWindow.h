#pragma once

#include <cstdlib>
#include <GLFW\glfw3.h>
#include <GL\freeglut.h>
#include <iostream>
#include <ode\ode.h>
#include "Camera.h"
#include <vector>
#include <tuple>
#include <unordered_map>
#include "Parameter.h"

using namespace std;

class DrawingWindow 
{
private:
	//光源（環境光，拡散光，鏡面光，位置）
	GLfloat lightAmb[4] = { 1.f, 1.f, 1.f, 1.f };
	GLfloat lightDiff[4] = { 1.f, 1.f, 1.f, 1.f };
	GLfloat lightSpec[4] = { 1.f, 1.f, 1.f, 1.f };
	//GLfloat lightPos[4] = { 0.5f, 0.5f, 0.5f, 1.f };//平行光源
	GLfloat lightPos[4] = { -20, -20, 100, 1.0 }; //光源の位置

	GLFWwindow* Window;
	Camera Camera1;

	enum MouseButton {
		LeftButton = 0,  //マウスの左ボタンに関するアクション（押されたかor離された）がされた
		RightButton = 1, //マウスの右ボタンに関するアクション（押されたかor離された）がされた
		MiddleButton = 2 //マウスの中央ボタンに関するアクション（押されたかor離された）がされた
	};

	enum MouseAction {
		MouseUp = 0,  //マウスボタンが離された
		MouseDown = 1 //マウスボタンが押された
	};
	enum MouseButtonState
	{
		IsNotDragging = 0,         // マウスはドラッグしていない
		LeftButtonIsDragging = 1,  // マウスの左ボタンでドラッグしている
		RightButtonIsDragging = 2, // マウスの右ボタンでドラッグしている
		MiddleButtonIsDragging = 3,// マウスの中央ボタンでドラッグしている
		MouseWheelIsRotating = 4   // マウスホイールが回転している 
	};

	// 描画ウィンドウを作る際，glfwCreateWindowのサイズの引数はint型なので，
	// WidthとHeightはint型を採用．一方，ウィンドウの中心は，マウスでカメラの
	// の方向を調節するときに用いる．マウスカーソルの座標はdouble型で取得されるので，
	// それに合わせて，WindowCenterはdouble型を採用．
	int Width, Height;            //ウィンドウの横幅と縦幅
	std::array<double, 2> WindowCenter;// ウィンドウ座標系でのウィンドウの中心座標(x,y)



	int GetWidth() { return Width; }
	int GetHeight() { return Height; }

	void DisplayInit();
	void Display();
	void SetColor(float Red, float Green, float Blue, float Alpha);
	void DrawGround();//地面の描画
	void DrawAxes();//ワールド座標系の原点にワールド座標系の座標軸を配置
	void DrawSounds(const vector<array<double, 4> >Sounds);//音源に音量に比例した線分を描画
	void DrawBeamSensor(vector<vector<dGeomID> > AgentsBeamSensorGeomIDs, unordered_map<dGeomID, double> HitBeamSensorValues);//ビームセンサーの描画
	void DrawAgents(const vector<vector<dGeomID> > AgentsGeomIDs);//エージェントの描画
	void DrawBox(dVector3 Sides, const dReal Attitude[], const dReal Position[]);//直方体の描画
	void DrawLine(const std::array<double, 3> &StartingPoint,
		const std::array<double, 3> &EndingPoint, const std::array<float, 4> &Color,
		double Width = 3);//線分の描画
	void ConvertOdeRotationToOpenGlRotation(const dReal OdeRotation[], double OpenGlRotation[]);//ODEの回転行列をOpenGLの回転行列に変換

	//コールバック関数は静的なメンバ関数でないとエラーが出る
	static void Reshape(GLFWwindow* Window, int NewWidth, int NewHeight);
	static void MouseClick(GLFWwindow* Window, int Button, int Action, int Mods);
	static void MouseCusor(GLFWwindow* Window, double Xcoordinate, double Ycoordinate);
	static void MouseWheel(GLFWwindow* Window, double Xoffset, double Yoffset);
	static void Keyboard(GLFWwindow* Window, int Key, int Scancode, int Action, int Mods);

public:
	//コンストラクタ
	DrawingWindow(int argc, char *argv[]);
	~DrawingWindow();

	void Draw(const vector<vector<dGeomID> > AgentsGeomIDs, const vector<array<double, 4> >Sounds,
		vector<vector<dGeomID> > AgentsBeamSensorGeomIDs, unordered_map<dGeomID, double> HitBeamSensorValues);//仮想物理環境の描画

	GLFWwindow* GetGlFWwindow() { return Window; }
};

