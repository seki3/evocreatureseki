#pragma once
#include "Connection.h"
#include "NeuralNetwork.h"

/*! @brief ノードの基底クラス
@details ノードに共通する処理や情報を持つ。\n
ここからノーマルノードとヘッドノードを作成する。\n
このクラス単体で使用することは無い。
*/
class BaseNode {
public:
	/*! @brief 通常のコンストラクタ
	@param[in] ST センサータイプ
	@details センサータイプを受け取りそれに対応したニューラルネットワークを作成する。\n
	辺の長さと視覚センサーの方向はランダムに決定される
	*/
	BaseNode(const unsigned int ST);

	/*! @brief 任意のベースノードを作成するコンストラクタ
	@param[in] Sides ノードの辺の長さ
	@param[in] EyeDirection 視覚センサーの方向
	@param[in] ST センサータイプ
	@param[in] NN ニューラルネットワーク
	@param[out] ConnectionVector コネクションの配列
	@details ベースノードの持つ全ての情報を与えてベースノードを作成する。\n
	範囲外の値があった場合は例外が発生し強制終了する。
	*/
	BaseNode(const std::array<double, 3> Sides, const std::array<double, 3> EyeDirection,
		const unsigned int ST, const NeuralNetwork NN, const std::vector<Connection> ConnectionVector);

	/*! @brief デストラクタ */
	virtual ~BaseNode();

	/*! @brief 辺の長さを返す
	@return std::array<double, 3> Sides*/
	std::array<double, 3> GetSides() { return Sides; }

	/*! @brief 視覚センサーの方向を返す
	@return std::array<double, 3> EyeDirection*/
	std::array<double, 3> GetEyeDirection() { return EyeDirection; }

	/*! @brief センサータイプを返す
	@return unsigned int ST*/
	unsigned int GetSensorType() { return ST; }

	/*! @brief ニューラルネットワークを返す
	@return NeuralNetwork NN*/
	NeuralNetwork GetNeuralNetwork() { return NN; }

	/*! @brief コネクションの配列を返す
	@return std::vector<Connection> ConnectionVector*/
	std::vector<Connection> GetConnectionVector() { return ConnectionVector; }

	/*! @brief ベースノードの持つ各情報に対して突然変異を発生させる
	@param[in] NormalNodeNumber 遺伝子型の持つノーマルノードの数*/
	virtual void Mutation(const unsigned int NormalNodeNumber);

	/*! @brief コネクションのインデックスを調整する
	@param [in] DeletedIndex 削除されたノーマルノードのインデックス
	@details ノーマルノードが削除された際にそのノードより大きいインデックスを持つ\n
	コネクションは全てインデックスを１つ前にずらす必要がある。（参照がずれないように）\n
	この関数では調節が必要なコネクションにインデックスを１つ減らす処理を行う。\n
	*/
	void AdjustConnectionIndex(const unsigned int DeletedIndex);

private:
	/*! @brief 突然変異によってコネクションを1つ追加
	@param[in] NormalNodeNumber 遺伝子型の持つノーマルノードの数
	@details 突然変異が発生してかつノーマルノードが存在している場合、\n
	ランダムに接続したコネクションを1つ追加する。
	*/
	void AddConnection(const unsigned int NormalNodeNumber);

	/*! @brief 突然変異によってコネクションを1つ削除
	@details 突然変異が発生した場合、今保持しているコネクションからランダムに1つ選び削除する。
	*/
	void DeleteConnection();

	/*! @brief 保持しているコネクションに突然変異を加える*/
	void MutateConnections();

	//! ノードの辺の長さ
	std::array<double, 3> Sides;

	//! ノードの持つ視覚センサーの方向
	std::array<double, 3> EyeDirection;

	//! センサーのタイプ
	unsigned int ST;

	//! ニューラルネットワーク
	NeuralNetwork NN;

	//! ノードの持つコネクションの配列
	std::vector<Connection> ConnectionVector;

	friend class cereal::access;
	/*! @brief シリアライズ
	@details ベースノードの持つ全ての情報をシリアライズする関数*/
	template<class Archive>
	void serialize(Archive& archive) {
		archive(CEREAL_NVP(Sides),
			CEREAL_NVP(EyeDirection),
			cereal::make_nvp("SensorType", ST),
			cereal::make_nvp("NeuralNetwork", NN),
			CEREAL_NVP(ConnectionVector));
	}

};