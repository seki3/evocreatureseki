#include "NeuralNetwork.h"

//コンストラクタ
NeuralNetwork::NeuralNetwork(const unsigned int ST) {
	MakeSensorOnOff(ST); //センサーのオンオフをランダムに決定
	MakeSensorNeurons(ST); //センサーニューロンを作成
	MakeEffectorNeurons(); //エフェクターニューロンを作成
}

NeuralNetwork::NeuralNetwork(const std::map<unsigned int, bool> SensorOnOff,
	const std::vector<SensorNeuron> SensorNeuronVector, const std::vector<NormalNeuron> NormalNeuronVector) {
	//メンバ変数を初期化
	this->SensorOnOff = SensorOnOff;
	this->SensorNeuronVector = SensorNeuronVector;
	this->NormalNeuronVector = NormalNeuronVector;

	MakeEffectorNeurons();
}

//デストラクタ
NeuralNetwork::~NeuralNetwork() {
}

//伝播
void NeuralNetwork::Propagation(const std::map<unsigned int, std::vector<double>> SensorValues) {
	PropagateSensorValues(SensorValues); //センサーの値を伝播
	PropagateNeuronVector(SensorNeuronVector); //センサーニューロンの値を伝播
	PropagateNeuronVector(NormalNeuronVector); //ノーマルニューロンの値を伝播
}

//発火
void NeuralNetwork::Fire() {
	for (auto& sn : SensorNeuronVector) {
		sn.Fire(); //センサーニューロンを発火
	}

	for (auto& nn : NormalNeuronVector) {
		nn.Fire(); //ノーマルニューロンを発火
	}

	for (auto& en : EffectorNeuronVector) {
		en.Fire(); //エフェクターニューロンを発火
	}
}

//突然変異
void NeuralNetwork::Mutation() {
	//センサーのオンオフを突然変異
	MutateSensorOnOff();

	//突然変異によってノーマルニューロンを1つ削除
	DeleteNormalNeuron();

	//突然変異によってノーマルニューロンを1つ追加
	AddNormalNeuron();

	//ノーマルニューロンに突然変異を加える
	for (auto& nn : NormalNeuronVector) {
		nn.Mutation(NormalNeuronVector.size());
	}

	//センサーニューロンに突然変異を加える
	for (auto& sn : SensorNeuronVector) {
		sn.Mutation(NormalNeuronVector.size());
	}
}

//センサーのオンオフをランダムに決定
void NeuralNetwork::MakeSensorOnOff(const unsigned int ST) {
	RandomOperation& RO = GetRandomOperation();
	//各センサータイプ毎に処理
	for (auto itr = SensorNeuronNumber.begin(); itr != SensorNeuronNumber.end(); itr++) {
		unsigned int Type = itr->first; //センサータイプ

		//もし該当のセンサーがあるならばそのセンサーのオンオフを作成
		if ((Type & ST) != 0) {
			SensorOnOff[Type] = RO.MakeRandomBoolValue();
		}
	}
}

//センサーニューロンを作成
void NeuralNetwork::MakeSensorNeurons(const unsigned int ST) {
	//各センサータイプ毎に処理
	for (auto itr = SensorNeuronNumber.begin(); itr != SensorNeuronNumber.end(); itr++) {
		unsigned int Type = itr->first; //センサータイプ
		unsigned int Number = itr->second; //センサーのニューロン数

		//もし該当のセンサーがあるならばそのセンサーニューロンを作成
		if ((Type & ST) != 0) AddSensorNeuron(Number);
	}

	//親コンポーネントと子コンポーネントから情報を受取るニューロンを作成
	AddSensorNeuron(2);
}

//センサーニューロンを追加
void NeuralNetwork::AddSensorNeuron(const unsigned int Number) {
	for (unsigned int i = 0; i < Number; i++) {
		SensorNeuronVector.push_back(SensorNeuron());
	}
}

//エフェクターニューロンを作成
void NeuralNetwork::MakeEffectorNeurons() {
	//3つのエフェクターニューロンを作成
	//ジョイントにかかるトルクを出力するニューロンと親コンポーネントと子コンポーネントへ情報を送るニューロン
	for (int i = 0; i < 3; i++) {
		EffectorNeuronVector.push_back(EffectorNeuron());
	}
}

//センサーの値を伝播
void NeuralNetwork::PropagateSensorValues(const std::map<unsigned int, std::vector<double>> SensorValues) {
	auto SensorNeuronIterator = SensorNeuronVector.begin(); //センサーニューロンベクトルのイテレータ

	//有効なセンサー毎に処理を行う
	for (auto itr = SensorOnOff.begin(); itr != SensorOnOff.end(); itr++) {
		unsigned int Type = itr->first; //センサータイプ
		bool OnOff = itr->second; //センサーのオンオフ

		std::vector<double> Values = SensorValues.at(Type); //センサーからの値を取り出す

		//センサーがオフならそのセンサーからの値は全て0.0にする
		if (!OnOff) {
			for (auto& v : Values) {
				v = 0.0;
			}
		}

		//センサーからの値をセンサーニューロンにセットする
		for (auto v : Values) {
			SensorNeuronIterator->Set(v);
			SensorNeuronIterator++;
		}
	}

	//親コンポーネントと子コンポーネントからの値を取り出す(センサータイプは0)
	std::vector<double> Values = SensorValues.at(0);

	//値をセンサーニューロンにセットする
	for (auto v : Values) {
		SensorNeuronIterator->Set(v);
		SensorNeuronIterator++;
	}

}

//ニューロンベクトルを伝播
template <typename T> void NeuralNetwork::PropagateNeuronVector(std::vector<T> NeuronVector) {
	//ニューロン毎に処理を行う
	for (auto n : NeuronVector) {
		std::vector<NeuroConnection> NeuroConnectionVector = n.GetNeuroConnectionVector(); //ニューロコネクションベクトルを取り出す
		double Output = n.GetOutput(); //ニューロンの出力を取り出す

		//ニューロコネクション毎に処理を行う
		for (auto nc : NeuroConnectionVector) {
			int NeuroNumber = nc.GetNeuroNumber(); //ニューロン番号を取り出す
			double Weight = nc.GetWeight(); //重みを取り出す

			//正の時はノーマルニューロンベクトルのインデックス
			if (NeuroNumber >= 0) {
				//ニューロン番号が正しくノーマルニューロンを指し示している必要がある
				if (NeuroNumber < static_cast<int>(NormalNeuronVector.size())) {
					NormalNeuronVector[NeuroNumber].Set(Weight * Output); //出力値に重みを掛けた値をニューロンにセットする
				}
			}
			//負の時はエフェクターニューロンベクトルのインデックス
			else {
				EffectorNeuronVector[abs(NeuroNumber) - 1].Set(Weight * Output); //出力値に重みを掛けた値をニューロンにセットする
			}
		}
	}
}

//突然変異によってノーマルニューロンを1つ追加
void NeuralNetwork::AddNormalNeuron() {
	//突然変異が発生した場合
	RandomOperation& RO = GetRandomOperation();
	if (RO.IsMutationOccurred()) {
		//ノーマルニューロンを1つ追加
		NormalNeuronVector.push_back(NormalNeuron());
	}
}

//突然変異によってノーマルニューロンを1つ削除
void NeuralNetwork::DeleteNormalNeuron() {
	//突然変異が発生してかつノーマルニューロンがあった場合
	RandomOperation& RO = GetRandomOperation();
	if (RO.IsMutationOccurred() && !NormalNeuronVector.empty()) {
		//今あるノーマルニューロンの中からランダムに1つ選び削除する
		unsigned int Index = RO.MakeRandomValue(0, NormalNeuronVector.size() - 1);
		RemoveVectorElement(NormalNeuronVector, Index);

		//ノーマルニューロンが持つニューロコネクションのニューロン番号を調整する
		for (auto& nn : NormalNeuronVector) {
			nn.AdjustNeuroNumber(Index);
		}

		//センサーニューロンが持つニューロコネクションのニューロン番号を調整する
		for (auto& sn : SensorNeuronVector) {
			sn.AdjustNeuroNumber(Index);
		}
	}
}

//センサーのオンオフを突然変異
void NeuralNetwork::MutateSensorOnOff() {
	RandomOperation& RO = GetRandomOperation();
	//各センサータイプ毎に処理
	for (auto itr = SensorOnOff.begin(); itr != SensorOnOff.end(); itr++) {
		unsigned int Type = itr->first; //センサータイプ
		bool OnOff = itr->second; //センサーのオンオフ

		SensorOnOff[Type] = RO.AddMutation(OnOff); //突然変異が発生した場合OnOffを反転
	}
}