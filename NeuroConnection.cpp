#include "NeuroConnection.h"

//コンストラクタ
NeuroConnection::NeuroConnection(const int NeuroNumber) {
	//メンバ変数をランダムに初期化
	RandomOperation& RO = GetRandomOperation();
	this->Weight = RO.MakeRandomValue(-1.0, 1.0);

	this->NeuroNumber = IsValueValid(NeuroNumber, -3);
}

NeuroConnection::NeuroConnection(const double Weight, const int NeuroNumber) {
	this->Weight = Weight;
	this->NeuroNumber = IsValueValid(NeuroNumber, -3);
}

//デストラクタ
NeuroConnection::~NeuroConnection() {
}

//突然変異
void NeuroConnection::Mutation() {
	//メンバ変数に突然変異を加える
	RandomOperation& RO = singleton<RandomOperation>::get_instance();
	Weight = RO.AddMutation(Weight);
	NeuroNumber = RO.AddMutation(NeuroNumber, -3);
}

//ニューロン番号から１つ減じる
void NeuroConnection::DecreaseNeuronNumber() {
	//ニューロン番号が-3より大きい場合のみ減じる
	if (NeuroNumber > -3) NeuroNumber--;
}