#include "Box.h"

Box::Box()
{
	Density = 25.0;
}


Box::~Box()
{
}

//直方体オブジェクトの生成
void Box::MakeBox(dWorldID World, dSpaceID Space, std::array<double, 3> Sides,
	std::array<double, 3> InitialPosition, std::array<double, 3> Angles, std::vector<dGeomID>& GeomIDs)
{
	//剛体オブジェクトの生成
	Body = dBodyCreate(World);
	//質量・重心・慣性テンソルをメンバに持つ質量パラメータを宣言
	dMass Mass;
	//密度と辺の長さから質量パラメータをセット
	dMassSetBox(&Mass, Density, Sides[0], Sides[1], Sides[2]);
	//決定した質量を剛体オブジェクトにセット
	dBodySetMass(Body, &Mass);
	//ジオメトリの生成
	Geom = dCreateBox(Space, Sides[0], Sides[1], Sides[2]);
	//GeomIDsにGeomを追加
	GeomIDs.push_back(Geom);
	//オブジェクトとジオメトリの結合
	dGeomSetBody(Geom, Body);
	//配置座標の決定
	dBodySetPosition(Body, InitialPosition[0], InitialPosition[1], InitialPosition[2]);
	//配置姿勢の決定
	dQuaternion q, q1, q2, q3, q4;
	dQFromAxisAndAngle(q1, 1.0, 0.0, 0.0, Angles[0]); // x軸まわりに回転
	dQFromAxisAndAngle(q2, 0.0, 1.0, 0.0, Angles[1]); // y軸まわりに回転
	dQFromAxisAndAngle(q3, 0.0, 0.0, 1.0, Angles[2]); // z軸まわりに回転
	dQMultiply0(q4, q1, q2);
	dQMultiply0(q, q4, q3);
	dBodySetQuaternion(Body, q);
}