#pragma once
#include <map>

//! 各センサーに固有のビットを割り当て区別する
namespace SensorType {
	extern const unsigned int Joint;
	extern const unsigned int Touch;
	extern const unsigned int Sound;
	extern const unsigned int Eye;
}

//! 各センサーが必要とするニューロンの数を定義する
extern const std::map<const unsigned int, const unsigned int> SensorNeuronNumber;