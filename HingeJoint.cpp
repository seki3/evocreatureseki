#include "HingeJoint.h"

//コンストラクタ
HingeJoint::HingeJoint() {
}

//デストラクタ
HingeJoint::~HingeJoint() {
}

//ジョイントにトルクを加える
void HingeJoint::AddTorque(const dReal Torque) {
	//ジョイントがある場合だけトルクを加える
	if (Joint != nullptr) dJointAddHingeTorque(Joint, Torque);
}

//　親パーツに子パーツを接続
void HingeJoint::JointAttach(dWorldID World, dBodyID ParentBody, dBodyID MyBody, const std::array<double, 3>JointPosition, const std::array<double, 3>JointAxis) {
	Joint = dJointCreateHinge(World, 0);   // ジョイントの生成
	dJointAttach(Joint, ParentBody, MyBody); // ジョイントの接続
	dJointSetHingeAxis(Joint, JointAxis[0], JointAxis[1], JointAxis[2]); // ヒンジの回転軸の設定
	dJointSetHingeAnchor(Joint, JointPosition[0], JointPosition[1], JointPosition[2]);//ジョイントの位置
	dJointSetHingeParam(Joint, dParamLoStop, -M_PI / 3.0); //ヒンジの可動上限角度
	dJointSetHingeParam(Joint, dParamHiStop, M_PI / 3.0);  //ヒンジの可動下限角度
}