#pragma once
#include <array>
#include <ode\ode.h>
#include "Parameter.h"

class Ray
{
private:
	/*! 線分の形の衝突空間を生成する際に与えられるID*/
	dGeomID RayGeom;
	
	double Length;//ビームセンサーの長さ
	double MeasuredDistance;//ビームセンサーの始点（ボックスパーツの重心と一致）から検知した物体までの距離

public:
	Ray();
	~Ray();

	void CreateRay(dSpaceID Space, dBodyID InstallationBody,
		std::array<double, 3> StartingPoint,
		std::array<double, 3> AttitudeAngle);

	dGeomID GetGeomID() {
		return RayGeom;
	}


	//測定距離の値をセットする
	void SetMeasuredDistance(double Distance) {
		MeasuredDistance = Distance;
	}

	//測定距離の値を返す
	double GetMeasuredDistance() {
		return MeasuredDistance;
	}
};
