#pragma once
#define _USE_MATH_DEFINES
#include <math.h>
#include "BaseNeuron.h"

/*! @brief エフェクターニューロンのクラス
@details ニューラルネットワークの出力を担う。\n
正負の値を出力するために活性化関数はtanhを用いる。
*/
class EffectorNeuron : public BaseNeuron {
public:
	/*! @brief コンストラクタ。活性化関数を初期化する。*/
	EffectorNeuron();

	/*! @brief デストラクタ */
	virtual ~EffectorNeuron();

};