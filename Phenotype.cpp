#include "Phenotype.h"

Phenotype::Phenotype(const Genotype G) : HeadComponent(Development(G)) {
}

Phenotype::~Phenotype() {
}

std::array<double, 3> Phenotype::GetHeadComponentPosition() {
	//ヘッドコンポーネントのdGeomIDはGeomIDs[0]に格納されている．
	const dReal* Position;
	Position = dGeomGetPosition(GeomIDs[0]);

	std::array<double, 3> HeadComponentPosition{ Position[0],Position[1],Position[2] };
	return HeadComponentPosition;
}

//GeomIDsを表示
void Phenotype::PrintGeomIDs() {
	for (unsigned int i = 0; i < GeomIDs.size(); i++) {
		std::cout << "GeomIDs[" << i << "]=" << GeomIDs[i] << std::endl;
	}
}

//仮想物理環境上に実体を作成
void Phenotype::MakeBody(dWorldID World, dSpaceID Space, const std::array<double, 3> HeadPosition) {
	HeadComponent.MakePart(World, Space, HeadPosition, GeomIDs);
}

//全コンポーネントを動かす
void Phenotype::MoveComponents() {
	for (int i = 0; i < NeuralNetworkCycle; i++) {
		//ニューラルネットワークを伝播させる
		//ヘッドコンポーネントは親コンポーネントを持たないので親からの情報は0.0
		HeadComponent.PropagateNeuralNetwork(0.0);

		//ニューラルネットワークを発火
		HeadComponent.FireNeuralNetwork();
	}

	//ボディーパーツを動かす
	HeadComponent.MoveBodyPart();
}

//引数として与えられたGeomを持っていればtrue,持っていなければfalseを返す．
bool Phenotype::HasTheGeomID(dGeomID Geom) {
	for (auto GID : GeomIDs) {
		if (Geom == GID) return true;
	}
	return false;
}

//各パーツが知覚する音の値をセット
void Phenotype::SetPerceivedSounds(const std::vector<std::array<double, 4>> Sounds) {
	HeadComponent.SetPerceivedSounds(Sounds);
}

//各ビームセンサーが検知した物体までの距離をセットする
void Phenotype::SetBeamSensorValues(const std::unordered_map<dGeomID, double> HitBeamSensorValues) {
	HeadComponent.SetBeamSensorValue(HitBeamSensorValues);
}

//各ボックスパーツのタッチセンサーにタッチの有無をセットする
void Phenotype::SetBoxPartTouchSensor(const std::vector<dGeomID> CollisionBoxGeomIDs) {
	HeadComponent.SetBoxPartTouchSensor(CollisionBoxGeomIDs);
}

//遺伝子型を展開しヘッドコンポーネントを作成
Component Phenotype::Development(Genotype G) {
	HeadNode HN = G.GetHeadNode(); //ヘッドノードを取り出す
	std::vector<Connection> ConnectionVector = HN.GetConnectionVector(); //ヘッドノードの持つコネクションを取り出す
	std::vector<Component> ChildComponentVector = ExpandGenotype(0, G, -1, ConnectionVector); //ヘッドコンポーネントの持つ子コンポーネントを作成

	return Component(HN.GetSides(), HN.GetEyeDirection(), HN.GetSensorType(), HN.GetNeuralNetwork(), std::array<double, 2>{0.0, 0.0}, std::array<double, 2>{0.0, 0.0}, std::array<double, 3>{0.0, 0.0, 0.0}, ChildComponentVector); //展開したヘッドコンポーネントを返す
}

//コネクションを元に遺伝子型を展開していく再帰関数
std::vector<Component> Phenotype::ExpandGenotype(const int NumComponents, Genotype G, const int ParentNodeIndex, std::vector<Connection> ConnectionVector, const int RecursiveCount) {
	std::vector<Component> ChildComponentVector; //親コンポーネントが持つ子コンポーネントベクトル

	if (NumComponents > 10) return ChildComponentVector; //すでに10個その枝でコンポーネントが展開されている場合これ以上展開しない(無限ループ阻止)

	//コネクション毎に子コンポーネント作成操作を行う
	for (auto C : ConnectionVector) {
		double Scale = C.GetScale(); //スケールを取り出す
		int ChildNodeIndex = C.GetNormalNodeIndex(); //子ノーマルノードのインデックスを取り出す

		//子ノードのインデックスが存在するノードを指し示していない場合、子コンポーネントは作成されない
		if (ChildNodeIndex >= G.GetNormalNodeVector().size()) {
			continue;
		}

		NormalNode ChildNode = G.GetNormalNodeVector()[ChildNodeIndex]; //子ノードを取り出す
		std::array<double, 3> Sides = ChildNode.GetSides(); //子ノードの辺の長さを取り出す

		//辺の長さにスケールを掛けて子コンポーネントの辺の長さとする
		for (double &s : Sides) {
			s *= Scale;
		}

		//再帰操作中ではないが親ノードと子ノードのインデックスが同じ（つまり再帰1回目）しかし許される再帰回数が0の時、子コンポーネントは作成されない
		if (RecursiveCount == -1 && ParentNodeIndex == ChildNodeIndex && ChildNode.GetRecursiveLimit() == 0) {
			continue;
		}
		//残り再帰回数が0で親ノードと子ノードのインデックスが同じ時、子コンポーネントは作成されない
		if (RecursiveCount == 0 && ParentNodeIndex == ChildNodeIndex) {
			continue;
		}
		//残り再帰回数が0でないかつ再帰終了時フラグが真の時、子コンポーネントは作成されない
		if (RecursiveCount != 0 && C.GetRecursiveEnd() == true) {
			continue;
		}

		std::vector<Component> ChildChildComponentVector; //子コンポーネントの持つ子コンポーネントベクトル
		//再帰操作中ではないが親ノードと子ノードのインデックスが同じ（つまり再帰1回目）場合、再帰操作に入る
		if (RecursiveCount == -1 && ParentNodeIndex == ChildNodeIndex) {
			ChildChildComponentVector = ExpandGenotype(NumComponents + 1, G, ChildNodeIndex, ChildNode.GetConnectionVector(), ChildNode.GetRecursiveLimit() - 1);
		}
		//再帰操作中で親ノードと子ノードのインデックスが同じ場合、引き続き再帰操作行う
		else if (RecursiveCount > 0 && ParentNodeIndex == ChildNodeIndex) {
			ChildChildComponentVector = ExpandGenotype(NumComponents + 1, G, ChildNodeIndex, ChildNode.GetConnectionVector(), RecursiveCount - 1);
		}
		//上記以外の場合普通に操作を行う
		else {
			ChildChildComponentVector = ExpandGenotype(NumComponents + 1, G, ChildNodeIndex, ChildNode.GetConnectionVector());
		}
		//作成した子コンポーネントをベクトルに追加
		ChildComponentVector.push_back(Component(Sides, ChildNode.GetEyeDirection(), ChildNode.GetSensorType(), ChildNode.GetNeuralNetwork(), C.GetJointAttachVector(), C.GetJointAxisVector(), C.GetAttitudeAngle(), ChildChildComponentVector));
	}

	//子コンポーネントベクトルを返す
	return ChildComponentVector;
}

void Phenotype::DestroyDynamicsPhenotype() {
	for (int i = 0; i < GeomIDs.size(); i++) {
		//dGeomIDに対応するdBodyIDを取得し，そのdBodyIDと紐づけられている剛体オブジェクトを破壊
		dBodyDestroy(dGeomGetBody(GeomIDs[i]));
		dGeomDestroy(GeomIDs[i]);
	}
}