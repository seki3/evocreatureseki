#include "Connection.h"

//コンストラクタ
Connection::Connection(const unsigned int NormalNodeIndex) {
	//メンバ変数をランダムに初期化
	RandomOperation& RO = GetRandomOperation();
	this->JointAttachVector = RO.MakeRandomValue<double, 2>(0.0, 2.0 * M_PI);
	this->JointAxisVector = RO.MakeRandomValue<double, 2>(0.0, 2.0 * M_PI);
	this->AttitudeAngle = RO.MakeRandomValue<double, 3>(0.0, M_PI);
	this->Scale = RO.MakeRandomValue(MinScale, MaxScale);
	this->RecursiveEnd = RO.MakeRandomBoolValue();

	this->NormalNodeIndex = IsValueValid<unsigned int>(NormalNodeIndex, 0);
}

Connection::Connection(const std::array<double, 2> JointAttachVector, const std::array<double, 2> JointAxisVector,
	const std::array<double, 3> AttitudeAngle, const double Scale, const bool RecursiveEnd, const unsigned int NormalNodeIndex) {

	//メンバ変数を初期化
	this->JointAttachVector = IsArrayValueValid(JointAttachVector, 0.0, 2.0 * M_PI);
	this->JointAxisVector = IsArrayValueValid(JointAxisVector, 0.0, 2.0 * M_PI);
	this->AttitudeAngle = IsArrayValueValid(AttitudeAngle, 0.0, M_PI);
	this->Scale = IsValueValid(Scale, MinScale, MaxScale);
	this->RecursiveEnd = RecursiveEnd;
	this->NormalNodeIndex = IsValueValid<unsigned int>(NormalNodeIndex, 0);
}

//デストラクタ
Connection::~Connection() {
}

//突然変異
void Connection::Mutation() {
	//ニューラルネットワークだけ進化する場合この関数は実行されない
	if (OnlyNeuralNetworkEvolves) return;

	//メンバ変数に突然変異を加える
	RandomOperation& RO = GetRandomOperation();
	JointAttachVector = RO.AddMutation<double, 2>(JointAttachVector, 0.0, 2.0 * M_PI);
	JointAxisVector = RO.AddMutation<double, 2>(JointAxisVector, 0.0, 2.0 * M_PI);
	AttitudeAngle = RO.AddMutation<double, 3>(AttitudeAngle, 0.0, M_PI);
	Scale = RO.AddMutation(Scale, MinScale, MaxScale);
	RecursiveEnd = RO.AddMutation(RecursiveEnd);
	NormalNodeIndex = RO.AddMutation(NormalNodeIndex, 0);
}

//ノーマルノードのインデックスから1つ減じる
void Connection::DecreaseIndex() {
	//ノーマルノードのインデックスが0より大きい場合のみ減じる
	if (NormalNodeIndex > 0) NormalNodeIndex--;
}