#include "BaseNeuron.h"

//コンストラクタ
BaseNeuron::BaseNeuron() {
}

//デストラクタ
BaseNeuron::~BaseNeuron() {
}

//値を他のニューロンから受け取る
void BaseNeuron::Set(const double x) {
	Input += x;
}

//発火
void BaseNeuron::Fire() {
	Output = ActivationFunction(Input);
	Input = 0.0;
}