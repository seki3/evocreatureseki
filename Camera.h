#pragma once

#include <iostream>
#include <array>
#include <GLFW\glfw3.h>
#include <ode\ode.h>

class Camera
{
private:
	std::array<double, 3> Position;     // 視点（カメラの位置）
	std::array<double, 3> RollPitchYaw; // カメラの向き(度)（ロール・ピッチ・ヨー）
	std::array<double, 2> PreviousMouseCoordinate;// 1回分更新前のウィンドウ上のマウスの座標(x,y)

	enum MouseButton
	{
		LeftButton = 0,  // マウスの左ボタンに関するアクション（押されたかor離された）がされた
		RightButton = 1, // マウスの右ボタンに関するアクション（押されたかor離された）がされた
		MiddleButton = 2 // マウスの中央ボタンに関するアクション（押されたかor離された）がされた
	};

	enum MouseAction
	{
		MouseUp = 0,  // マウスボタンが離された
		MouseDown = 1 // マウスボタンが押された
	};

	int MouseButtonState;// マウスのボタン操作の状態に応じてenum MouseButtonStateの値が格納される
	enum MouseButtonState
	{
		IsNotDragging = 0,         // マウスはドラッグしていない
		LeftButtonIsDragging = 1,  // マウスの左ボタンでドラッグしている
		RightButtonIsDragging = 2, // マウスの右ボタンでドラッグしている
		MiddleButtonIsDragging = 3,// マウスの中央ボタンでドラッグしている
		MouseWheelIsRotating = 4   // マウスホイールが回転している 
	};

public:
	Camera();
	~Camera();
	void Motion(int Mode, double Width, double Height, double Xcoordinate, double Ycoordinate, double ScrollingIncrement=0);// カメラの位置と向きの変更
	void CameraUpdate();// カメラの位置と向きの更新	
	void SetMouseButtonState(int Button, int Action, double Xcoordinate, double Ycoordinate);// メンバ変数MouseButtonStateに値をセット
	int GetMouseButtonState() {
		return MouseButtonState;
	}
	// カメラの位置座標を返す
	std::array<double, 3> GetPosition() {
		return { Position[0], Position[1], Position[2] };
	}

	// カメラの向き（ロール，ピッチ，ヨー）を返す
	std::array<double, 3> GetRollPitchYaw() {
		return { RollPitchYaw[0], RollPitchYaw[1], RollPitchYaw[2] };
	}
	
};

