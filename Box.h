/**
ボックスクラスは仮想物理空間の直方体に関するクラスです
*/

#pragma once
#include <iostream>
#include <ode\ode.h>
#include <array>
#include <vector>


class Box
{
private:

	/*! 剛体の直方体を生成する際に与えられるID */
	dBodyID Body;

	/*! 直方体の形の衝突空間を生成する際に与えられるID */
	dGeomID Geom;

	/*! 直方体の密度 */
	double Density;

	/*! タッチセンサーの検知の有無を表す論理型．地面や他の物体に触れた場合はTrue,そうでなけばfalse*/
	bool TouchSensor;


public:
	/*! コンストラクタ*/
	Box();
	/*! デストラクタ*/
	~Box();


	/*! @brief 直方体の重心座標を返す
	@return 直方体の重心座標 */
	const dReal* GetPosition() {
		return dGeomGetPosition(Geom);
	}

	/*! @brief 姿勢行列を取得し返す．
	@return 姿勢行列 */
	const dReal* GetRotation() {
		return dGeomGetRotation(Geom);
	}

	/*! @brief 自身のdBodyIDを返す
	@return 自身のdBodyID */
	dBodyID GetBodyID() {
		return Body;
	}

	/*! @brief 自身のdGeomIDを返す
	@return 自身のdGeomID */
	dGeomID GetGeomID() {
		return Geom;
	}

	/*! @brief タッチの有無を返す
	@return タッチセンサーの真理値\n
	真理値の定義はこの直方体オブジェクトが地面や他の立体オブジェクトに触れていればtrue．そうでなければfalseである．\n
	@sa SetTouchSensor() で真理値をセットする*/
	bool GetTouchSensor() {
		return TouchSensor;
	}

	/*! @brief タッチの有無をセットする
	この直方体オブジェクトが地面や他の立体オブジェクトに触れているかを表すTouchSensorに値をセットする
	@param bool HasTouched：タッチしたかの有無．タッチしていればtrue．していなければfalse */
	void SetTouchSensor(bool HasTouched) {
		TouchSensor = HasTouched;
	}

	/*! @brief 仮想物理空間に直方体オブジェクトを生成する
	@param dWorldID World：仮想物理空間のID
	@param dSpaceID Space：仮想物理空間の衝突空間のID
	@param std::array<double, 3> Sides：直方体の各辺の長さ
	直方体は回転する指示がない場合ワールド座標系のxyz軸と直方体の各辺がそれぞれ平行になるように生成される．\n
	Sides[0]：ワールド座標系でのx軸と平行な直方体の辺の長さ\n
	Sides[1]：ワールド座標系でのy軸と平行な直方体の辺の長さ\n
	Sides[2]：ワールド座標系でのz軸と平行な直方体の辺の長さ\n
	@param std::array<double, 3> InitialPosition：直方体が生成されるワールド座標系での重心座標
	@param std::array<double, 3> Angles：直方体の姿勢角\n
	回転軸は直方体オブジェクトのローカル座標系で，各軸に対して右手回りが回転の正の向きである．\n
	x軸まわりにAngles[0][rad]回転し，それに伴って回転した直方体のローカル座標系での\n
	y軸まわりにAngles[1][rad]回転し，さらにそれに伴って回転した直方体のローカル座標系での\n
	z軸まわりにAngles[2][rad]回転する．\n
	回転にはクォータニオンを使用した．この際，次のサイトを参考にした．\n
	https://www18.atwiki.jp/zukunashi-yarou/pages/34.html \n
	ただし，このサイトのdQMultiply0(q, q3, q4);の部分は誤りで
	dQMultiply0(q, q4, q3);が正解だと思われるのでそのように変更した．
	@param std::vector <dGeomID>& GeomIDs：Phenotype::GeomIDsに登録するために引いている
	*/
	void MakeBox(dWorldID World, dSpaceID Space, std::array<double, 3> Sides, std::array<double, 3> InitialPosition,
		std::array<double, 3> Angles, std::vector <dGeomID>& GeomIDs);


};

