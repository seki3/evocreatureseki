#pragma once
#include "BaseNode.h"

/*! @brief 通常のノード
@details ベースノードを継承する。\n
エージェントの体のほとんどはノーマルノードによって作られる。\n
コネクションによってノーマルノード同士は繋がる。
*/
class NormalNode : public BaseNode {
public:
	/*! @brief 通常のコンストラクタ
	@details センサーのタイプをパラメータNormalNodeSensorTypeとし、ベースノードの通常コンストラクタを呼び出す。
	*/
	NormalNode();

	/*! @brief 任意のノーマルノードを作成するコンストラクタ
	@param[in] Sides ノードの辺の長さ
	@param[in] EyeDirection 視覚センサーの方向
	@param[in] NN ニューラルネットワーク
	@param[in] ConnectionVector コネクションの配列
	@param[in] RecursiveLimit 再帰的に呼び出すことのできる回数
	@details ノーマルノードの持つ全ての情報を与えてノーマルノードを作成する。\n
	範囲外の値があった場合は例外が発生し強制終了する。
	*/
	NormalNode(const std::array<double, 3> Sides, const std::array<double, 3> EyeDirection,
		const NeuralNetwork NN, const std::vector<Connection> ConnectionVector, const unsigned int RecursiveLimit);

	/*! @brief デストラクタ*/
	virtual ~NormalNode();

	/*! @brief 再帰的に呼び出すことのできる回数を返す
	@return unsigned int RecursiveLimit*/
	unsigned int GetRecursiveLimit() { return RecursiveLimit; }

	/*! @brief ノーマルノードの持つ各情報に対して突然変異を発生させる
	@param[in] NormalNodeNumber 遺伝子型の持つノーマルノードの数*/
	virtual void Mutation(const unsigned int NormalNodeNumber);

private:
	//! 表現型に展開する際に再帰的に呼び出すことのできる回数
	unsigned int RecursiveLimit;

	friend class cereal::access;
	/*! @brief シリアライズ
	@details ノーマルノードの持つ全ての情報をシリアライズする関数*/
	template<class Archive>
	void serialize(Archive& archive) {
		archive(cereal::make_nvp("BaseNode", cereal::base_class<BaseNode>(this)),
			CEREAL_NVP(RecursiveLimit));
	}

};