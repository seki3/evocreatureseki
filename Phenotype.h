#pragma once
#include <iostream>
#include "Component.h"
#include "Genotype.h"

/*!
@brief エージェントの表現型クラス
@details このクラスでは遺伝子型を受け取り表現型に展開する。\n
また、エージェント全体への操作を行う。
*/
class Phenotype {
public:
	/*! @brief コンストラクタ
	@param[in] G 遺伝子型
	@details 遺伝子型を受け取り展開を行う。
	*/
	Phenotype(const Genotype G);

	/*! @brief デストラクタ */
	virtual ~Phenotype();

	/*! @brief ヘッドコンポーネントを返す
	@return Component& HeadComponent
	*/
	Component& GetHeadComponent() { return HeadComponent; }
	int GetGeomIDsSize() { return GeomIDs.size(); }
	std::vector<dGeomID>& GetGeomIDs() { return GeomIDs; }
	std::vector<dGeomID> GetRayGeomIDs() { return HeadComponent.GetHeadPartBeamSensorRayGeomID(); }
	std::array<double, 3> GetHeadComponentPosition();

	void PrintGeomIDs(); //GeomIDsを表示
	void MakeBody(dWorldID World, dSpaceID Space, const std::array<double, 3> HeadPosition); //仮想物理環境上に実体を作成
	void MoveComponents(); //コンポーネントを動かす
	bool HasTheGeomID(dGeomID Geom); //引数として与えられたGeomを持っていればtrue,持っていなければfalseを返す．

	void SetPerceivedSounds(const std::vector<std::array<double, 4>> Sounds); //各パーツが知覚する音の値をセット
	void SetBeamSensorValues(const std::unordered_map<dGeomID, double> HitBeamSensorValues); //各ビームセンサーが検知した物体までの距離をセットする
	void SetBoxPartTouchSensor(const std::vector<dGeomID> CollisionBoxGeomIDs); //各ボックスパーツのタッチセンサーにタッチの有無をセットする

	/*! @brief 遺伝子型から表現型へ展開を行う
	@param[in] G 遺伝子型
	@return Component HeadComponent
	@details 遺伝子型から表現型へ展開する際に最初に呼び出される関数。\n
	この関数はHeadNodeから展開を始め、最後にHeadComponentを返す。
	*/
	Component Development(Genotype G);

	/*! @brief コネクションを基にノードを展開していく関数
	@param[in] NumComponents 今現在展開しているコンポーネントの数
	@param[in] G 遺伝子型
	@param[in] ParentNodeIndex 親ノードのインデックス
	@param[in] ConnectionVector 親ノードのコネクション配列
	@param[in] RecursiveCount 現在の再帰回数
	@return std::vector<Component> ChildComponentVector
	@details 親コンポーネントが持つ子コンポーネントの配列を作成していく。\n
	この関数は再帰的に子コンポーネントの持つ子コンポーネントの配列を作成し、展開していく。
	*/
	std::vector<Component> ExpandGenotype(const int NumComponents, Genotype G, const int ParentNodeIndex, std::vector<Connection> ConnectionVector, const int RecursiveCount = -1);


	void DestroyDynamicsPhenotype();
private:

	//! ヘッドノードを展開したヘッドコンポーネント
	Component HeadComponent;

	std::vector <dGeomID> GeomIDs; //ヘッドパーツ及びボディパーツのGeomIDの一覧．展開順に格納されている．

};