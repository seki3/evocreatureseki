#pragma once
#include "Sensor.h"

//ノード
//! ノードの辺の最小値(0より大きくMaxScaleより小さい)
extern const double MinSide;

//! ノードの辺の最大値(MaxSideより大きい)
extern const double MaxSide;

//! 許される再帰回数の上限(0以上)
extern const unsigned int MaxRecursiveLimit;

//コネクション
//! スケールの最小値(0より大きくMaxScaleより小さい)
extern const double MinScale;

//! スケールの最大値(MinScaleより大きい)
extern const double MaxScale;

//ニューラルネットワーク
//! ビームセンサーの長さ(0以上)
extern const double BeamSensorLength;

//! ニューラルネットワークのサイクル(伝播と発火)の回数(1以上)
extern const unsigned int NeuralNetworkCycle;

//! ヘッドノードのセンサータイプ
extern const unsigned int HeadNodeSensorType;

//! ノーマルノードのセンサータイプ
extern const unsigned int NormalNodeSensorType;

//遺伝的アルゴリズム
//! 乱数シード
extern const int RandomSeed;

//! 個体数(1以上)
extern const unsigned int Population;

//! 世代数(1以上)
extern const unsigned int Generation;

//! ニューラルネットワークだけを進化させるか
extern const bool OnlyNeuralNetworkEvolves;

//! 交叉する確率(0以上1以下)
extern const double CrossOverRate;

//! 突然変異率(0以上1以下)
extern const double MutationRate;

//! 1世代あたりの評価時間(物理演算を更新するステップ数)(1以上)
extern const unsigned int EvaluationTime;

//物理エンジン
//物理エンジンのパラメータをここにまとめて書く