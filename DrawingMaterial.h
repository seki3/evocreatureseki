#pragma once
#include <GLFW\glfw3.h>
//--------------------------------------------------------
// 物質質感の定義
//--------------------------------------------------------
// 参考サイト：仮想物理実験室の構築（ver1.0）
// http://www.natural-science.or.jp/article/20091209225032.php
// 以下のページで公開されているソースを利用させていただきます。
// http://akita-nct.jp/yamamoto/comp/OpenGL/OpenGL.html
//--------------------------------------------------------
struct MaterialStruct {
	GLfloat ambient[4];
	GLfloat diffuse[4];
	GLfloat specular[4];
	GLfloat shininess;
};
//////////////////////////////////////////
//ruby(ルビー)
MaterialStruct ms_ruby = {
	{ 0.1745f,   0.01175f,  0.01175f,   1.0f },
	{ 0.61424f,  0.04136f,  0.04136f,   1.0f },
	{ 0.727811f, 0.626959f, 0.626959f,  1.0f },
	76.8f };
//emerald(エメラルド)
MaterialStruct ms_emerald = {
	{ 0.0215f,  0.1745f,   0.0215f,  1.0f },
	{ 0.07568f, 0.61424f,  0.07568f, 1.0f },
	{ 0.633f,   0.727811f, 0.633f,   1.0f },
	76.8f };
//jade(翡翠)
MaterialStruct ms_jade = {
	{ 0.135f,     0.2225f,   0.1575f,   1.0f },
	{ 0.54f,      0.89f,     0.63f,     1.0f },
	{ 0.316228f,  0.316228f, 0.316228f, 1.0f },
	12.8f };
//obsidian(黒曜石)
MaterialStruct ms_obsidian = {
	{ 0.05375f, 0.05f,    0.06625f, 1.0f },
	{ 0.18275f, 0.17f,    0.22525f, 1.0f },
	{ 0.332741f,0.328634f,0.346435f,1.0f },
	38.4f };
// pearl(真珠)
MaterialStruct ms_pearl = {
	{ 0.25f,     0.20725f,  0.20725f,  1.0f },
	{ 1.0f,      0.829f,    0.829f,    1.0f },
	{ 0.296648f, 0.296648f, 0.296648f, 1.0f },
	10.24f };
//turquoise(トルコ石)
MaterialStruct ms_turquoise = {
	{ 0.1f,     0.18725f, 0.1745f,  1.0f },
	{ 0.396f,   0.74151f, 0.69102f, 1.0f },
	{ 0.297254f,0.30829f, 0.306678f,1.0f },
	12.8f };
//brass(真鍮)
MaterialStruct ms_brass = {
	{ 0.329412f,  0.223529f, 0.027451f, 1.0f },
	{ 0.780392f,  0.568627f, 0.113725f, 1.0f },
	{ 0.992157f,  0.941176f, 0.807843f, 1.0f },
	27.89743616f };
//bronze(青銅)
MaterialStruct ms_bronze = {
	{ 0.2125f,   0.1275f,   0.054f,   1.0f },
	{ 0.714f,    0.4284f,   0.18144f, 1.0f },
	{ 0.393548f, 0.271906f, 0.166721f,1.0f },
	25.6f };
//chrome(クローム)
MaterialStruct ms_chrome = {
	{ 0.25f,    0.25f,     0.25f,     1.0f },
	{ 0.4f,     0.4f,      0.4f,      1.0f },
	{ 0.774597f,0.774597f, 0.774597f, 1.0f },
	76.8f };
//copper(銅)
MaterialStruct ms_copper = {
	{ 0.19125f,  0.0735f,   0.0225f,  1.0f },
	{ 0.7038f,   0.27048f,  0.0828f,  1.0f },
	{ 0.256777f, 0.137622f, 0.086014f,1.0f },
	12.8f };
//gold(金)
MaterialStruct ms_gold = {
	{ 0.24725f,  0.1995f,   0.0745f,    1.0f },
	{ 0.75164f,  0.60648f,  0.22648f,   1.0f },
	{ 0.628281f, 0.555802f, 0.366065f,  1.0f },
	51.2f };
//silver(銀)
MaterialStruct ms_silver = {
	{ 0.19225f,  0.19225f,  0.19225f, 1.0f },
	{ 0.50754f,  0.50754f,  0.50754f, 1.0f },
	{ 0.508273f, 0.508273f, 0.508273f,1.0f },
	51.2f };
//プラスチック(黒)
MaterialStruct ms_black_plastic = {
	{ 0.0f,    0.0f,    0.0f,  1.0f },
	{ 0.01f,   0.01f,   0.01f, 1.0f },
	{ 0.50f,   0.50f,   0.50f, 1.0f },
	32.0f };
//プラスチック(シアン)
MaterialStruct ms_cyan_plastic = {
	{ 0.0f,   0.1f,    0.06f,    1.0f },
	{ 0.0f,       0.50980392f,0.50980392f, 1.0f },
	{ 0.50196078f,0.50196078f,0.50196078f, 1.0f },
	32.0f };
//プラスチック(緑)
MaterialStruct ms_green_plastic = {
	{ 0.0f,     0.0f,   0.0f,  1.0f },
	{ 0.1f,     0.35f,  0.1f,  1.0f },
	{ 0.45f,    0.55f,  0.45f, 1.0f },
	32.0f };
//プラスチック(赤)
MaterialStruct ms_red_plastic = {
	{ 0.0f,     0.0f,     0.0f,  1.0 },
	{ 0.5f,     0.0f,     0.0f,  1.0 },
	{ 0.7f,     0.6f,     0.6f,  1.0 },
	32.0f };
//プラスチック(白)
MaterialStruct ms_white_plastic = {
	{ 0.0f,   0.0f,     0.0f,  1.0f },
	{ 0.55f,  0.55f,    0.55f, 1.0f },
	{ 0.70f,  0.70f,    0.70f, 1.0f },
	32.0f };
//プラスチック(黄)
MaterialStruct ms_yellow_plastic = {
	{ 0.0f,  0.0f,     0.0f,  1.0f },
	{ 0.5f,  0.5f,     0.0f,  1.0f },
	{ 0.60f, 0.60f,    0.50f, 1.0f },
	32.0f };
//ゴム(黒)
MaterialStruct ms_black_rubber = {
	{ 0.02f,   0.02f,    0.02f, 1.0f },
	{ 0.01f,   0.01f,    0.01f, 1.0f },
	{ 0.4f,    0.4f,     0.4f,  1.0f },
	10.0f };
//ゴム(シアン)
MaterialStruct ms_cyan_rubber = {
	{ 0.0f,     0.05f,    0.05f, 1.0f },
	{ 0.4f,     0.5f,     0.5f,  1.0f },
	{ 0.04f,    0.7f,     0.7f,  1.0f },
	10.0f };
//ゴム(緑)
MaterialStruct ms_green_rubber = {
	{ 0.0f,    0.05f,    0.0f,  1.0f },
	{ 0.4f,    0.5f,     0.4f,  1.0f },
	{ 0.04f,   0.7f,     0.04f, 1.0f },
	10.0f };
//ゴム(赤)
MaterialStruct ms_red_rubber = {
	{ 0.05f,     0.0f,     0.0f,  1.0f },
	{ 0.5f,      0.4f,     0.4f,  1.0f },
	{ 0.7f,      0.04f,    0.04f, 1.0f },
	10.0f };
//ゴム(白)
MaterialStruct ms_white_rubber = {
	{ 0.05f,   0.05f,    0.05f, 1.0f },
	{ 0.5f,    0.5f,     0.5f,  1.0f },
	{ 0.7f,    0.7f,     0.7f,  1.0f },
	10.0f };
//ゴム(黄)
MaterialStruct ms_yellow_rubber = {
	{ 0.05f,  0.05f,    0.0f,  1.0f },
	{ 0.5f,   0.5f,     0.4f,  1.0f },
	{ 0.7f,   0.7f,     0.04f, 1.0f },
	10.0f };
//白色（大桃オリジナル）
MaterialStruct ms_my_white = {
	{ 0.0f,  0.0f,  0.0f,  1.0f },
	{ 1.0f,  1.0f,  1.0f,  1.0f },
	{ 0.0f,  0.0f,  0.0f,  1.0f },
	10.0f };
