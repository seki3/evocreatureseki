#pragma once
#include "Phenotype.h"

//エージェントクラス
class Agent {
public:
	Agent(const Genotype MyGenotype);
	virtual ~Agent();

	//Getter
	std::vector<dGeomID>& GetGeomIDs() { return(MyPhenotype.GetGeomIDs()); }
	std::vector<dGeomID> GetRayGeomIDs() { return(MyPhenotype.GetRayGeomIDs()); }
	std::array<double, 3> GetPosition() {
		return(MyPhenotype.GetHeadComponentPosition());
	}

	//仮想物理空間上に実体としての表現型を生成
	void MakeDynamicsPhenotype(dWorldID World, dSpaceID Space, const std::array<double, 3> HeadPosition);
	void Behave(); //エージェントの行動
	void PrintGeomIDs(); //GeomIDsを表示
	void SetPerceivedSounds(const std::vector<std::array<double, 4> > Sounds); //各パーツが知覚する音の値をセットする
	void SetBeamSensorValues(const std::unordered_map<dGeomID, double> HitBeamSensorValues); //各ビームセンサーが検知した物体までの距離をセットする
	void SetBoxPartTouchSensor(const std::vector<dGeomID> CollisionBoxGeomIDs); //各ボックスパーツのタッチセンサーにタッチの有無をセットする
	bool HasTheGeomID(dGeomID Geom); //引数で与えられたGeomを持っていればtrue,持っていなければfalseを返す
	
	//エージェントの破壊
	void DestroyDynamicsPhenotype() {
		MyPhenotype.DestroyDynamicsPhenotype();
	}
private:
	Phenotype MyPhenotype; //表現型

};
