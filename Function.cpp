#include "Function.h"

//極座標から直行座標に変換
std::array<double, 3> Polar2Cartesian(const double Thetha, const double Phi) {
	return std::array<double, 3>{sin(Thetha) * cos(Phi), sin(Thetha) * sin(Phi), cos(Thetha)};
}

//シグモイド関数
double Sigmoid(const double x) {
	return (1 / (1 + exp(-x)));
}

//線形関数
double Linear(const double x) {
	return x;
}

//直方体の3辺のうち，最も短い辺の長さを返す
double GetMinSideLength(std::array<double, 3> Sides) {
	double MinSideLength = Sides[0];
	if (Sides[1] < MinSideLength) {
		MinSideLength = Sides[1];
	}
	if (Sides[2] < MinSideLength) {
		MinSideLength = Sides[2];
	}
	return (MinSideLength);
}

//3次元での2点間の距離
double Distance3D(std::array<double, 3>P1, std::array<double, 3>P2) {
	double Sum = 0.0;
	for (unsigned int i = 0; i < 3; i++) {
		Sum += std::pow(P2[i] - P1[i], 2);
	}

	return std::pow(Sum, 0.5);
}

//ブール値を反転
bool FlipBoolValue(bool BoolValue) {
	return !BoolValue;
}

//ODEを使用しない直方体同士の衝突検出
//直方体同士が衝突していたらtrueを返し，衝突していなければfalseを返す
bool VerifyCollisionOfBoxes(const std::array<double, 3> Box1Position, const std::array<double, 3>Box2Position,
	const std::array<double, 3> Box1Sides, const std::array<double, 3> Box2Sides,
	const std::array<double, 3> Box1AttitudeAngle, const std::array<double, 3> Box2AttitudeAngle)
{
	//直方体と直方体の衝突判定
	//使用したアルゴリズム：OBBとOBBの衝突判定
	//※OBB：有向境界ボックス（Oriented Bounding Box）
	//参考サイト：http://marupeke296.com/COL_3D_No13_OBBvsOBB.html

	//各方向ベクトルの確保
	//Box1について
	Eigen::Vector3d Box1X(Box1Sides[0] / 2, 0.0, 0.0);
	Eigen::Vector3d Box1Y(0.0, Box1Sides[1] / 2, 0.0);
	Eigen::Vector3d Box1Z(0.0, 0.0, Box1Sides[2] / 2);

	//Box2について																			  
	Eigen::Vector3d Box2X(Box2Sides[0] / 2, 0.0, 0.0);
	Eigen::Vector3d Box2Y(0.0, Box2Sides[1] / 2, 0.0);
	Eigen::Vector3d Box2Z(0.0, 0.0, Box2Sides[2] / 2);

	//クォータニオンを使って，直方体の姿勢に合わせて各方向ベクトルを回転
	//Box1について
	Eigen::Quaterniond Q1X, Q1Y, Q1Z, Q1Product;
	Q1X = Eigen::AngleAxisd(Box1AttitudeAngle[0], Box1X);
	Q1Y = Eigen::AngleAxisd(Box1AttitudeAngle[1], Box1Y);
	Q1Z = Eigen::AngleAxisd(Box1AttitudeAngle[2], Box1Z);
	Q1Product = Q1X * Q1Y * Q1Z;
	Box1X = Q1Product*Box1X;
	Box1Y = Q1Product*Box1Y;
	Box1Z = Q1Product*Box1Z;

	//Box2について
	Eigen::Quaterniond Q2X, Q2Y, Q2Z, Q2Product;
	Q2X = Eigen::AngleAxisd(Box2AttitudeAngle[0], Box2X);
	Q2Y = Eigen::AngleAxisd(Box2AttitudeAngle[1], Box2Y);
	Q2Z = Eigen::AngleAxisd(Box2AttitudeAngle[2], Box2Z);
	Q2Product = Q2X * Q2Y * Q2Z;
	Box2X = Q2Product*Box2X;
	Box2Y = Q2Product*Box2Y;
	Box2Z = Q2Product*Box2Z;

	//Box1の重心からBox2の重心へのベクトル
	Eigen::Vector3d Interval;
	for (int i = 0; i < 3; i++) {
		Interval[i] = -Box1Position[i] + Box2Position[i];
	}

	//分離平面の有無を調べる．分離平面があるならば，2つの直方体は衝突していない
	double Box1R, Box2R, L;

	//Box1Xが分離軸になるかの調査
	Box1R = Box1X.norm();
	Box2R = std::fabs(Box2X.dot(Box1X)) + std::fabs(Box2Y.dot(Box1X)) + std::fabs(Box2Z.dot(Box1X));
	L = std::fabs(Interval.dot(Box1X));
	if (L > Box1R + Box2R) return false;//分離平面が存在しているので衝突していない

	//Box1Yが分離軸になるかの調査
	Box1R = Box1Y.norm();
	Box2R = std::fabs(Box2X.dot(Box1Y)) + std::fabs(Box2Y.dot(Box1Y)) + std::fabs(Box2Z.dot(Box1Y));
	L = std::fabs(Interval.dot(Box1Y));
	if (L > Box1R + Box2R) return false;//分離平面が存在しているので衝突していない

	//Box1Zが分離軸になるかの調査
	Box1R = Box1Z.norm();
	Box2R = std::fabs(Box2X.dot(Box1Z)) + std::fabs(Box2Y.dot(Box1Z)) + std::fabs(Box2Z.dot(Box1Z));
	L = std::fabs(Interval.dot(Box1Z));
	if (L > Box1R + Box2R) return false;//分離平面が存在しているので衝突していない

	//Box2Xが分離軸になるかの調査
	Box1R = std::fabs(Box1X.dot(Box2X)) + std::fabs(Box1Y.dot(Box2X)) + std::fabs(Box1Z.dot(Box2X));
	Box2R = Box2X.norm();
	L = std::fabs(Interval.dot(Box2X));
	if (L > Box1R + Box2R) return false;//分離平面が存在しているので衝突していない

	//Box2Yが分離軸になるかの調査
	Box1R = std::fabs(Box1X.dot(Box2Y)) + std::fabs(Box1Y.dot(Box2Y)) + std::fabs(Box1Z.dot(Box2Y));
	Box2R = Box2Y.norm();
	L = std::fabs(Interval.dot(Box2Y));
	if (L > Box1R + Box2R) return false;//分離平面が存在しているので衝突していない

	//Box2Zが分離軸になるかの調査
	Box1R = std::fabs(Box1X.dot(Box2Z)) + std::fabs(Box1Y.dot(Box2Z)) + std::fabs(Box1Z.dot(Box2Z));
	Box2R = Box2Z.norm();
	L = std::fabs(Interval.dot(Box2Z));
	if (L > Box1R + Box2R) return false;//分離平面が存在しているので衝突していない


	//Box1とBox2の各軸の外積が分離軸になるかの調査
	Eigen::Vector3d Cross;//外積

	//Box1XとBox2Xの外積が分離軸になるかの調査
	Cross = Box1X.cross(Box2X);
	Box1R = std::fabs(Box1Y.dot(Cross)) + std::fabs(Box1Z.dot(Cross));
	Box2R = std::fabs(Box2Y.dot(Cross)) + std::fabs(Box2Z.dot(Cross));
	L = std::fabs(Interval.dot(Cross));
	if (L > Box1R + Box2R) return false;//分離平面が存在しているので衝突していない

	//Box1XとBox2Yの外積が分離軸になるかの調査
	Cross = Box1X.cross(Box2Y);
	Box1R = std::fabs(Box1Y.dot(Cross)) + std::fabs(Box1Z.dot(Cross));
	Box2R = std::fabs(Box2X.dot(Cross)) + std::fabs(Box2Z.dot(Cross));
	L = std::fabs(Interval.dot(Cross));
	if (L > Box1R + Box2R) return false;//分離平面が存在しているので衝突していない

	//Box1XとBox2Zの外積が分離軸になるかの調査
	Cross = Box1X.cross(Box2Z);
	Box1R = std::fabs(Box1Y.dot(Cross)) + std::fabs(Box1Z.dot(Cross));
	Box2R = std::fabs(Box2X.dot(Cross)) + std::fabs(Box2Y.dot(Cross));
	L = std::fabs(Interval.dot(Cross));
	if (L > Box1R + Box2R) return false;//分離平面が存在しているので衝突していない

	//Box1YとBox2Xの外積が分離軸になるかの調査
	Cross = Box1Y.cross(Box2X);
	Box1R = std::fabs(Box1X.dot(Cross)) + std::fabs(Box1Z.dot(Cross));
	Box2R = std::fabs(Box2Y.dot(Cross)) + std::fabs(Box2Z.dot(Cross));
	L = std::fabs(Interval.dot(Cross));
	if (L > Box1R + Box2R) return false;//分離平面が存在しているので衝突していない

	//Box1YとBox2Yの外積が分離軸になるかの調査
	Cross = Box1Y.cross(Box2Y);
	Box1R = std::fabs(Box1X.dot(Cross)) + std::fabs(Box1Z.dot(Cross));
	Box2R = std::fabs(Box2X.dot(Cross)) + std::fabs(Box2Z.dot(Cross));
	L = std::fabs(Interval.dot(Cross));
	if (L > Box1R + Box2R) return false;//分離平面が存在しているので衝突していない

	//Box1YとBox2Zの外積が分離軸になるかの調査
	Cross = Box1Y.cross(Box2Z);
	Box1R = std::fabs(Box1X.dot(Cross)) + std::fabs(Box1Z.dot(Cross));
	Box2R = std::fabs(Box2X.dot(Cross)) + std::fabs(Box2Y.dot(Cross));
	L = std::fabs(Interval.dot(Cross));
	if (L > Box1R + Box2R) return false;//分離平面が存在しているので衝突していない

	//Box1ZとBox2Xの外積が分離軸になるかの調査
	Cross = Box1Z.cross(Box2X);
	Box1R = std::fabs(Box1X.dot(Cross)) + std::fabs(Box1Y.dot(Cross));
	Box2R = std::fabs(Box2Y.dot(Cross)) + std::fabs(Box2Z.dot(Cross));
	L = std::fabs(Interval.dot(Cross));
	if (L > Box1R + Box2R) return false;//分離平面が存在しているので衝突していない

	//Box1ZとBox2Yの外積が分離軸になるかの調査
	Cross = Box1Z.cross(Box2Y);
	Box1R = std::fabs(Box1X.dot(Cross)) + std::fabs(Box1Y.dot(Cross));
	Box2R = std::fabs(Box2X.dot(Cross)) + std::fabs(Box2Z.dot(Cross));
	L = std::fabs(Interval.dot(Cross));
	if (L > Box1R + Box2R) return false;//分離平面が存在しているので衝突していない

	//Box1ZとBox2Zの外積が分離軸になるかの調査
	Cross = Box1Z.cross(Box2Z);
	Box1R = std::fabs(Box1X.dot(Cross)) + std::fabs(Box1Y.dot(Cross));
	Box2R = std::fabs(Box2X.dot(Cross)) + std::fabs(Box2Y.dot(Cross));
	L = std::fabs(Interval.dot(Cross));
	if (L > Box1R + Box2R) return false;//分離平面が存在しているので衝突していない

	//分離平面が存在しないので衝突している
	return true;
}