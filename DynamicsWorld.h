/**
DynamicsWorldクラスは3次元仮想物理空間に関するクラスです\n
3次元仮想物理空間の作成にはOpen Dynamics Engine（略称：ODE）を使用しています．\n
本クラスのインスタンスは1世代ごとに生成し，その世代の評価時間が終了したら破棄する使い方を前提に作られています．\n
そのため，本クラスのメンバ変数の寿命は本クラスのインスタンスが作られたその1世代の間のみです．\n
本クラスのインスタンスは同時に2つ以上生成する場合を考慮していません．ですので本クラスのインスタンスを\n
同時に2つ以上生成しないでください．
*/

#pragma once

#include <Eigen\Core>
#include <Eigen\Geometry>
#include <GLFW\glfw3.h>
#include <ode\ode.h>
#include <Windows.h>
#include "Agent.h"


using namespace std;

class DynamicsWorld
{
private:
	/*! 仮想物理空間を生成する際に与えられるID*/
	dWorldID World;

	/*! 衝突計算用スペースを生成する際に与えられるID*/
	dSpaceID Space;

	/*! 地面が持つ衝突空間のID．地面は平面なので，この衝突ジオメトリも同様に平面で定義される*/
	dGeomID Ground;

	/*! ジョイントグループのID*/
	dJointGroupID ContactGroup;

	/*! 地面との衝突で生じる音源情報\n
	Sounds[0]：音源のx座標\n
	Sounds[1]：音源のy座標\n
	Sounds[2]：音源のz座標\n
	Sounds[3]：音量\n
	1ステップごとに内容は破棄され，更新される．1ステップごとに内容を破棄することを忘れないこと．*/
	vector<array<double, 4> > Sounds;

	/*! 衝突したボックスオブジェクトのdGeomIDを格納するvector\n
	1ステップごとにこの内容は破棄され，更新される．1ステップごとに内容を破棄することを忘れないこと．*/
	vector<dGeomID> CollisionBoxGeomIDs;

	/*! 地面や他のオブジェクトを検知したビームセンサーのビームのdGeomIDを格納するvector\n
	1ステップごとにこの内容は破棄され，更新される．1ステップごとに内容を破棄することを忘れないこと．*/
	vector<dGeomID> HitBeamSensorGeomIDs;

	/*! 地面や他のオブジェクトを検知したビームセンサーのビームの始点から検知した対象物までの距離が格納されるunordered_map\n
	キーはビームのdGeomIDで，値はビームの始点から検知した対象物までの距離である．\n
	地面や他のオブジェクトを検知していないビームは格納されない．\n
	地面や複数のオブジェクトとビームが交わった場合，最もビームの始点に近い対象物までの距離が格納される．\n
	1ステップごとにこの内容は破棄され，更新される．1ステップごとに内容を破棄することを忘れないこと．*/
	unordered_map<dGeomID, double> HitBeamSensorValues;

	/*! Agentクラスのインスタンスのvector*/
	vector <Agent> Creatures;

	/*! 開始時のエージェントの座標\n
	この世代のCreatures[0]から順に，この世代の評価開始時のヘッドコンポーネントの重心座標が格納される\n
	FirstAgentsPosition[n][0]：Agentクラスのインスタンスの配列のn個体目のヘッドコンポーネントの重心のx座標\n
	FirstAgentsPosition[n][1]：Agentクラスのインスタンスの配列のn個体目のヘッドコンポーネントの重心のy座標\n
	FirstAgentsPosition[n][2]：Agentクラスのインスタンスの配列のn個体目のヘッドコンポーネントの重心のz座標\n*/
	vector<array<double, 3> > FirstAgentsPosition;

	/*! 終了時のエージェントの座標\n
	この世代のCreatures[0]から順に，この世代の評価終了時のヘッドコンポーネントの重心座標が格納される\n
	LastAgentsPosition[n][0]：Agentクラスのインスタンスの配列のn個体目のヘッドコンポーネントの重心のx座標\n
	LastAgentsPosition[n][1]：Agentクラスのインスタンスの配列のn個体目のヘッドコンポーネントの重心のy座標\n
	LastAgentsPosition[n][2]：Agentクラスのインスタンスの配列のn個体目のヘッドコンポーネントの重心のz座標\n*/
	vector<array<double, 3> > LastAgentsPosition;

	/*! 物理演算のステップ数*/
	int Steps;

	double a, b;

	vector<double>MusicPosition;

	//Getter
	/*! @brief dWorldIDを返す
	@return dWorldID World */
	dWorldID GetWorldID() {
		return World;
	};

	/*! @brief dSpaceIDを返す
	@return dSpaceID Space*/
	dSpaceID GetSpaceID() {
		return Space;
	};

	/*! @brief dJointGroupIDを返す
	@return dJointGroupID ContactGroup*/
	dJointGroupID GetJointGroupID() {
		return ContactGroup;
	};

	/*! @brief 全てのエージェントの現在の座標が格納されたvectorを返す
	@return 全てのエージェントのヘッドコンポーネントの重心座標が格納されたvector\n
	AgentsPosition[n][0]：Agentクラスのインスタンスの配列のn個体目の現在のx座標\n
	AgentsPosition[n][1]：Agentクラスのインスタンスの配列のn個体目の現在のy座標\n
	AgentsPosition[n][2]：Agentクラスのインスタンスの配列のn個体目の現在のz座標\n*/
	vector<array<double, 3> > GetAgentsPosition();

	


	//Setter
	/*! @brief 衝突時の物理挙動のパラメータの設定
	@param dContact Contact[]：接触時の挙動を決定するための接触面の性質を定義する構造体
	@param int i：Contact[]の配列の大きさ\n
	実装では接触面の定義の仕方を定義1と定義2の2種類用意した．\n
	定義1はERP（ジョイント誤差修正パラメータ：Error Reduction Parameter）や\n
	CFM（拘束力混合パラメータ：Constraint Force Mixing）などを使用して物理挙動を定義する方法である\n
	定義2は定義1より単純で，反発係数や反発に必要な最低速度などから物理挙動を定義を定義している\n
	どちらが適しているかや，個々のパラメータなどは物理挙動を描画ウィンドウで確認しながら試行錯誤で決定する必要がある*/
	void SetContactParameter(dContact Contact[], int i);

	/*! @brief 地面と接触時に生成される音情報をセットする
	@param dContact Contact[]：音源座標や衝突深さが格納されている
	@param int i：Contact[]の配列の大きさ*/
	void SetSound(dContact Contact[], int i);

	/*! @brief エージェントの各パーツが知覚する音量をセットする
	@remarks
	現在の実装では距離による音の減衰は考慮されていません．\n
	そのため，全てのエージェントの全てのパーツが知覚する音の大きさは同一です．\n
	今後拡張する際は，この部分に音の減衰を考慮する関数を追加すると良いと思います．*/
	void SetAgentsPerceivedSounds();

	/*! @brief エージェントにビームセンサーで検出した物体までの距離をセットする*/
	void SetAgentsBeamSensorValues();

	/*! @brief エージェントの各パーツが持つタッチセンサーにタッチの有無をセットする */
	void SetAgentsBoxPartTouchSensor();

	/*! @brief 引数で与えられたdGeomIDがRayクラスのdGeomIDか判定する
	@param dGeomID Geom：判定したいdGeomID
	@return 引数に与えられたdGeomIDがRayクラスのdGeomIDであればtrueを返す．そうでなければfalseを返す*/
	bool IsRayGeomID(dGeomID Geom);

	/*! @brief 引数で与えられたdGeomIDがGroundのdGeomIDか判定する
	@param dGeomID Geom：判定したいdGeomID
	@return 引数に与えられたdGeomIDがGroundのdGeomIDであればtrueを返す．そうでなければfalseを返す*/
	bool IsGroundGeomID(dGeomID Geom);

	/*! @brief 数で与えられたdGeomIDがBoxクラスのdGeomIDか判定する
	@param dGeomID Geom：判定したいdGeomID
	@return 引数に与えられたdGeomIDがBoxクラスのdGeomIDであればtrueを返す．そうでなければfalseを返す*/
	bool IsBoxGeomID(dGeomID Geom);

	/*! @brief CollisionBoxGeomIDsが引数のdGeomIDを保持しているか判定する
	@param dGeomID Geom：判定したいdGeomID
	@return 引数に与えられたdGeomIDがCollisionBoxGeomIDsに登録されていればtrueを返す．そうでなければfalseを返す*/
	bool CollisionBoxGeomIDsContains(dGeomID Geom);

	/*! @brief CollisionBoxGeomIDsに引数のdGeomIDを登録する
	@param dGeomID Geom：CollisionBoxGeomIDsに登録したいdGeomID
	@sa IsBoxGeomID() でBoxクラスのdGeomIDか確認したのち登録する*/
	void RegisterCollisionBoxGeomIDs(dGeomID Geom);

	/*! @brief 仮想物理空間にエージェントを生成する
	@param vector<Genotype>& Genotypes：生成するエージェントの個体数分の遺伝型のvector*/
	void MakeAgents(vector<Genotype>& Genotypes);

	/*! @brief 仮想物理空間のエージェントを動かす */
	void MoveAgents();

	/*! @brief ビームセンサーに関する衝突の処理をする
	@param dGeomID o1：衝突する可能性のある物体1
	@param dGeomID o2：衝突する可能性のある物体2\n
	NearCallback() は物体同士が衝突する可能性があるときに呼び出されるコールバック関数である．\n
	そしてNearCallback()内にある本関数が呼び出された時点ではo1とo2は衝突する可能性があり互いに距離は近いが，\n
	必ずしも衝突するわけではないので注意が必要である．\n
	まず，o1とo2のどちらがRayクラスのdGeomIDか特定し，そのRayクラスのdGeomIDがHitBeamSensorValuesに登録されて\n
	いない場合，そのビームの始点から検知した物体までの距離をHitBeamSensorValuesに登録する．なお，Rayクラスの衝突空間は\n
	線分であり，線分は複数のオブジェクトと交わる可能性がある．あるビームが複数のオブジェクトと交わる場合，\n
	そのビームの始点からもっとも近い物体までの距離がHitBeamSensorValuesに登録されるように更新される*/
	void BeamSensorCollision(dGeomID o1, dGeomID o2);

	/*! @brief ボックスオブジェクトに関する衝突衝突の処理をする
	現在のステップにおいて衝突したBoxクラスのdGeomIDをCollisionBoxGeomIDsに登録する
	@param dGeomID o1：衝突する可能性のある物体*/
	void BoxCollision(dGeomID o1);

	/*! @brief 衝突演算のコールバック

	オブジェクト同士が衝突する"可能性"がある時に"自動的"に呼び出される.\n
	そのため，呼び出されたo1とo2が必ずしも衝突するわけではないことに注意が必要である．
	実際に衝突したかの有無は接触点の数から判定する．
	@param dGeomID o1：衝突する可能性のある物体1
	@param dGeomID o2：衝突する可能性のある物体2
	@param void *data：DynamicsWorldクラスのthisポインタ\n
	ODEの仕様上，本関数はstaicでないとコールバック関数に設定できない．\n
	staticの関数内ではstaticのメンバ変数やメンバ関数しか見れない．\n
	しかし，多くの値をstaticにすることはオブジェクト指向のカプセル化の観点からよくない．\n
	そこでthisポインタを引数にとることで，このthisポインタを介して，staticの関数でありながら，\n
	staticでないメンバ変数やメンバ関数にアクセスできる．	*/
	static void NearCallback(void *data, dGeomID o1, dGeomID o2);


public:
	/*! @brief コンストラクタ

	コンストラクタ内で仮想物理環境のパラメータの設定やエージェントの生成を行う
	@param vector<Genotype> Genotypes：生成するエージェントの個体数分の遺伝型のvector*/
	DynamicsWorld(vector<Genotype> Genotypes);

	/*! @brief デストラクタ

	FinalizeDynamicsWorld()は仮想物理環境を終了させる関数であるが，この関数の実装をデストラクタ内で\n
	行わなかった理由は，デストラクタが呼び出されるタイミングが明示的でないためである．\n
	DynamicsWorldクラスのインスタンスは同時に2つ以上存在することを考慮していないので，\n
	タイミングが明示的でないデストラクタ内で仮想物理環境を終了させる関数は書けない．\n
	よってデストラクタ内の実装はなし*/
	~DynamicsWorld();

	/*! @brief 物理演算のステップを1進める

	dSpaceCollide() で現ステップの衝突判定を行い，現ステップでの以下の情報\n
	- Sounds
	- CollisionBoxGeomIDs
	- HitBeamSensorGeomIDs
	- HitBeamSensorValues\n
	を更新する．\n
	そのため1ステップ前のこれらの情報を消す必要がある．\n
	よって，以下の\n
	- Sounds.clear()
	- CollisionBoxGeomIDs.clear()
	- HitBeamSensorGeomIDs.clear()
	- HitBeamSensorValues.clear() \n
	をdSpaceCollide() を呼び出す前に必ず行う
	*/
	void OdeStepsUpdate();

	//Getter
	/*! @brief 物理演算のステップ数を返す
	@return Steps*/
	int GetSteps()
	{
		return Steps;
	}

	//エージェントのGeomIDsを返す
	/*! @brief 全てのエージェントについて，各エージェントが持つ全てのパーツのdGeomIDを返す

	Phenotype::GeomIDsはエージェント自身が持つの全てのパーツのdGeomIDを展開順に格納したvectorである\n
	このGeomIDsを全エージェント分つなげた2次元vectorにして返す\n
	@return 全エージェント分のGeomIDs*/
	vector<vector<dGeomID> > GetAgentsGeomIDs();

	/*! @brief 全てのエージェントについて，各エージェントが持つ全てのビームのdGeomIDを返す
	@return 全てのエージェントについて，各エージェントが持つ全てのビームのdGeomIDが格納されたvecctor*/
	vector<vector<dGeomID> > GetAgentsBeamSensorGeomIDs();

	/*! @brief HitBeamSensorValuesを返す
	@return HitBeamSensorValues*/
	unordered_map<dGeomID, double> GetHitBeamSensorValues() {
		return HitBeamSensorValues;
	}

	/*! @brief 現ステップにおける全ての音源情報が格納されたvectorを返す
	return Sounds*/
	vector<array<double, 4> > GetSounds() {
		return Sounds;
	};

	/*! @brief 開始時のエージェントの座標を返す
	@return 開始時（すなわちSteps==0）の時の全てのエージェントのヘッドコンポーネントの重心座標が格納されたvectorを返す*/
	vector<array<double, 3> > GetFirstAgentsPosition()
	{
		return FirstAgentsPosition;
	}

	/*! @brief 終了時のエージェントの座標を返す
	@return 終了時（すなわちSteps==EvaluationTime）の時の全てのエージェントのヘッドコンポーネントの重心座標が格納されたvectorを返す*/
	vector<array<double, 3> > GetLastAgentsPosition()
	{
		return LastAgentsPosition;
	}

	//音源の座標を返す
	vector <double>GetMusicPosition() {
		return MusicPosition;
	}

	double music(double dis, double sound) {
		double sound2 = -dis/100 + 1;
		if (sound2 < 0) sound2 = 0;
		return sound * sound2;
	}

	/*! @brief エージェント数を返す
	@return エージェント数*/
	int GetCreaturesVectorSize() {
		return Creatures.size();
	}

	/*! @brief 仮想物理空間を終了する
	@sa ~DynamicsWorld() */
	void FinalizeDynamicsWorld();


};
