#include "HeadNode.h"

//コンストラクタ
HeadNode::HeadNode() : BaseNode(HeadNodeSensorType) {
}

HeadNode::HeadNode(const std::array<double, 3> Sides, const std::array<double, 3> EyeDirection,
	const NeuralNetwork NN, const std::vector<Connection> ConnectionVector)
	: BaseNode(Sides, EyeDirection, HeadNodeSensorType, NN, ConnectionVector) {
}

//デストラクタ
HeadNode::~HeadNode() {
}