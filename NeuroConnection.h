#pragma once
#include <cereal/access.hpp>
#include "RandomOperation.h"

/*!
@brief ニューロンの間の繋がりを表すニューロコネクションのクラス
@details ニューロコネクションによってニューロンの出力がどのように伝播するかが決まる。
*/
class NeuroConnection {
public:
	/*! @brief シリアライズに対応する為のコンストラクタ。何も行わない。*/
	NeuroConnection() = default;

	/*! @brief 通常のコンストラクタ
	@param[out] NeuroNumber 接続先のニューロンのインデックス
	@details ニューロコネクションの重みは[-1.0, 1.0]でランダムに決定される
	*/
	NeuroConnection(const int NeuroNumber);

	/*! @brief 任意のニューロコネクションを作成するコンストラクタ
	@param[in] Weight ニューロコネクションの重み
	@param[out] NeuroNumber 接続先のニューロンのインデックス
	@details ニューロコネクションの持つ全ての情報を与えてニューロコネクションを作成する。\n
	範囲外の値があった場合は例外が発生し強制終了する。
	*/
	NeuroConnection(const double Weight, const int NeuroNumber);

	/*! @brief デストラクタ */
	virtual ~NeuroConnection();

	/*! @brief ニューロコネクションの重みを返す
	@return double Weight*/
	double GetWeight() { return Weight; }

	/*! @brief 接続先のニューロンのインデックスを返す
	@return int NeuroNumber*/
	int GetNeuroNumber() { return NeuroNumber; }

	/*! @brief ニューロコネクションの持つ各情報に対して突然変異を発生させる*/
	virtual void Mutation();

	/*! @brief 接続先のニューロンのインデックスから1つ減じる
	@details ニューラルネットワークの突然変異でノーマルニューロンが削除された際、\n
	削除されたノーマルニューロンのインデックスより大きいインデックスを持つ場合\n
	インデックスを１つ前にずらす必要がある。\n
	そのためにインデックスを１つ減らす関数が必要になる。
	*/
	void DecreaseNeuronNumber();

private:
	//! ニューロコネクションの重み
	double Weight;

	/*! @brief 接続先のニューロンのインデックス
	@details 正の時はノーマルニューロンベクトルのインデックス、負の時はエフェクターニューロンベクトルのインデックスを表す*/
	int NeuroNumber;

	friend class cereal::access;
	template<class Archive>
	/*! @brief シリアライズ
	@details ニューロコネクションの持つ全ての情報をシリアライズする関数*/
	void serialize(Archive& archive) {
		archive(CEREAL_NVP(Weight), CEREAL_NVP(NeuroNumber));
	}

};