#pragma once
#include "EffectorNeuron.h"
#include "Sensor.h"
#include "SensorNeuron.h"

/*! @brief ノードの持つニューラルネットワークのクラス
@details センサーニューロン、ノーマルニューロン、エフェクターニューロンを持ち、発火と伝播を行う。\n
また、結合しているコンポーネント間の情報のやりとりも行う
*/
class NeuralNetwork {
public:
	/*! @brief 通常のコンストラクタ。
	@param[in] ST センサータイプ
	@details 与えられたセンサータイプを基にセンサーニューロンを作成する。*/
	NeuralNetwork(const unsigned int ST);

	/*! @brief 任意のニューラルネットワークを作成するコンストラクタ
	@param[in] SensorOnOff センサーのオンオフ
	@param[in] SensorNeuronVector センサーニューロンの配列
	@param[in] NormalNeuronVector ノーマルニューロンの配列
	@details ニューラルネットワークの持つ全ての情報を与えてニューラルネットワークを作成する。
	*/
	NeuralNetwork(const std::map<unsigned int, bool> SensorOnOff,
		const std::vector<SensorNeuron> SensorNeuronVector, const std::vector<NormalNeuron> NormalNeuronVector);

	/*! @brief デストラクタ */
	virtual ~NeuralNetwork();

	/*! @brief センサーのオンオフを返す
	@return std::map<unsigned int, bool> SensorOnOff*/
	std::map<unsigned int, bool> GetSensorOnOff() { return SensorOnOff; }

	/*! @brief センサーニューロンの配列を返す
	@return std::vector<SensorNeuron> SensorNeuronVector*/
	std::vector<SensorNeuron> GetSensorNeuronVector() { return SensorNeuronVector; }

	/*! @brief ノーマルニューロンの配列を返す
	@return std::vector<NormalNeuron> NormalNeuronVector*/
	std::vector<NormalNeuron> GetNormalNeuronVector() { return NormalNeuronVector; }

	/*! @brief エフェクターニューロンの配列を返す
	@return std::vector<EffectorNeuron> EffectorNeuronVector*/
	std::vector<EffectorNeuron> GetEffectorNeuronVector() { return EffectorNeuronVector; }

	/*! @brief ジョイントに掛けるトルクの値を返す
	@return double Torque*/
	double GetJointTorque() { return EffectorNeuronVector[0].GetOutput(); }

	/*! @brief 接続している親コンポーネントのニューラルネットワークへの値を返す
	@return double InformationForParent*/
	double GetInformationForParent() { return EffectorNeuronVector[1].GetOutput(); }

	/*! @brief 接続している子コンポーネントのニューラルネットワークへの値を返す
	@return double InformationForChild*/
	double GetInformationForChild() { return EffectorNeuronVector[2].GetOutput(); }

	/*! @brief ニューラルネットワーク内で伝播を行う
	@param[in] SensorValues センサーからの値
	@details センサーからの値を受け取り、ニューロン間で値を伝播する。\n
	出力値を更新しないように発火とは別の操作となる。*/
	void Propagation(const std::map<unsigned int, std::vector<double>> SensorValues);

	/*! @brief ニューラルネットワークの全ニューロンを発火する*/
	void Fire();

	/*! @brief ニューラルネットワークの持つ各情報に対して突然変異を発生させる*/
	virtual void Mutation();

private:
	/*! @brief センサーのオンオフをランダムに決定する
	@param[in] ST センサータイプ
	@details　センサータイプを基に対応するセンサーのオンオフをランダムに決定する。
	*/
	void MakeSensorOnOff(const unsigned int ST);

	/*! @brief センサーニューロンを作成する
	@param[in] ST センサータイプ
	@details　センサータイプを基にセンサーニューロンを作成する。
	*/
	void MakeSensorNeurons(const unsigned int ST);

	/*! @brief センサーニューロンを追加する
	@param[in] ST センサーニューロンの数
	@details　指定された数だけセンサーニューロンを配列に追加する。
	*/
	void AddSensorNeuron(const unsigned int Number);

	/*! @brief エフェクターニューロンを作成する*/
	void MakeEffectorNeurons();

	/*! @brief センサーの値を伝播
	@param[in] SensorValues センサーからの値
	@details センサーからの値を対応するセンサーニューロンに伝播する。\n
	もしあるセンサーがオフである場合そのセンサーからの値は0となり無視される。
	*/
	void PropagateSensorValues(const std::map<unsigned int, std::vector<double>> SensorValues);

	/*! @brief ニューロン配列内のニューロンの値の伝播
	@param[in] NeuronVector ニューロン配列
	@details ノーマルニューロンの配列とセンサーニューロンの配列内のニューロンの値を伝播する。
	*/
	template <typename T> void PropagateNeuronVector(std::vector<T> NeuronVector);

	/*! @brief 突然変異によってノーマルニューロンを1つ追加
	@details 突然変異が発生した場合ノーマルニューロンを１つ追加する。
	*/
	void AddNormalNeuron();

	/*! @brief 突然変異によってノーマルニューロンを1つ削除
	@details 突然変異が発生した場合、今保持しているノーマルニューロンからランダムに1つ選び削除する。
	*/
	void DeleteNormalNeuron();

	/*! @brief センサーのオンオフに突然変異を加える
	@details それぞれのセンサーに関して突然変異が起きたかを確認し、もし発生した場合オンオフを反転する。
	*/
	void MutateSensorOnOff();

	//! センサーのオンオフ
	std::map<unsigned int, bool> SensorOnOff;

	//! センサーニューロンの配列
	std::vector<SensorNeuron> SensorNeuronVector;

	//! ノーマルニューロンの配列
	std::vector<NormalNeuron> NormalNeuronVector;

	//! エフェクターニューロンの配列
	std::vector<EffectorNeuron> EffectorNeuronVector;

	friend class cereal::access;
	/*! @brief シリアライズ
	@details ニューラルネットワークの持つ全ての情報をシリアライズする関数*/
	template<class Archive>
	void serialize(Archive& archive) {
		archive(CEREAL_NVP(SensorOnOff),
			CEREAL_NVP(SensorNeuronVector),
			CEREAL_NVP(NormalNeuronVector));
	}

};