#include "GenotypeArray.h"

//コンストラクタ
GenotypeArray::GenotypeArray(const std::vector<Genotype> GenotypeVector) {
	//メンバ変数を初期化
	this->GenotypeVector = GenotypeVector;
}

//デストラクタ
GenotypeArray::~GenotypeArray() {
}

//演算子[]を定義
Genotype GenotypeArray::operator[] (unsigned int i) {
	try {
		//範囲外にアクセスしようとした場合例外を投げる
		if (i >= GenotypeVector.size()) throw std::out_of_range("Out of range");
	}
	catch (std::out_of_range &ValueError) {
		std::cerr << ValueError.what() << std::endl;
		system("pause");
		exit(EXIT_FAILURE);
	}

	return GenotypeVector[i];
}

//突然変異
void GenotypeArray::Mutation() {
	for (auto& g : GenotypeVector) {
		g.Mutation();
	}
}

//２つの遺伝子型を交叉
Genotype GenotypeArray::CrossOver(const unsigned int Parent1, const unsigned int Parent2, const unsigned int CrossPoint) {
	Genotype G1 = GenotypeVector[Parent1]; //親1の遺伝子型
	Genotype G2 = GenotypeVector[Parent2]; //親2の遺伝子型

	HeadNode HN = G1.GetHeadNode(); //ヘッドノード
	std::vector<NormalNode> G1NormalNodes = G1.GetNormalNodeVector(); //親1のノーマルノード
	std::vector<NormalNode> G2NormalNodes = G2.GetNormalNodeVector(); //親2のノーマルノード

	//前半部分(最初から交叉するポイントの直前まで）をParent1からコピー
	std::vector<NormalNode> Former;
	for (unsigned int i = 0; i < CrossPoint; i++) {
		Former.push_back(G1NormalNodes[i]);
	}

	//後半部分(交叉するポイント以降)をParent2からコピー
	std::vector<NormalNode> Latter;
	for (unsigned int i = CrossPoint; i < G2NormalNodes.size(); i++) {
		Latter.push_back(G2NormalNodes[i]);
	}

	//ノーマルノードの前半部分と後半部分を結合
	std::vector<NormalNode> NormalNodeVector = ConcatVector(Former, Latter);

	return Genotype(HN, NormalNodeVector);
}