#pragma once
#include "Box.h"
#include "Ray.h"
#include "HingeJoint.h"

class PhysicalPart
{
public:
	PhysicalPart();
	~PhysicalPart();

	//仮想物理空間上にパーツを生成
	void MakePart(const dWorldID World, const dSpaceID Space, const std::array<double, 3> Sides,
		const std::array<double, 3> Position, const std::array<double, 3> AttitudeAngle,
		const std::array<double, 3> BeamAngle, std::vector <dGeomID>& GeomIDs);

	//仮想物理空間上にパーツをジョイントで接続する
	void JointAttach(const dWorldID World, const dBodyID ParentBody,
		const std::array<double, 3>JointPosition, const std::array<double, 3>JointAxis);

	//BoxPartのBodyIDを取得する
	dBodyID GetBoxPartBodyID() {
		return BoxPart.GetBodyID();
	}

	//BoxPartのGeomIDを取得する
	dGeomID GetBoxPartGeomID() {
		return BoxPart.GetGeomID();
	}

	//知覚した音の値をセット
	void SetPerceivedSoundVolume(const double SumSounds) {
		PerceivedSoundVolume = SumSounds;
	}

	//知覚した音量を返す
	double GetPerceivedSoundVolume() {
		return PerceivedSoundVolume;
	}

	//ビームセンサーが検知した物体までの距離をセットする
	void SetBeamSensorValue(double Distance) {
		BeamSensor.SetMeasuredDistance(Distance);
	}

	//ボックスパーツのタッチセンサーにタッチの有無をセット
	void SetBoxPartTouchSensor(bool HasTouched) {
		BoxPart.SetTouchSensor(HasTouched);
	}

	//ボックスパーツのタッチセンサーのタッチの有無を返す
	bool GetBoxPartTouchSensor() {
		return BoxPart.GetTouchSensor();
	}

	//ヒンジの軸にトルクを加える
	void AddTorque(const double Torque);

	//ジョイントの角度を返す
	double GetJointAngle() {
		return MyHingeJoint.GetJointAngle();
	}

	//ビームセンサーのGeomIDを返す
	dGeomID GetBeamSensorGeomID() {
		return BeamSensor.GetGeomID();
	}

	//ビームセンサーの計測距離を返す
	double GetBeamSensorMeasuredDistance() {
		return BeamSensor.GetMeasuredDistance();
	}

private:
	Box BoxPart;
	HingeJoint MyHingeJoint;
	Ray BeamSensor;

	double PerceivedSoundVolume;//知覚した音の大きさ

};

