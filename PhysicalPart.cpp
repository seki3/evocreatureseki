#include "PhysicalPart.h"

PhysicalPart::PhysicalPart()
{
}


PhysicalPart::~PhysicalPart()
{
}

//仮想物理空間上にパーツを生成
void PhysicalPart::MakePart(const dWorldID World, const dSpaceID Space,
	const std::array<double, 3> Sides, const std::array<double, 3> Position,
	const std::array<double, 3> AttitudeAngle, const std::array<double, 3> BeamAngle,
	std::vector <dGeomID>& GeomIDs)
{
	BoxPart.MakeBox(World, Space, Sides, Position, AttitudeAngle, GeomIDs);
	BeamSensor.CreateRay(Space, BoxPart.GetBodyID(), Position, BeamAngle);
}

//仮想物理空間上にパーツをジョイントで接続する
void PhysicalPart::JointAttach(dWorldID World, dBodyID ParentBody, std::array<double, 3>JointPosition, const std::array<double, 3>JointAxis)
{
	MyHingeJoint.JointAttach(World, ParentBody, BoxPart.GetBodyID(), JointPosition, JointAxis);
}

//ヒンジの軸にトルクを加える
void PhysicalPart::AddTorque(const double Torque)
{
	MyHingeJoint.AddTorque(Torque);
}
