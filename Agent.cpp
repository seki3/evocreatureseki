#include "Agent.h"

Agent::Agent(const Genotype G) : MyPhenotype(G) {
}

Agent::~Agent() {
}

//仮想物理空間上に実体としての表現型を生成
void Agent::MakeDynamicsPhenotype(dWorldID World, dSpaceID Space, const std::array<double, 3> HeadPosition) {
	MyPhenotype.MakeBody(World, Space, HeadPosition);
}

//エージェントの行動
void Agent::Behave() {
	MyPhenotype.MoveComponents();
}

//GeomIDsを表示
void Agent::PrintGeomIDs() {
	MyPhenotype.PrintGeomIDs();
}

//各パーツが知覚する音の値をセットする
void Agent::SetPerceivedSounds(const std::vector<std::array<double, 4> > Sounds) {
	MyPhenotype.SetPerceivedSounds(Sounds);
}

//各ビームセンサーが検知した物体までの距離をセットする
void Agent::SetBeamSensorValues(const std::unordered_map<dGeomID, double> HitBeamSensorValues) {
	MyPhenotype.SetBeamSensorValues(HitBeamSensorValues);
}

//各ボックスパーツのタッチセンサーにタッチの有無をセットする
void Agent::SetBoxPartTouchSensor(const std::vector<dGeomID> CollisionBoxGeomIDs) {
	MyPhenotype.SetBoxPartTouchSensor(CollisionBoxGeomIDs);
}

//引数で与えられたGeomを持っていればtrue,持っていなければfalseを返す
bool Agent::HasTheGeomID(dGeomID Geom) {
	return(MyPhenotype.HasTheGeomID(Geom));
}
