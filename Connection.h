﻿#pragma once
#define _USE_MATH_DEFINES
#include <cereal/access.hpp>
#include <math.h>
#include "RandomOperation.h"

/*!
@brief ノードの間の繋がりを表すコネクションのクラス
@details コネクションは表現型に展開される際のジョイントと接続先のノードの大きさを決定する。\n
各ノードはコネクションを０個以上持つ。
*/
class Connection {
public:
	/*! @brief シリアライズに対応する為のコンストラクタ。何も行わない。*/
	Connection() = default;

	/*! @brief 通常のコンストラクタ
	@param[out] NormalNodeIndex 接続先のノーマルノードのインデックス
	@details 接続先以外のコネクションに関する情報はランダムに決定される
	*/
	Connection(const unsigned int NormalNodeIndex);

	/*! @brief 任意のコネクションを作成するコンストラクタ
	@param[in] JointAttachVector ジョイントの接続位置
	@param[in] JointAxisVector ジョイントの可動方向
	@param[in] AttitudeAngle ノードの姿勢
	@param[out] Scale ノードの大きさ
	@param[in] 再帰終了時のみ有効かどうか
	@param[out] NormalNodeIndex 接続先のノーマルノードのインデックス
	@details コネクションの持つ全ての情報を与えてコネクションを作成する。\n
	範囲外の値があった場合は例外が発生し強制終了する。
	*/
	Connection(const std::array<double, 2> JointAttachVector, const std::array<double, 2> JointAxisVector,
		const std::array<double, 3> AttitudeAngle, const double Scale, const bool RecursiveEnd, const unsigned int NormalNodeIndex);

	/*! @brief デストラクタ */
	virtual ~Connection();

	/*! @brief ジョイント接続ベクトルを返す
	@return std::array<double, 2> JointAttachVector*/
	std::array<double, 2> GetJointAttachVector() { return JointAttachVector; }

	/*! @brief ジョイント軸ベクトルを返す
	@return std::array<double, 2> JointAxisVector*/
	std::array<double, 2> GetJointAxisVector() { return JointAxisVector; }

	/*! @brief 姿勢角を返す
	@return std::array<double, 3> AttitudeAngle*/
	std::array<double, 3> GetAttitudeAngle() { return AttitudeAngle; }

	/*! @brief スケールを返す
	@return double Scale*/
	double GetScale() { return Scale; }

	/*! @brief 再帰終了時フラグを返す
	@return bool RecursiveEnd*/
	bool GetRecursiveEnd() { return RecursiveEnd; }

	/*! @brief ノーマルノードのインデックスを返す
	@return unsigned int NormalNodeIndex*/
	unsigned int GetNormalNodeIndex() { return NormalNodeIndex; }

	/*! @brief コネクションの持つ各情報に対して突然変異を発生させる*/
	virtual void Mutation();

	/*! @brief ノーマルノードのインデックスから1つ減じる
	@details 遺伝子型の突然変異でノーマルノードが削除された際、\n
	削除されたノーマルノードのインデックスより大きいインデックスを持つ場合\n
	インデックスを１つ前にずらす必要がある。\n
	そのためにインデックスを１つ減らす関数が必要になる
	*/
	void DecreaseIndex();

private:
	//! ジョイントがどこに接続するかを示すジョイント接続ベクトル
	std::array<double, 2> JointAttachVector;

	//! ジョイントがどの方向に可動するかを示すジョイント軸ベクトル
	std::array<double, 2> JointAxisVector;

	//! 接続した際のノードの姿勢を決定する姿勢角
	std::array<double, 3> AttitudeAngle;

	//! 接続先のノードの大きさを決定するスケール
	double Scale;

	//! このコネクションが再帰終了時のみ有効化どうかを決定する再帰的終了時フラグ
	bool RecursiveEnd;

	//! コネクションが接続してるノーマルノードのインデックス
	unsigned int NormalNodeIndex;

	friend class cereal::access;
	/*! @brief シリアライズ
	@details コネクションの持つ全ての情報をシリアライズする関数*/
	template<class Archive>
	void serialize(Archive& archive) {
		archive(CEREAL_NVP(JointAttachVector),
			CEREAL_NVP(JointAxisVector),
			CEREAL_NVP(AttitudeAngle),
			CEREAL_NVP(Scale),
			CEREAL_NVP(RecursiveEnd),
			CEREAL_NVP(NormalNodeIndex));
	}

};