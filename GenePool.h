#pragma once
#include "GenotypeArray.h"

/*! @brief 遺伝子型配列を持ち、遺伝的操作を行うクラス*/
class GenePool {
public:
	/*! @brief 通常のコンストラクタ
	@details 個体数分遺伝子型を作成し遺伝子型配列とする。
	*/
	GenePool();

	/*! @brief 任意の遺伝子プールを作成するコンストラクタ
	@param[in] GA 遺伝子型配列
	@details 任意の遺伝子型配列から遺伝子プールを作成する
	*/
	GenePool(const GenotypeArray GA);

	/*! @brief デストラクタ*/
	virtual ~GenePool();

	/*! @brief 遺伝子型配列を返す
	@return GenotypeArray GA*/
	GenotypeArray GetGenotypeArray() { return GA; }

	/*! @brief 遺伝的操作を行う
	@param[in] Fitness 各個体の適応度がはいった配列
	@details 交叉確率に基づき交叉するかどうかが決定する。\n
	交叉する場合は適応度に基づき2つの親個体を選び交叉を行う。\n
	交叉しない場合は適応度に基づき1個体を選びそれをコピーする。\n
	その後、交叉の有無にかかわらず突然変異が発生する。\n
	*/
	void GeneticOperation(const std::vector<double> Fitness);

private:
	/*! @brief 初期個体群を作成する関数*/
	GenotypeArray MakeInitialPopulation();

	//! 遺伝子型配列
	GenotypeArray GA;

	friend class cereal::access;
	/*! @brief シリアライズ
	@details 遺伝子型配列をシリアライズする関数*/
	template<class Archive>
	void serialize(Archive& archive) {
		archive(cereal::make_nvp("GenotypeArray", GA));
	}

};