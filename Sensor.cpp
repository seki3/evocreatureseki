#include "Sensor.h"

namespace SensorType {
	const unsigned int Joint = 0b0001;
	const unsigned int Touch = 0b0010;
	const unsigned int Sound = 0b0100;
	const unsigned int Eye = 0b1000;
}

const std::map<const unsigned int, const unsigned int> SensorNeuronNumber{
	{
		SensorType::Joint, 1
	},
	{
		SensorType::Touch, 1
	},
	{
		SensorType::Sound, 1
	},
	{
		SensorType::Eye, 1
	}
};