#include "Camera.h"

Camera::Camera()
{
	// マウスボタンの状態の初期化
	MouseButtonState = IsNotDragging;
	// カメラの位置と向きの初期化
	Position[0] = -3.0;
	Position[1] = 2.5;
	Position[2] = 3.5;
	RollPitchYaw[0] = 0.0;
	RollPitchYaw[1] = -35.0;
	RollPitchYaw[2] = -45.0;
}

// マウスボタンがドラッグ状態かどうかを管理
void Camera::SetMouseButtonState(int Button, int Action, double Xcoordinate, double Ycoordinate)
{
	std::cout << "@SetMouseButtonState, Button=" << Button << ", Action=" << Action << std::endl;
	if (Action == MouseDown) {
		// ドラッグ開始点を記録
		PreviousMouseCoordinate[0] = Xcoordinate;
		PreviousMouseCoordinate[1] = Ycoordinate;

		// ドラッグに関するフラグの処理．クリック時も一瞬ドラッグされたと解釈する．
		switch (Button){ 
		case LeftButton:
			MouseButtonState = LeftButtonIsDragging;
			break;

		case RightButton:
			MouseButtonState = RightButtonIsDragging;
			break;

		case MiddleButton:
			MouseButtonState = MiddleButtonIsDragging;
			break;
		}
	}else if (Action == MouseUp){
		MouseButtonState = IsNotDragging;
		std::cout << "ドラッグ終了" << std::endl;
	}
}

// カメラの操作
void Camera::Motion(int Mode, double Width, double Height, double Xcoordinate, double Ycoordinate, double ScrollingIncrement)
{
	double DeltaX = Xcoordinate - PreviousMouseCoordinate[0];// x軸方向のマウスカーソルの移動量の変位
	double DeltaY = Ycoordinate - PreviousMouseCoordinate[1];// y軸方向のマウスカーソルの移動量の変位
	double Side = 0.01f * DeltaX;           // 人間が使いやすい値になるように係数をかけて補正
	double Foward = 0.15*ScrollingIncrement;// 人間が使いやすい値になるように係数をかけて補正

	// マウスホイール回転時にマウスカーソルの位置に応じてカメラが水平方向に移動するために補正角度を計算する
	double NormalizedXcoordinate = (Xcoordinate - Width / 2) / (Width / 2); // マウスカーソルx軸方向の値を-1から1に正規化
	double CorrectionAngle = ScrollingIncrement* 30 * NormalizedXcoordinate;// マウスホイールの正方向の回転と負の方向の回転の整合をとるためにScrollingIncrementをかけている．-30°から30°に正規化

	double S = sin((RollPitchYaw[2] - CorrectionAngle) * M_PI / 180);
	double C = cos((RollPitchYaw[2] - CorrectionAngle) * M_PI / 180);

	if (Mode == LeftButtonIsDragging) {
		RollPitchYaw[2] += DeltaX * 0.5f;
		RollPitchYaw[1] += DeltaY * 0.5f;
		std::cout << "@Motion Mode == LeftButtonIsDragging" << std::endl;
	}
	else if (Mode == MouseWheelIsRotating) {
		Position[0] += -S*Side + C*Foward;// カメラ座標系でのx方向の移動量
		Position[1] += C*Side + S*Foward; // カメラ座標系でのy方向
	}
	else if(Mode == MiddleButtonIsDragging){
		Position[0] += -S*Side;
		Position[1] += C*Side;
		Position[2] += 0.01 * DeltaY;
	}
	
	// ロール角，ピッチ角，ヨー角は-180°から180°内で表現
	for (int i = 0; i<3; i++) {
		while (RollPitchYaw[i] > 180) RollPitchYaw[i] -= 360;
		while (RollPitchYaw[i] < -180) RollPitchYaw[i] += 360;
	}

	// マウスカーソルの座標を保存
	PreviousMouseCoordinate[0] = Xcoordinate;
	PreviousMouseCoordinate[1] = Ycoordinate;
}

// カメラの位置と向きの更新	
void Camera::CameraUpdate()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotated(90, 0, 0, 1);
	glRotated(90, 0, 1, 0);
	glRotated(RollPitchYaw[0], 1, 0, 0); // ロール
	glRotated(RollPitchYaw[1], 0, 1, 0); // ピッチ
	glRotated(-RollPitchYaw[2], 0, 0, 1);// ヨー
	glTranslated(-Position[0], -Position[1], -Position[2]);
}

Camera::~Camera()
{
}
