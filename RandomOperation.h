#pragma once
#include <random>
#include "Function.h"
#include "Parameter.h"
#include "Singleton.h"

/*! @brief 乱数処理を行うクラス*/
class RandomOperation {
public:
	/*! @brief コンストラクタ
	@details 乱数シードからメルセンヌ・ツイスタを初期化する。
	*/
	RandomOperation();

	/*! @brief デストラクタ*/
	virtual ~RandomOperation();

	/*! @brief 任意の範囲の乱数を生させる(int)
	@param[in] Min 乱数の範囲の下限
	@param[in] Max 乱数の範囲の上限
	@return int RandomNumber
	@details Min以上Max以下で一様な乱数を生成する。
	*/
	int MakeRandomValue(const int Min, const int Max);

	/*! @brief 任意の範囲の乱数を生させる(double)
	@param[in] Min 乱数の範囲の下限
	@param[in] Max 乱数の範囲の上限
	@return double RandomNumber
	@details Min以上Max以下で一様な乱数を生成する。
	*/
	double MakeRandomValue(const double Min, const double Max);

	/*! @brief 任意の範囲の乱数で初期化した配列を生成する
	@param[in] Min 乱数の範囲の下限
	@param[in] Max 乱数の範囲の上限
	@return std::array<T, N> RandomArray
	@details Min以上Max以下の一様な乱数で配列を初期化する
	*/
	template < typename  T, unsigned int  N >
	std::array<T, N> MakeRandomValue(const T Min, const T Max) {
		std::array<T, N> RandomArray;

		//配列のそれぞれの要素をランダムに決定
		for (auto& ra : RandomArray) {
			ra = MakeRandomValue(Min, Max);
		}

		return RandomArray;
	}

	/*! @brief trueまたはfalseをランダムに生成する
	@return bool RandomBool*/
	bool MakeRandomBoolValue();

	/*! @brief -1または+1をランダムに生成する
	@return int RandomPlusMinusOne*/
	int MakeRandomPlusMinusOne();

	/*! @brief スケールした微小値をランダムに生成する
	@param[in] Scale 微小値をどれだけスケールするか
	@return double InfinitesimalValue
	@details　[-0.001, 0.001]の一様乱数を生成し、Scaleを掛ける
	*/
	double MakeInfinitesimalValue(const double Scale);

	/*! @brief 確率的事象が起きたかどうか
	@param[in] Probability 事象の起きる確率
	@return bool IsEventOccured
	@details 与えられた確率を基に事象が起きたかどうかを返す
	*/
	bool IsProbabilityEventOccurred(const double Probability);

	/*! @brief 突然変異が起きたかどうかを返す
	@return bool IsMutationOccured
	*/
	bool IsMutationOccurred();

	/*! @brief 突然変異を加える(int)
	@param[in] Value 元の値
	@param[in] Min 値の取りうる最小値
	@param[in] Max 値の取りうる最大値
	@return int MutatedValue
	@details 突然変異が発生した場合元の値をランダムにプラスマイナス1する。\n
	値が範囲外になった場合調整を行う。
	*/
	int AddMutation(int Value, const int Min, const int Max);

	/*! @brief 突然変異を加える(int)
	@param[in] Value 元の値
	@param[in] Min 値の取りうる最小値
	@return int MutatedValue
	@details 突然変異が発生した場合元の値をランダムにプラスマイナス1する。\n
	値が範囲外になった場合調整を行う。
	*/
	int AddMutation(int Value, const int Min);

	/*! @brief 突然変異を加える(double)
	@param[in] Value 元の値
	@param[in] Min 値の取りうる最小値
	@param[in] Max 値の取りうる最大値
	@return double MutatedValue
	@details 突然変異が発生した場合元の値にスケールした微小値を加える。\n
	値が範囲外になった場合調整を行う。
	*/
	double AddMutation(double Value, const double Min, const double Max);

	/*! @brief 突然変異を加える(double)
	@param[in] Value 元の値
	@param[in] Min 値の取りうる最小値
	@return double MutatedValue
	@details 突然変異が発生した場合元の値に微小値を加える。\n
	値が範囲外になった場合調整を行う。
	*/
	double AddMutation(double Value, const double Min);

	/*! @brief 突然変異を加える(double)
	@param[in] Value 元の値
	@return double MutatedValue
	@details 突然変異が発生した場合元の値に微小値を加える。
	*/
	double AddMutation(double Value);

	/*! @brief 突然変異を加える(bool)
	@param[in] Value 元の値
	@return bool MutatedValue
	@details 突然変異が発生した場合元の値をフリップする
	*/
	bool AddMutation(bool Value);

	/*! @brief 突然変異を加える(配列)
	@param[in] Array 元の配列
	@param[in] Min 値の取りうる最小値
	@param[in] Max 値の取りうる最大値
	@return double MutatedArray
	@details 配列のそれぞれの値に突然変異を加える。\n
	値が範囲外になった場合調整を行う。
	*/
	template < typename  T, unsigned int  N >
	std::array<T, N> AddMutation(std::array<T, N> Array, const T Min, const T Max) {
		//配列のそれぞれの要素に突然変異を加える
		for (auto& a : Array) {
			a = AddMutation(a, Min, Max);
		}

		return Array;
	}

	/*! @brief 突然変異を加える(配列)
	@param[in] Array 元の配列
	@param[in] Min 値の取りうる最小値
	@return double MutatedArray
	@details 配列のそれぞれの値に突然変異を加える。\n
	値が範囲外になった場合調整を行う。
	*/
	template < typename  T, unsigned int  N >
	std::array<T, N> AddMutation(std::array<T, N> Array, const T Min) {
		//配列のそれぞれの要素に突然変異を加える
		for (auto& a : Array) {
			a = AddMutation(a, Min);
		}

		return Array;
	}

	/*! @brief ルーレットを回す
	@param[in] Weights それぞれの要素の重み
	@return unsigned int HitElement
	@details 重みに応じて要素が選ばれる確率が変わるルーレットを回す。\n
	*/
	unsigned int Roulette(const std::vector<double> Weights);

private:
	//! メルセンヌ・ツイスタ
	std::mt19937 MT;

};

/*! @brief 乱数処理を行うインスタンスを返す
@return RandomOperation& RO
@details 本フレームワークでは乱数処理のクラスはシングルトンで運用する。\n
この関数を用いることでシングルトンのインスタンスを取得することができる。
*/
RandomOperation& GetRandomOperation();