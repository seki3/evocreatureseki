#pragma once
#include <unordered_map>
#include "NeuralNetwork.h"
#include "PhysicalPart.h"
#include "Sensor.h"

/*! @brief 表現型の構成要素であるコンポーネントのクラス
@details 表現型はこのコンポーネントによって構成される。\n
コンポーネントは物理空間における実体であるPhysicalPartと\n
関節の動きを制御するNeuralNetworkを持つ。
*/
class Component {
public:
	/*! @brief コンストラクタ
	@param[in] Sides 辺の長さ
	@param[in] EyeDirection 視覚センサーの方向
	@param[in] ST センサータイプ
	@param[in] NN ニューラルネットワーク
	@param[in] JointAttachVector ジョイント接続ベクトル
	@param[in] JointAxisVector ジョイント軸ベクトル
	@param[in] AttitudeAngle 姿勢角
	@param[in] ChildComponentVector 子コンポーネントの配列
	@details 物理空間に物体とジョイントを作る上で必要な情報を受け取りインスタンス化する。
	*/
	Component(const std::array<double, 3> Sides, const std::array<double, 3> EyeDirection, const unsigned int ST, const NeuralNetwork NN,
		const std::array<double, 2> JointAttachVector, const std::array<double, 2> JointAxisVector,
		const std::array<double, 3> AttitudeAngle, const std::vector<Component> ChildComponentVector);

	/*! @brief デストラクタ*/
	virtual ~Component();

	/*! @brief 辺の長さを返す
	@return std::array<double, 3> Sides
	*/
	std::array<double, 3> GetSides() { return Sides; }

	/*! @brief 視覚センサーの方向を返す
	@return std::array<double, 3> EyeDirection
	*/
	std::array<double, 3> GetEyeDirection() { return EyeDirection; }

	/*! @brief センサータイプを返す
	@return unsigned int ST
	*/
	unsigned int GetSensorType() { return ST; }

	/*! @brief ニューラルネットワークを返す
	@return NeuralNetwork NN
	*/
	NeuralNetwork& GetNeuralNetwork() { return NN; }

	/*! @brief ジョイント接続ベクトルを返す
	@return std::array<double, 3> JointAttachVector
	*/
	std::array<double, 3> GetJointAttachVector() { return JointAttachVector; }

	/*! @brief ジョイント軸ベクトルを返す
	@return std::array<double, 3> JointAxisVector
	*/
	std::array<double, 3> GetJointAxisVector() { return JointAxisVector; }

	/*! @brief 姿勢角を返す
	@return std::array<double, 3> AttitudeAngle
	*/
	std::array<double, 3> GetAttitudeAngle() { return AttitudeAngle; }

	/*! @brief 子コンポーネントの配列を返す
	@return std::vector<Component>& ChildComponentVector
	*/
	std::vector<Component>& GetChildComponentVector() { return ChildComponentVector; }

	//ボディパーツを動かす
	void MoveBodyPart();

	//ニューラルネットワーク
	/*! @brief ニューラルネットワークを発火
	@details コンポーネントの持つニューラルネットワークを発火させる。\n
	この関数は再帰的に子コンポーネントのニューラルネットワークも発火させる。
	*/
	void FireNeuralNetwork();

	/*! @brief ニューラルネットワークの値を伝播
	@param[in] Parentinformation 親コンポーネントからの情報
	@details 親コンポーネントからの値と子コンポーネントからの値を基にニューラルネットワークの値を伝播させる。\n
	この関数は再帰的に子コンポーネントのニューラルネットワークも伝播させる。
	*/
	void PropagateNeuralNetwork(const double Parentinformation);

	/*! @brief ニューラルネットワークの親コンポーネントへの情報を返す
	@return double InformationForParent
	*/
	double GetInformationForParent() { return NN.GetInformationForParent(); }

	//ジョイント角度を返す
	double GetJointAngle() { return MyPhysicalPart.GetJointAngle(); }

	//ボックスパーツのタッチセンサーのタッチの有無を返す．物体にタッチしていればtrue．そうでなければfalseを返す．
	bool GetBoxPartTouchSensorValue() { return MyPhysicalPart.GetBoxPartTouchSensor(); }

	//知覚した音量を返す
	double GetBoxPartPerceivedSoundVolume() { return MyPhysicalPart.GetPerceivedSoundVolume(); }

	//ビームセンサーが検知した物体までの距離を返す
	double GetBeamSensorMeasuredDistance() { return MyPhysicalPart.GetBeamSensorMeasuredDistance(); }

	void SetPerceivedSounds(const std::vector<std::array<double, 4>> Sounds); //パーツに知覚する音の値をセットする
	void SetBeamSensorValue(std::unordered_map<dGeomID, double> HitBeamSensorValues); //ビームセンサーが検知した物体までの距離をセットする
	void SetBoxPartTouchSensor(std::vector<dGeomID> CollisionBoxGeomIDs); //ボックスパーツが他の物体にタッチしたかの有無をセットする

	//再帰的にビームセンサーのRayGeomIDを返す
	std::vector<dGeomID> GetHeadPartBeamSensorRayGeomID();//ヘッドパーツのビームセンサーのRayGeomIDを返す
	void GetChildBeamSensorRayGeomID(std::vector<dGeomID>& RayGeomIDs);//子コンポーネントを取得し，GetBeamSensorRayGeomIDを実行する
	void GetBeamSensorRayGeomID(std::vector<dGeomID>& RayGeomIDs);//RayGeomIDを返す

	/*! @brief 仮想物理空間にヘッドパーツを生成し，子パーツ生成のための再帰ループに入る

	仮想物理空間にエージェントを生成する際の手順は以下のように再帰的に行われる\n
	(1)本関数を実行し，ヘッドパーツを生成する．\n
	(2)MakeChildPart() を実行する．MakeChildPart() では呼び出し元のパーツが子パーツを持っているか調べる．\n
	持っていれば持っている子パーツの数分(3)を行う，持っていなければエージェントのパーツ生成を終了する\n
	(3)MakeBodyPart() を実行し，仮想物理空間上にボディパーツを生成する．そして，MakeChildPart() を実行する．\n
	この際，(1)と(3)のパーツを生成する関数を分けた理由は，(1)で生成するヘッドパーツは取り付け先がないので，ジョイントを\n
	接続する作業がないのに対し，(3)で生成するボディパーツは必ず取り付け先が存在し，ジョイントで親パーツと接続しなければならないからである．
	@param dWorldID World：ヘッドパーツを生成する仮想物理空間のID
	@param dSpaceID Space：ヘッドパーツは自身の形状と合致した衝突空間を持つので，その衝突空間を生成する空間のID
	@param std::array<double, 3> HeadPosition：生成されるヘッドパーツの重心の座標
	@param std::vector<dGeomID>& GeomIDs：Phenotypeクラスが持つ自身のパーツのdGeomIDの一覧に登録するために必要
	@sa MakeChildPart()
	@sa MakeBodyPart()*/
	void MakePart(dWorldID World, dSpaceID Space, std::array<double, 3> HeadPosition, std::vector<dGeomID>& GeomIDs);

	/*! @brief 自身が持つ子パーツの数に応じて MakeBodyPart() を実行する

	本関数を呼び出したComponentクラスのインスタンスが子パーツを持っているか調べ，持っていれば，持っている分だけMakeBodyPart()を実行する\n
	本関数と MakeBodyPart() により再帰的にボディパーツを生成する
	@param dWorldID World：ボディパーツを生成する仮想物理空間のID
	@param dSpaceID Space：ボディパーツは自身の形状と合致した衝突空間を持つので，その衝突空間を生成する空間のID
	@param std::array<double, 3> ParentPosition：ボディパーツは必ず自身の親パーツと結合するために親パーツの重心位置が必要
	@param double ParentMinSideLength：ボディパーツの結合時に必要．結合位置の決定アルゴリズムは MakeBodyPart() を参照
	@param dBodyID ParentBody：ボディパーツが結合する親パーツのdBodyID
	@param std::vector<dGeomID>& GeomIDs：Phenotypeクラスが持つ自身のパーツのdGeomIDの一覧に登録するために必要*/
	void MakeChildPart(dWorldID World, dSpaceID Space, std::array<double, 3> ParentPosition,
		double ParentMinSideLength, dBodyID ParentBody, std::vector<dGeomID>& GeomIDs);

	/*! @brief 仮想物理空間にボディパーツを生成し MakeChildPart() を実行する

	自身のボディパーツを親パーツと接続する際の結合位置のアルゴリズムについて述べる．\n
	自身のボディパーツの直方体の形状内に内接する最大の大きさ球を仮定する．同様に親パーツの直方体の形状内に内接する最大の大きさの球を仮定数．\n
	それら二つの球が接する位置に自身のボディパーツと親パーツが置かれる．この際の接点が自身のボディパーツと親パーツを結合するジョイントの位置となる
	@param dWorldID World：ボディパーツを生成する仮想物理空間のID
	@param dSpaceID Space：ボディパーツは自身の形状と合致した衝突空間を持つので，その衝突空間を生成する空間のID
	@param std::array<double, 3> ParentPosition：ボディパーツは必ず自身の親パーツと結合するために親パーツの重心位置が必要
	@param double ParentMinSideLength：ボディパーツの結合時にの親パーツの直方体の最大内接球を求める際に使用．
	@param dBodyID ParentBody：ボディパーツが結合する親パーツのdBodyID
	@param std::vector<dGeomID>& GeomIDs：Phenotypeクラスが持つ自身のパーツのdGeomIDの一覧に登録するために必要*/
	void MakeBodyPart(dWorldID World, dSpaceID Space, std::array<double, 3> ParentPosition,
		double ParentMinSideLength, dBodyID ParentBody, std::vector<dGeomID>& GeomIDs);

private:
	//! 辺の長さ
	const std::array<double, 3> Sides;

	//! 視覚センサーの方向
	const std::array<double, 3> EyeDirection;

	//! センサータイプ
	const unsigned int ST;

	//! ニューラルネットワーク
	NeuralNetwork NN;

	//! ジョイント接続ベクトル
	const std::array<double, 3> JointAttachVector;

	//! ジョイント軸ベクトル
	const std::array<double, 3> JointAxisVector;

	//! 姿勢角
	const std::array<double, 3> AttitudeAngle;

	//! 子コンポーネントの配列
	std::vector<Component> ChildComponentVector;

	PhysicalPart MyPhysicalPart;

	/*! @brief ニューラルネットワークの入力値が格納されたmapを返す

	自身の直方体オブジェクトが持つセンサーで得た情報が格納されたmapを返す.\n
	maのキーは整数値，値はvectorとなっている．\n
	まず，キーの値は SensorType で定義されている．\n
	次に，値はvectorであるが，これは今後の拡張性を考慮したものである．現在は各センサーが取得する値は1つである．\n
	具体的には以下の通りである\n
	- ジョイントセンサー\n
	親パーツとの結合に使用しているジョイントの角度(rad)
	- タッチセンサー\n
	自身の直方体オブジェクトが地面や，他の自分のパーツや相手エージェントなど他のオブジェトに触れた場合に1.0で，そうでなければ0.0
	- 音センサー\n
	自身の直方体オブジェクトに届いた音量の大きさに応じた実数値
	- 視覚センサー\n
	自身の直方体が持つビームセンサーが他のオブジェクトを検知している場合1.0で検知していない場合0.0*/
	std::map<unsigned int, std::vector<double> > GetNNinputMap();
};