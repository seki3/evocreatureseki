#include "BaseNode.h"

//コンストラクタ
BaseNode::BaseNode(const unsigned int ST) : NN(ST) {
	//メンバ変数をランダムに初期化
	RandomOperation& RO = GetRandomOperation();
	this->Sides = RO.MakeRandomValue<double, 3>(MinSide, MaxSide);
	this->EyeDirection = RO.MakeRandomValue<double, 3>(0.0, 2.0 * M_PI);

	this->ST = IsValueValid<unsigned int>(ST, 0);
}

BaseNode::BaseNode(const std::array<double, 3> Sides, const std::array<double, 3> EyeDirection,
	const unsigned int ST, const NeuralNetwork NN, const std::vector<Connection> ConnectionVector) : NN(NN) {

	//メンバ変数の初期化
	this->Sides = IsArrayValueValid(Sides, MinSide, MaxSide);
	this->EyeDirection = IsArrayValueValid(EyeDirection, 0.0, 2.0 * M_PI);
	this->ST = IsValueValid<unsigned int>(ST, 0);
	this->ConnectionVector = ConnectionVector;
}

//デストラクタ
BaseNode::~BaseNode() {
}

//突然変異
void BaseNode::Mutation(const unsigned int NormalNodeNumber) {
	//ニューラルネットワークに突然変異を加える
	NN.Mutation();

	//ニューラルネットワークだけ進化する場合以降の部分は実行されない
	if (OnlyNeuralNetworkEvolves) return;

	//メンバ変数に突然変異を加える
	RandomOperation& RO = GetRandomOperation();
	Sides = RO.AddMutation<double, 3>(Sides, MinSide, MaxSide);
	EyeDirection = RO.AddMutation<double, 3>(EyeDirection, 0.0, 2.0 * M_PI);

	//突然変異によってコネクションを1つ削除
	DeleteConnection();

	//突然変異によってコネクションを1つ追加
	AddConnection(NormalNodeNumber);

	//コネクションに突然変異を加える
	MutateConnections();
}

//コネクションのインデックスを調整
void BaseNode::AdjustConnectionIndex(const unsigned int DeletedIndex) {
	for (auto& c : ConnectionVector) {
		unsigned int NormalNodeIndex = c.GetNormalNodeIndex(); //ノーマルノードのインデックス
		if (NormalNodeIndex > DeletedIndex) c.DecreaseIndex(); //もし削除されたノードより大きいインデックスならインデックスから1つ減じる
	}
}

//突然変異によってコネクションを1つ追加
void BaseNode::AddConnection(const unsigned int NormalNodeNumber) {
	//突然変異が発生してかつノーマルノードが1つ以上ある場合
	RandomOperation& RO = GetRandomOperation();
	if (RO.IsMutationOccurred() && NormalNodeNumber >= 1) {
		//今あるノーマルノードの中からランダムに１つ選び、そこに新しいコネクションが接続する
		ConnectionVector.push_back(Connection(RO.MakeRandomValue(0, NormalNodeNumber - 1)));
	}
}

//突然変異によってコネクションを1つ削除
void BaseNode::DeleteConnection() {
	//突然変異が発生してかつコネクションがあった場合
	RandomOperation& RO = GetRandomOperation();
	if (RO.IsMutationOccurred() && !ConnectionVector.empty()) {
		//今あるコネクションの中からランダムに１つ選び削除する
		RemoveVectorElement(ConnectionVector, RO.MakeRandomValue(0, ConnectionVector.size() - 1));
	}
}

//コネクションに突然変異を加える
void BaseNode::MutateConnections() {
	for (auto& c : ConnectionVector) {
		c.Mutation();
	}
}