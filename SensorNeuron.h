#pragma once
#include <cereal/types/base_class.hpp>
#include <cereal/types/polymorphic.hpp>
#include "NormalNeuron.h"

/*! @brief センサーニューロンのクラス
@details ニューラルネットワークの入力にあたる。\n
コネクションによってノーマルニューロンまたはエフェクターニューロンに接続する。\n
活性化関数には線形関数を用いる。（そのまま出力する)
*/
class SensorNeuron : public NormalNeuron {
public:
	/*! @brief 通常のコンストラクタ。活性化関数を初期化する。*/
	SensorNeuron();

	/*! @brief 任意のセンサーニューロンを作成するコンストラクタ
	@param[out] NeuroConnectionVector ニューロコネクションの配列
	@details センサーニューロンの持つ全ての情報を与えてセンサーニューロンを作成する。
	*/
	SensorNeuron(const std::vector<NeuroConnection> NeuroConnectionVector);

	/*! @brief デストラクタ */
	virtual ~SensorNeuron();

private:
	friend class cereal::access;
	/*! @brief シリアライズ
	@details センサーニューロンの持つ全ての情報をシリアライズする関数*/
	template<class Archive>
	void serialize(Archive& archive) {
		archive(cereal::make_nvp("NormalNeuron", cereal::base_class<NormalNeuron>(this)));
	}

};