#pragma once
#include "HeadNode.h"
#include "NormalNode.h"

/*! @brief エージェントの遺伝情報が記述されるクラス
@details このクラスではエージェントの中心となるヘッドノードとそれに付随して形をつくるノーマルノードを保持する。
*/
class Genotype {
public:
	/*! @brief 通常のコンストラクタ
	@details ヘッドノードのみを持つ遺伝子型を作成する
	*/
	Genotype();

	/*! @brief 任意の遺伝子型を作成するコンストラクタ
	@param[in] HN ヘッドノード
	@param[in] NormalNodeVector ノーマルノードの配列
	@details 任意のヘッドノードとノーマルノードの配列から遺伝子型を作成する。
	*/
	Genotype(const HeadNode HN, const std::vector<NormalNode> NormalNodeVector);

	/*! @brief デストラクタ*/
	virtual ~Genotype();

	/*! @brief ヘッドノードを返す
	@return HeadNode HN*/
	HeadNode GetHeadNode() { return HN; }

	/*! @brief ノーマルノードの配列を返す
	@return std::vector<NormalNode> NormalNodeVector*/
	std::vector<NormalNode> GetNormalNodeVector() { return NormalNodeVector; }

	/*! @brief 遺伝子型の持つ各情報に突然変異を加える*/
	virtual void Mutation();

private:
	/*! @brief 突然変異によってノーマルノードを1つ追加
	@details 突然変異が発生した場合、通常のコンストラクタを用いてノーマルノードを１つ追加する
	*/
	void AddNormalNode();

	/*! @brief 突然変異によってノーマルノードを1つ削除
	@details 突然変異が発生した場合、今保持しているノーマルノードからランダムに1つ選び削除する。
	*/
	void DeleteNormalNode();

	/*! @brief 保持しているノーマルノードに突然変異を加える*/
	void MutateNormalNodes();

	//! ヘッドノード
	HeadNode HN;

	//! ノーマルノードの配列
	std::vector<NormalNode> NormalNodeVector;

	friend class cereal::access;
	/*! @brief シリアライズ
	@details 遺伝子型の持つ全ての情報をシリアライズする関数*/
	template<class Archive>
	void serialize(Archive& archive) {
		archive(cereal::make_nvp("HeadNode", HN),
			CEREAL_NVP(NormalNodeVector));
	}

};