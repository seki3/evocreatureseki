#include "Ray.h"

Ray::Ray()
{
	Length = BeamSensorLength;
}


Ray::~Ray()
{
}

void Ray::CreateRay(dSpaceID Space, dBodyID InstallationBody,
	std::array<double, 3> StartingPoint,
	std::array<double, 3> AttitudeAngle)
{
	RayGeom = dCreateRay(Space, Length);
	dGeomSetBody(RayGeom, InstallationBody);

	//クォータニオンでビームの角度を指定
	dQuaternion Q, Q1, Q2, Q3, Q4;
	dQFromAxisAndAngle(Q1, 1.0, 0.0, 0.0, AttitudeAngle[0]); // x軸まわりの回転
	dQFromAxisAndAngle(Q2, 0.0, 1.0, 0.0, AttitudeAngle[1]); // y軸まわりの回転
	dQFromAxisAndAngle(Q3, 0.0, 0.0, 1.0, AttitudeAngle[2]); // z軸まわりの回転
	dQMultiply0(Q4, Q1, Q2);
	dQMultiply0(Q, Q3, Q4);

	dMatrix3 RotationMatrix;//ビームの向きを変える回転行列（そのままだとビームはｚ軸に平行な向きになる）
	dRfromQ(RotationMatrix, Q);//クォータニオンを回転行列に変換
	dGeomSetOffsetRotation(RayGeom, RotationMatrix);
}
