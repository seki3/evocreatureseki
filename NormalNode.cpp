#include "NormalNode.h"

//コンストラクタ
NormalNode::NormalNode() : BaseNode(NormalNodeSensorType) {
	//メンバ変数をランダムに初期化
	RandomOperation& RO = GetRandomOperation();
	this->RecursiveLimit = RO.MakeRandomValue(0, MaxRecursiveLimit);
}

NormalNode::NormalNode(const std::array<double, 3> Sides, const std::array<double, 3> EyeDirection,
	const NeuralNetwork NN, const std::vector<Connection> ConnectionVector, const unsigned int RecursiveLimit)
	: BaseNode(Sides, EyeDirection, NormalNodeSensorType, NN, ConnectionVector) {

	//メンバ変数を初期化
	this->RecursiveLimit = IsValueValid<unsigned int>(RecursiveLimit, 0, MaxRecursiveLimit);
}

//デストラクタ
NormalNode::~NormalNode() {
}

//突然変異
void NormalNode::Mutation(const unsigned int NormalNodeNumber) {
	//メンバ変数に突然変異を加える
	//ニューラルネットワークだけ進化する場合この部分はスキップ
	RandomOperation& RO = GetRandomOperation();
	if (!OnlyNeuralNetworkEvolves) RecursiveLimit = RO.AddMutation(RecursiveLimit, 0, MaxRecursiveLimit);

	//基底クラスの関数を実行
	BaseNode::Mutation(NormalNodeNumber);
}