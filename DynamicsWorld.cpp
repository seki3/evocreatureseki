#include "DynamicsWorld.h"

//コンストラクタ
DynamicsWorld::DynamicsWorld(vector<Genotype> Genotypes)
{
	dInitODE();// ODEの初期化
	dRandSetSeed(0);//　ODEの物理演算のシード
	World = dWorldCreate();// 物理環境を生成
	Space = dHashSpaceCreate(0);//　衝突用空間の生成
	ContactGroup = dJointGroupCreate(0);// ジョイントグループの生成

	// 関節誤差を修正するパラメータ
	dWorldSetERP(World, 0.85);// ERP（関節誤差修正パラメータ）の設定(0〜1.0の中から設定)

	// 関節や地面の柔らかさを決めるパラメータ
	dWorldSetCFM(World, 0.1);// CFM（拘束力混合パラメータ）の設定(0〜1.0の中から設定)

	Ground = dCreatePlane(Space, 0, 0, 1, 0);// z=0の平面を生成
	dWorldSetGravity(World, 0.0, 0.0, -9.8);// 重力の設定

	Steps = 0;//ステップ数の初期化

	//仮想生物の生成
	std::array <double, 3> HeadPosition = { 0.0,0.0,0.0 };//ダミー入力
	MakeAgents(Genotypes);

	//第0ステップで適応度0の個体をチェック
	dSpaceCollide(Space, this, &NearCallback);
	dWorldStep(World, 0.001);
	dJointGroupEmpty(ContactGroup);
}

//デストラクタ
DynamicsWorld::~DynamicsWorld()
{
}

//物理演算のステップを1進める
void DynamicsWorld::OdeStepsUpdate()
{
	RandomOperation& RO = GetRandomOperation();
	
	//cout << "Steps=" << Steps << endl;
	if (Steps == 0) { FirstAgentsPosition = GetAgentsPosition();
	a = RO.MakeRandomValue(-10, 60);
	b = RO.MakeRandomValue(-10, 60);
	MusicPosition = { a,b,0 };
	}

	

	Sounds.clear();//音情報を空にする
	Sounds.push_back({ a, b, 0, 1000000 });
	CollisionBoxGeomIDs.clear();//衝突したボックスオブジェクトの一覧を空にする

	//ビームセンサーに関する情報を空にする
	HitBeamSensorGeomIDs.clear();
	HitBeamSensorValues.clear();

	dSpaceCollide(Space, this, &NearCallback);// 衝突判定
	SetAgentsPerceivedSounds();//エージェントの各パーツに聴こえる音量をセットする
	SetAgentsBeamSensorValues();//エージェントにビームセンサーで検出した物体までの距離をセットする
	SetAgentsBoxPartTouchSensor();//エージェントのタッチセンサーにタッチの有無をセットする

	MoveAgents();

	dWorldStep(World, 0.01);//0.001
	dJointGroupEmpty(ContactGroup);
	Steps++;
	if (Steps == EvaluationTime) LastAgentsPosition = GetAgentsPosition();
}

// 衝突演算のコールバック関数
// オブジェクト同士が衝突する"可能性"がある時に"自動的"に呼び出される.
void DynamicsWorld::NearCallback(void *data, dGeomID o1, dGeomID o2) {
	DynamicsWorld *ThisPtr = (DynamicsWorld*)data;
	dBodyID b1 = dGeomGetBody(o1); // 衝突する可能性のある物体1
	dBodyID b2 = dGeomGetBody(o2); // 衝突する可能性のある物体2

	//物体1と物体2のボディがジョイントで結合されていたら衝突検出しないための処理
	if (b1 && b2 && dAreConnectedExcluding(b1, b2, dJointTypeContact))return;

	const int N = 10; // 接触点数の最大値
	dContact Contact[N];     //　接触点

	//接触点の数を求める．n>0で衝突が発生したということ
	int n = dCollide(o1, o2, N, &Contact[0].geom, sizeof(dContact));

	if (n > 0) {
		//ビームに関する衝突
		if ((ThisPtr->IsRayGeomID(o1)) || (ThisPtr->IsRayGeomID(o2))) {
			ThisPtr->BeamSensorCollision(o1, o2);
			return;//Rayは物体に衝突させて物理的な処理（物体を動かしたりなど）しないので，ここでreturn文で抜ける
		}

		for (int i = 0; i < n; i++) {
			ThisPtr->SetContactParameter(Contact, i);
			dJointID c = dJointCreateContact(ThisPtr->GetWorldID(), ThisPtr->GetJointGroupID(), &Contact[i]);
			dJointAttach(c, b1, b2);

			//自身のパーツ同士がめり込んでいる
			if (ThisPtr->Steps == 0) {
				for (int i = 0; i < Population; i++) {
					if ((ThisPtr->Creatures[i].HasTheGeomID(o1) == true) &&
						(ThisPtr->Creatures[i].HasTheGeomID(o2) == true))
					{
						cout << "Creatures[" << i << "]は不正なめり込みがあります" << endl;
					}
				}
			}

			//自身が地面とめり込んでいる
			if (ThisPtr->Steps == 0) {
				if ((ThisPtr->IsGroundGeomID(o1)) || (ThisPtr->IsGroundGeomID(o2))) {
					cout << "地面とめり込んでいます" << endl;
				}
			}

			//ボックスオブジェクトに関する衝突を処理
			ThisPtr->BoxCollision(o1);
			ThisPtr->BoxCollision(o2);

			if ((ThisPtr->IsGroundGeomID(o1) == true) || (ThisPtr->IsGroundGeomID(o2) == true)) {
				ThisPtr->SetSound(Contact, i);//地面と接触時に生成される音情報をセットする
			}
		}
	}
}

//エージェントの座標を返す
vector<array<double, 3> > DynamicsWorld::GetAgentsPosition() {
	vector<array<double, 3> >AgentsPosition;
	for (unsigned int i = 0; i < Creatures.size(); i++) {
		AgentsPosition.push_back(Creatures[i].GetPosition());
	}
	return AgentsPosition;
}


//衝突時の物理挙動のパラメータの設定
void DynamicsWorld::SetContactParameter(dContact Contact[], int i) {
	//定義1
	Contact[i].surface.mode = dContactSlip1 | dContactSoftERP | dContactSoftCFM | dContactApprox1;
	Contact[i].surface.mu = dInfinity;//dInfinity0.05
	Contact[i].surface.slip1 = 0.6;
	Contact[i].surface.soft_erp = 0.8;//0.8,1e-4
	Contact[i].surface.soft_cfm = 1e-4;//1e-5,1e-4

	//定義2
	//Contact[i].surface.mode = dContactBounce;//地面の反発係数を設定
	//Contact[i].surface.mu = dInfinity;
	//Contact[i].surface.bounce = 0.2;//反発係数0から1の範囲で設定//0.05
	//Contact[i].surface.bounce_vel = 0.0001;//反発するために必要な最低速度//0.1
}

//地面と接触時に生成される音情報をセットする
void DynamicsWorld::SetSound(dContact Contact[], int i) {
	array<double, 4> Sound;
	Sound[0] = Contact[i].geom.pos[0];//音源のx座標の代入
	Sound[1] = Contact[i].geom.pos[1];//音源のy座標の代入
	Sound[2] = Contact[i].geom.pos[2];//音源のz座標の代入
	Sound[3] = Contact[i].geom.depth; //音量の代入
	Sounds.push_back(Sound);
}


//ビームセンサーに関する衝突
void DynamicsWorld::BeamSensorCollision(dGeomID o1, dGeomID o2)
{
	const int N = 1;//接触点数の最大値．Rayの最大接触点数は1であるから，接触点数の最大値は1で良い
	dContactGeom RayContact;//dContactGeomのdepthは、rayとの衝突のときに限りrayの始点位置からの距離が格納される

	//接触点の数を返す（Rayの最大接触点数は1なのでが衝突していれば1が返る．衝突していなければ0が返る）
	int RayContactN = dCollide(o1, o2, N, &RayContact, sizeof(dContactGeom));

	if (RayContactN > 0) {
		dGeomID RayGeomID;
		if (dGeomGetClass(o1) == dRayClass) {      //o1がRayだった場合
			RayGeomID = o1;
		}
		else if (dGeomGetClass(o2) == dRayClass) { //o2がRayだった場合
			RayGeomID = o2;
		}

		HitBeamSensorGeomIDs.push_back(RayGeomID); //物体を検知したビームセンサーのGeomIDを追加

		//物体を検知したビームセンサーがまだ登録されていない場合
		if (HitBeamSensorValues.count(RayGeomID) == 0) {
			HitBeamSensorValues[RayGeomID] = RayContact.depth;//物体までの距離を登録
		}
		else {//物体を検知したビームセンサーが既に登録されている場合
			//ビームセンサーの始点に近い物体までの距離を格納する
			if (HitBeamSensorValues[RayGeomID] > RayContact.depth) {
				HitBeamSensorValues[RayGeomID] = RayContact.depth;//物体までの距離を登録
			}
		}
	}

#if _DEBUG
	if (dGeomGetClass(o1) == dRayClass && dGeomGetClass(o2) == dRayClass) {
		cout << "Ray同士の衝突しないものだと思っていましたが，" << endl;
		cout << "Ray同士でも衝突することが確認されたので対策が必要です" << endl;
	}
#endif 
}

//ボックスオブジェクトに関する衝突
void DynamicsWorld::BoxCollision(dGeomID Geom)
{
	if (IsBoxGeomID(Geom) == false) { return; }                //ボックスクラスでないのでリターン
	if (CollisionBoxGeomIDsContains(Geom) == true) { return; } //CollisionBoxGeomIDsに既に登録されているのでリターン
	RegisterCollisionBoxGeomIDs(Geom);                         //CollisionBoxGeomIDsに登録
}

//仮想物理空間にエージェントを生成
void DynamicsWorld::MakeAgents(vector<Genotype>& Genotypes)
{
	
	/*for (int i = 0; i < Population; i++) {
		Creatures.push_back(Agent(Genotypes[i]));
		//Creatures[i].MakeDynamicsPhenotype(World, Space, std::array<double, 3>{i*2.0, 0.0, 1.5}); 
		double a = RO.MakeRandomValue(-10, 10);
		double b = RO.MakeRandomValue(-10, 10);
		Creatures[i].MakeDynamicsPhenotype(World, Space, std::array<double, 3>{i*2.0, b, 1.5});
	}*/
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
		Creatures.push_back(Agent(Genotypes[i*10 + j]));
		//Creatures[i].MakeDynamicsPhenotype(World, Space, std::array<double, 3>{i*2.0, 0.0, 1.5}); 
		Creatures[i * 10 + j].MakeDynamicsPhenotype(World, Space, std::array<double, 3>{i*5.0, j*5.0, 1.5});
	}
	}
}

//仮想物理空間のエージェントを動かす
void DynamicsWorld::MoveAgents()
{
	for (int i = 0; i < Population; i++) {
		Creatures[i].Behave();
	}
}

//エージェントのGeomIDsを返す
vector<vector<dGeomID> > DynamicsWorld::GetAgentsGeomIDs()
{
	vector<vector<dGeomID> > AgentsGeomIDs;
	for (unsigned int i = 0; i < Creatures.size(); i++) {
		AgentsGeomIDs.push_back(Creatures[i].GetGeomIDs());
	}
	return AgentsGeomIDs;
}

//エージェントのビームセンサーのGeomIDのベクターを返す
vector<vector<dGeomID> > DynamicsWorld::GetAgentsBeamSensorGeomIDs()
{
	vector<vector<dGeomID> > AgentsBeamSensorGeomIDs;
	for (unsigned int i = 0; i < Creatures.size(); i++) {
		AgentsBeamSensorGeomIDs.push_back(Creatures[i].GetRayGeomIDs());
	}
	return AgentsBeamSensorGeomIDs;
}

//エージェントの各パーツに聴こえる音量をセットする
void DynamicsWorld::SetAgentsPerceivedSounds()
{
	vector<array<double, 4>> Sounds2 = Sounds;
	for (unsigned int i = 0; i < Creatures.size(); i++) {
		for (int j = 0; j < Sounds2.size(); j++) {
			double distance = Distance3D(Creatures[i].GetPosition(), { Sounds[j][0],Sounds[j][1],Sounds[j][2] });
			Sounds2[j][3] = music(distance,Sounds[j][3]);		
		}
		Creatures[i].SetPerceivedSounds(Sounds2);
	}
}


//エージェントにビームセンサーで検出した物体までの距離をセットする
void DynamicsWorld::SetAgentsBeamSensorValues()
{
	for (unsigned int i = 0; i < Creatures.size(); i++) {
		Creatures[i].SetBeamSensorValues(HitBeamSensorValues);
	}
}

//エージェントのタッチセンサーにタッチの有無をセットする
void DynamicsWorld::SetAgentsBoxPartTouchSensor()
{
	for (unsigned int i = 0; i < Creatures.size(); i++) {
		Creatures[i].SetBoxPartTouchSensor(CollisionBoxGeomIDs);
	}
}

//CollisionBoxGeomIDsは引数のGeomIDを保持しているか調べる．保持していればtrueを返す．そうでなければfalseを返す．
bool DynamicsWorld::CollisionBoxGeomIDsContains(dGeomID Geom) {
	//CollisionBoxGeomIDsは引数のGeomIDを保持しているか
	if (std::find(CollisionBoxGeomIDs.begin(), CollisionBoxGeomIDs.end(), Geom) != CollisionBoxGeomIDs.end()) {
		return true; //保持している
	}
	else {
		return false;//保持していない
	}
}

//引数に与えられたGeomIDがRayのGeomIDであればtrueを返す．そうでなければfalseを返す
bool DynamicsWorld::IsRayGeomID(dGeomID Geom) {
	if (dGeomGetClass(Geom) == dRayClass) { return true; }
	else { return false; }
}

//引数に与えられたGeomIDがGroundのGeomIDであればtrueを返す．そうでなければfalseを返す
bool DynamicsWorld::IsGroundGeomID(dGeomID Geom) {
	if (Geom == Ground) { return true; }
	else { return false; }
}

//引数に与えられたGeomIDがBoxのGeomIDであればtrueを返す．そうでなければfalseを返す
bool DynamicsWorld::IsBoxGeomID(dGeomID Geom) {
	if (dGeomGetClass(Geom) == dBoxClass) { return true; }
	else { return false; }
}

//CollisionBoxGeomIDsに引数のGeomIDを登録する
void DynamicsWorld::RegisterCollisionBoxGeomIDs(dGeomID Geom) {
	CollisionBoxGeomIDs.push_back(Geom);
}

//動力学世界を終了する
void DynamicsWorld::FinalizeDynamicsWorld() {
	//エージェントの破壊
	for (unsigned int i = 0; i < Creatures.size(); i++) {
		Creatures[i].DestroyDynamicsPhenotype();
	}
	dJointGroupDestroy(ContactGroup);//ジョイントグループの破壊
	dSpaceDestroy(Space);//衝突検出用スペースの破壊
	dWorldDestroy(World);//動力学計算用ワールドの破壊
	dCloseODE();//ODEを終了
}
