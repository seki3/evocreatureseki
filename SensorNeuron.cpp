#include "SensorNeuron.h"

//コンストラクタ
SensorNeuron::SensorNeuron() {
	this->ActivationFunction = Linear; //活性化関数はlinear
}

//コンストラクタ
SensorNeuron::SensorNeuron(const std::vector<NeuroConnection> NeuroConnectionVector) 
	: NormalNeuron(NeuroConnectionVector) {
	this->ActivationFunction = Linear; //活性化関数はlinear
}

//デストラクタ
SensorNeuron::~SensorNeuron() {
}