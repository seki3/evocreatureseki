#pragma once
#include "BaseNode.h"

/*! @brief 遺伝子型の中心となるノード
@details ベースノードを継承する。\n
表現型へ展開する際はこのノードから展開される。\n
このノードへコネクションが接続することはできない。
*/
class HeadNode : public BaseNode {
public:
	/*! @brief 通常のコンストラクタ
	@details センサーのタイプをパラメータHeadNodeSensorTypeとし、ベースノードの通常コンストラクタを呼び出す。
	*/
	HeadNode();

	/*! @brief 任意のヘッドノードを作成するコンストラクタ
	@param[in] Sides ノードの辺の長さ
	@param[in] EyeDirection 視覚センサーの方向
	@param[in] NN ニューラルネットワーク
	@param[in] ConnectionVector コネクションの配列
	@details ヘッドノードの持つ全ての情報を与えてヘッドノードを作成する。\n
	範囲外の値があった場合は例外が発生し強制終了する。
	*/
	HeadNode(const std::array<double, 3> Sides, const std::array<double, 3> EyeDirection,
		const NeuralNetwork NN, const std::vector<Connection> ConnectionVector);

	/*! @brief デストラクタ*/
	virtual ~HeadNode();

private:
	friend class cereal::access;
	/*! @brief シリアライズ
	@details ヘッドノードの持つ全ての情報をシリアライズする関数*/
	template<class Archive>
	void serialize(Archive& archive) {
		archive(cereal::make_nvp("BaseNode", cereal::base_class<BaseNode>(this)));
	}

};