#include "Component.h"

Component::Component(const std::array<double, 3> Sides, const std::array<double, 3> EyeDirection, const unsigned int ST,
	const NeuralNetwork NN, const std::array<double, 2> JointAttachVector, const std::array<double, 2> JointAxisVector,
	const std::array<double, 3> AttitudeAngle, const std::vector<Component> ChildComponentVector)
	: Sides(Sides), EyeDirection(EyeDirection), ST(ST), NN(NN), JointAttachVector(Polar2Cartesian(JointAttachVector[0], JointAttachVector[1])),
	JointAxisVector(Polar2Cartesian(JointAxisVector[0], JointAxisVector[1])), AttitudeAngle(AttitudeAngle), ChildComponentVector(ChildComponentVector) {
}

Component::~Component() {
}

//ボディパーツを動かす
void Component::MoveBodyPart() {
	//ニューラルネットワークからトルクを受け取りボディーパーツを動かす
	//今は係数を40としているが、コンポーネントの大きさに比例するよう変更する
	MyPhysicalPart.AddTorque(NN.GetJointTorque() * 40);

	//子コンポーネントを動かす
	for (auto& C : ChildComponentVector) {
		C.MoveBodyPart();
	}
}

//ニューラルネットワークの入力に必要なデータのmapを返す
std::map<unsigned int, std::vector<double>> Component::GetNNinputMap() {
	std::map<unsigned int, std::vector<double>> NNinputMap;

	//ジョイントの角度をセット
	NNinputMap[SensorType::Joint] = std::vector<double>{ GetJointAngle() };

	//タッチセンサーのタッチの有無をセット
	//タッチしている場合:1.0,タッチしていない場合:0.0をセット
	std::vector<double> TouchSensorValueVector;
	if (GetBoxPartTouchSensorValue() == true) {
		TouchSensorValueVector.push_back(1.0);
	}
	else {
		TouchSensorValueVector.push_back(0.0);
	}
	NNinputMap[SensorType::Touch] = TouchSensorValueVector;

	//知覚した音量をセット
	NNinputMap[SensorType::Sound] = std::vector<double>{ GetBoxPartPerceivedSoundVolume() };

	//視覚センサーの値をセット
	//測定距離ではなく，物体を検知した場合:1.0，検知していない場合:0.0をセット
	std::vector<double> BeamSensorValueVector;
	if (GetBeamSensorMeasuredDistance() < BeamSensorLength) {
		BeamSensorValueVector.push_back(1.0);
	}
	else {
		BeamSensorValueVector.push_back(0.0);
	}
	NNinputMap[SensorType::Eye] = BeamSensorValueVector;

	return NNinputMap;
}

//ニューラルネットワークを発火
void Component::FireNeuralNetwork() {
	NN.Fire(); //発火

	//子コンポーネントのニューラルネットワークを発火
	for (auto& C : ChildComponentVector) {
		C.FireNeuralNetwork();
	}
}

//ニューラルネットワークを伝播
void Component::PropagateNeuralNetwork(const double Parentinformation) {
	std::map<unsigned int, std::vector<double>> Input = GetNNinputMap();

	//子コンポーネントからの情報を取得
	double ChildInformation = 0.0;
	for (auto& C : ChildComponentVector) {
		ChildInformation += C.GetInformationForParent();
	}

	//親コンポーネントと子コンポーネントからの情報を入力に追加
	Input[0] = std::vector<double>{ Parentinformation, ChildInformation };

	//伝播
	NN.Propagation(Input);

	//子コンポーネントのニューラルネットワークを伝播
	for (auto& C : ChildComponentVector) {
		//自身の子コンポーネントへの情報を引数として入力
		C.PropagateNeuralNetwork(NN.GetInformationForChild());
	}
}

//パーツが知覚する音の値をセット
void Component::SetPerceivedSounds(const std::vector<std::array<double, 4> > Sounds) {
	double SumSounds = 0;
	for (auto& S : Sounds) {
		SumSounds += S[3];
	}

	MyPhysicalPart.SetPerceivedSoundVolume(SumSounds); //知覚した音量をセット

	//子パーツに知覚する音の値をセットする
	for (auto& C : ChildComponentVector) {
		C.SetPerceivedSounds(Sounds);
	}
}

//ビームセンサーが検知した物体までの距離をセットする
void Component::SetBeamSensorValue(const std::unordered_map<dGeomID, double> HitBeamSensorValues) {
	// HitBeamSensorValuesのキーに自身のBeamSensorのGeomIDが登録されていれば，
	// 検知した物体までの距離をセットする．そうでなければ，ビームセンサーの初期値の長さをセットする．
	if (HitBeamSensorValues.count(MyPhysicalPart.GetBeamSensorGeomID()) == 1) {
		MyPhysicalPart.SetBeamSensorValue(HitBeamSensorValues.at(MyPhysicalPart.GetBeamSensorGeomID()));
	}
	else {
		MyPhysicalPart.SetBeamSensorValue(BeamSensorLength);
	}

	//子パーツのビームセンサーに検知した物体までの距離をセットする
	for (auto& C : ChildComponentVector) {
		C.SetBeamSensorValue(HitBeamSensorValues);
	}
}

//ボックスパーツが他の物体にタッチしたかの有無をセットする
void Component::SetBoxPartTouchSensor(std::vector<dGeomID> CollisionBoxGeomIDs) {
	// CollisionBoxGeomIDsに登録されているGeomIDで自身のBoxPartのGeomIDと
	// 一致するものがあれば，そのBoxPartのタッチをtrueにする．そうでなければfalseにする．
	if (std::find(CollisionBoxGeomIDs.begin(), CollisionBoxGeomIDs.end(), MyPhysicalPart.GetBoxPartGeomID()) != CollisionBoxGeomIDs.end()) {
		MyPhysicalPart.SetBoxPartTouchSensor(true);
	}
	else {
		MyPhysicalPart.SetBoxPartTouchSensor(false);
	}

	//子ボックスパーツが他の物体にタッチしたかの有無をセットする
	for (auto& C : ChildComponentVector) {
		C.SetBoxPartTouchSensor(CollisionBoxGeomIDs);
	}
}

//ヘッドパーツのビームセンサーのRayGeomIDをRayGeomIDsに登録し，GetChildBeamSensorRayGeomIDを実行
std::vector<dGeomID> Component::GetHeadPartBeamSensorRayGeomID() {
	std::vector<dGeomID> RayGeomIDs;
	RayGeomIDs.push_back(MyPhysicalPart.GetBeamSensorGeomID());

	GetChildBeamSensorRayGeomID(RayGeomIDs);
	return RayGeomIDs;
}

//子コンポーネントを取得し，GetBeamSensorRayGeomIDを実行する
void Component::GetChildBeamSensorRayGeomID(std::vector<dGeomID>& RayGeomIDs) {
	for (unsigned int i = 0; i < ChildComponentVector.size(); i++) {
		Component& C = ChildComponentVector[i];
		C.GetBeamSensorRayGeomID(RayGeomIDs);
	}
}

//RayGeomIDを返す
void Component::GetBeamSensorRayGeomID(std::vector<dGeomID>& RayGeomIDs) {
	RayGeomIDs.push_back(MyPhysicalPart.GetBeamSensorGeomID());

	GetChildBeamSensorRayGeomID(RayGeomIDs);
}

//仮想物理空間にヘッドパーツを生成し，子パーツ生成のための再帰ループに入る
void Component::MakePart(dWorldID World, dSpaceID Space, std::array<double, 3> HeadPosition, std::vector <dGeomID>& GeomIDs) {
	MyPhysicalPart.MakePart(World, Space, Sides, HeadPosition, AttitudeAngle, EyeDirection, GeomIDs); //仮想物理空間にパーツ作成

	MakeChildPart(World, Space, HeadPosition, GetMinSideLength(Sides), MyPhysicalPart.GetBoxPartBodyID(), GeomIDs);
}

//仮想物理空間に子パーツを生成
void Component::MakeChildPart(dWorldID World, dSpaceID Space, std::array<double, 3> ParentPosition,
	double ParentMinSideLength, dBodyID ParentBody, std::vector<dGeomID>& GeomIDs) {
	for (unsigned int i = 0; i < ChildComponentVector.size(); i++)//ChildComponentVector.size()==0の時for文に入らない
	{
		Component& C = ChildComponentVector[i];
		C.MakeBodyPart(World, Space, ParentPosition, ParentMinSideLength, ParentBody, GeomIDs);
	}
}

//仮想物理空間にボディパーツを生成
void Component::MakeBodyPart(dWorldID World, dSpaceID Space, std::array<double, 3> ParentPosition,
	double ParentMinSideLength, dBodyID ParentBody, std::vector <dGeomID>& GeomIDs) {
	double ChildMinSideLength = GetMinSideLength(Sides);

	std::array<double, 3> ChildPosition;
	for (int i = 0; i < 3; i++) {
		ChildPosition[i] = ParentPosition[i] + (ChildMinSideLength / 2.0 + ParentMinSideLength / 2.0)*JointAttachVector[i];
	}

	MyPhysicalPart.MakePart(World, Space, Sides, ChildPosition, AttitudeAngle, EyeDirection, GeomIDs);
	std::array<double, 3> JointPosition;
	for (int i = 0; i < 3; i++) {
		JointPosition[i] = ParentPosition[i] + (ParentMinSideLength / 2.0) * JointAttachVector[i];
	}
	MyPhysicalPart.JointAttach(World, ParentBody, JointPosition, JointAxisVector);
	MakeChildPart(World, Space, ChildPosition, ChildMinSideLength, MyPhysicalPart.GetBoxPartBodyID(), GeomIDs);
}
