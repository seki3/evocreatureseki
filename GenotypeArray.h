#pragma once
#include "Genotype.h"

/*! @brief 遺伝子型の配列を持ち遺伝子型同士の操作を行う*/
class GenotypeArray {
public:
	/*! @brief コンストラクタ
	@param[in] GenotypeVector 遺伝子型の配列
	@details 遺伝子型の配列からインスタンスを作成する。
	*/
	GenotypeArray(const std::vector<Genotype> GenotypeVector);

	/*! @brief デストラクタ*/
	virtual ~GenotypeArray();

	/*! @brief 演算子[]を定義
	@param[in] i 遺伝子型配列のインデックス
	@return Genotype G
	@details 演算子[]を用いて遺伝子型配列の要素にアクセスできるようにする
	*/
	Genotype operator[] (unsigned int i);

	/*! @brief 遺伝子型配列を返す
	@return std::vector<Genotype> GenotypeVector*/
	std::vector<Genotype> GetGenotypeVector() { return GenotypeVector; }

	/*! @brief 各遺伝子型に突然変異を加える*/
	void Mutation();

	/*! @brief ２つの遺伝子型を交叉して新しい遺伝子型を作る
	@param[in] Parent1 親1のインデックス
	@param[in] Parent2 親2のインデックス
	@param[in] CrossPoint 遺伝子を交叉する場所
	@details 新しい遺伝子型は親1の交叉する場所直前までのノーマルノードとそれ以降の親2のノーマルノードを持つ。\n
	ヘッドノードは親1のものがコピーされる。
	*/
	Genotype CrossOver(const unsigned int Parent1, const unsigned int Parent2, const unsigned int CrossPoint);

private:
	//! 遺伝子型配列
	std::vector<Genotype> GenotypeVector;

	friend class cereal::access;
	/*! @brief シリアライズ
	@details 遺伝子型配列をシリアライズする関数*/
	template<class Archive>
	void serialize(Archive& archive) {
		archive(CEREAL_NVP(GenotypeVector));
	}

};