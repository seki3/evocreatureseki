#include "Genotype.h"

//コンストラクタ
Genotype::Genotype() : HN() {
}

Genotype::Genotype(const HeadNode HN, const std::vector<NormalNode> NormalNodeVector)
	: HN(HN), NormalNodeVector(NormalNodeVector) {
}

//デストラクタ
Genotype::~Genotype() {
}

//突然変異
void Genotype::Mutation() {
	//ニューラルネットワークだけ進化する場合この部分はスキップ
	if (!OnlyNeuralNetworkEvolves) {
		//突然変異によってコネクションを1つ削除
		DeleteNormalNode();

		//突然変異によってノーマルノードを1つ追加
		AddNormalNode();
	}

	//ヘッドノードに突然変異を加える
	HN.Mutation(NormalNodeVector.size());

	//ノーマルノードに突然変異を加える
	MutateNormalNodes();
}

//突然変異によってノーマルノードを1つ追加
void Genotype::AddNormalNode() {
	//突然変異が発生した場合
	RandomOperation& RO = GetRandomOperation();
	if (RO.IsMutationOccurred()) {
		//ノーマルノードを1つ追加
		NormalNodeVector.push_back(NormalNode());
	}
}

//突然変異によってノーマルノードを1つ削除
void Genotype::DeleteNormalNode() {
	//突然変異が発生してかつノーマルノードがあった場合
	RandomOperation& RO = GetRandomOperation();
	if (RO.IsMutationOccurred() && !NormalNodeVector.empty()) {
		//今あるノーマルノードの中からランダムに1つ選び削除する
		int Index = RO.MakeRandomValue(0, NormalNodeVector.size() - 1);
		RemoveVectorElement(NormalNodeVector, Index);

		//ヘッドノードの持つコネクションのインデックスを調整する
		HN.AdjustConnectionIndex(Index);

		//各ノーマルノードの持つコネクションのインデックスを調整する
		for (auto& nn : NormalNodeVector) {
			nn.AdjustConnectionIndex(Index);
		}
	}
}

//ノーマルノードに突然変異を加える
void Genotype::MutateNormalNodes() {
	for (auto& nn : NormalNodeVector) {
		nn.Mutation(NormalNodeVector.size());
	}
}