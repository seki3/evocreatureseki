#pragma once
#include <array>
#include <ode\ode.h>

class HingeJoint {
public:
	//コンストラクタ
	HingeJoint();

	//デストラクタ
	virtual ~HingeJoint();

	//ジョイントがなければ0.0を、そうでなければ自ジョイントの角度を返す
	double GetJointAngle() {
		return (Joint == nullptr ? 0.0 : double(dJointGetHingeAngle(Joint)));
	}

	// ジョイントにトルクを加える
	void AddTorque(const dReal Torque);

	// 親パーツに子パーツをジョイントで接続
	void JointAttach(dWorldID World, dBodyID ParentBody, dBodyID MyBody, const std::array<double, 3>JointPosition, const std::array<double, 3>JointAxis);

private:
	//メンバ変数
	dJointID Joint = nullptr; //ジョイントのID

};