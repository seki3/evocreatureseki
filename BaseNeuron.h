#pragma once

/*! @brief ニューロンの基底クラス
@details ニューロンに共通する処理や情報を持つ。\n
ここから各種ニューロンを作成する。\n
このクラス単体で使用することは無い。
*/
class BaseNeuron {
public:
	/*! @brief コンストラクタ。何もしない。*/
	BaseNeuron();

	/*! @brief デストラクタ */
	virtual ~BaseNeuron();

	/*! @brief ニューロンの入力を返す
	@return double Input*/
	double GetInput() { return Input; }

	/*! @brief ニューロンの出力を返す
	@return double Output*/
	double GetOutput() { return Output; }

	/*! @brief ニューロンに値をセットする
	@param[in] x ニューロンにセットする値*/
	void Set(const double x);

	/*! @brief 活性化関数を用いてニューロンを発火する*/
	void Fire();

protected:
	//! 活性化関数の関数ポインタ
	double(*ActivationFunction)(double x);

private:
	//! ニューロンの入力
	double Input = 0.0;

	//! ニューロンの出力
	double Output = 0.0;

};