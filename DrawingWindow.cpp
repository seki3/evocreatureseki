#include "DrawingWindow.h"

static double angle = 0.0;
DrawingWindow::DrawingWindow(int argc, char *argv[]):Camera1()
{
	//メンバ変数の初期化
	Width = 640;
	Height = 480;
	WindowCenter = std::array<double, 2>{double(Width) / 2.0, double(Height) / 2.0};
	
	//GLFWの初期化
	if (glfwInit() == GL_FALSE) {
		cout << "@DrawingWindow::DrawingWindow GLFWの初期化に失敗" << endl;
	}

	//プログラム終了時の処理を登録する
	atexit(glfwTerminate);

	//ウィンドウの作成
	Window = glfwCreateWindow(Width, Height, "EvoCreature", nullptr, nullptr);
	if (Window == NULL) {
		cout << "@DrawingWindow::DrawingWindow ウィンドウの作成に失敗" << endl;
		glfwTerminate();
	}

	//作成したウィンドウをOpenGLの処理対象にする
	glfwMakeContextCurrent(Window);

	//GLFWのハンドルにthisポインタを登録
	glfwSetWindowUserPointer(Window, this);

	//コールバックとして静的メンバ関数を登録
	//ただし，先ほど登録したthisポインタを使用できるため，
	//静的メンバ関数であるがクラスのインスタンスを取り出せる
	glfwSetWindowSizeCallback(Window, Reshape);
	glfwSetMouseButtonCallback(Window, MouseClick);
	glfwSetKeyCallback(Window, Keyboard);
	glfwSetCursorPosCallback(Window, MouseCusor);
	glfwSetScrollCallback(Window, MouseWheel);

	//カラー・バッファの初期値
	glClearColor(0.8f, 0.8f, 0.8f, 1.0f);

	glClearDepth(1.0f);//デプス値の初期値設定
	glEnable(GL_DEPTH_TEST);

	//光源設定(環境､拡散､鏡面のみ)
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmb);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiff);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpec);

	//光源とライティング有効化
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);

	//ビューポートの設定
	glViewport(0, 0, Width, Height);//ウィンドウ全体に表示させるよう設定（デフォルト値と同じ値を引数に設定した）

	//射影行列の設定
	glMatrixMode(GL_PROJECTION);//行列モードの切り替え
	glLoadIdentity();           //設定対象の行列の初期化
	//引数1：画角（度），引数2：アスペクト比，引数3，引数4：視点位置を原点とした時の視体積の手前と奥の座標
	gluPerspective(30.0, double(Width) / double(Height), 1.0, 100.0);
}

DrawingWindow::~DrawingWindow()
{

}

void DrawingWindow::DisplayInit()
{
	//カラー・バッファを初期化
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void DrawingWindow::Display()
{
	//カメラの位置，向きの更新
	Camera1.CameraUpdate();

	//カラーバッファを入れ替え
	glfwSwapBuffers(Window);

	//コールバック関数の呼び出し
	glfwPollEvents();
}


void DrawingWindow::Reshape(GLFWwindow* Window, int NewWidth, int NewHeight)
{
	cout << "@Reshape:画面サイズは(Width,Height)=(" << NewWidth << "," << NewWidth << ")です" << endl;

	// thisポインタのように，ThisPtr->でメンバ変数やメンバ関数にアクセスできる
	DrawingWindow* This = (DrawingWindow*)glfwGetWindowUserPointer(Window);
	//サイズに関するメンバ変数の更新
	This->Width = NewWidth;
	This->Height = NewHeight;
	This->WindowCenter = std::array<double, 2> {double(NewWidth) / 2.0, double(NewHeight) / 2.0};

	//ビューポートの設定
	glViewport(0, 0, NewWidth, NewHeight);//ウィンドウ全体に表示させるよう設定

	//射影行列の設定
	glMatrixMode(GL_PROJECTION);//行列モードの切り替え
	glLoadIdentity();           //設定対象の行列の初期化
	//引数1：画角（度），引数2：アスペクト比，引数3，引数4：視点位置を原点とした時の視体積の手前と奥の座標
	gluPerspective(30.0, double(NewWidth) / double(NewHeight), 1.0, 100.0);

	glfwSwapBuffers(Window);
}

// マウスをクリックしたときに呼び出されるコールバック
// マウスキーを押したタイミングと離したタイミングに呼び出される
void DrawingWindow::MouseClick(GLFWwindow* Window, int Button, int Action, int Mods)
{
	cout << "@MouseClick Button=" << Button << ", Action=" << Action << ", Mods=" << Mods;

	//座標は左上が原点であり，x軸は右向きを正，y軸は下向きを正としている．
	double Xcoordinate, Ycoordinate;
	glfwGetCursorPos(Window, &Xcoordinate, &Ycoordinate);
	cout << "Xcoordinate=" << Xcoordinate << ", Ycoordinate=" << Ycoordinate << endl;

	// thisポインタのように，This->でメンバ変数やメンバ関数にアクセスできる
	DrawingWindow* This = (DrawingWindow*)glfwGetWindowUserPointer(Window);
	This->Camera1.SetMouseButtonState(Button, Action, Xcoordinate, Ycoordinate);
}

// マウスカーソルが動いた時に呼び出されるコールバック
void DrawingWindow::MouseCusor(GLFWwindow* Window, double Xcoordinate, double Ycoordinate)
{
	//cout << "@MouseCusor Xcoordinate=" << Xcoordinate << ", Ycoordinate=" << Ycoordinate << endl;
	// thisポインタのように，This->でメンバ変数やメンバ関数にアクセスできる
	DrawingWindow* This = (DrawingWindow*)glfwGetWindowUserPointer(Window);

	switch (This->Camera1.GetMouseButtonState())
	{
	case IsNotDragging://マウス座標の更新
		This->Camera1.Motion(IsNotDragging, This->Width, This->Height, Xcoordinate, Ycoordinate);
		break;

	case LeftButtonIsDragging://カメラの回転
		This->Camera1.Motion(LeftButtonIsDragging, This->Width, This->Height, Xcoordinate, Ycoordinate);
		break;

	case MiddleButtonIsDragging://カメラの昇降
		This->Camera1.Motion(MiddleButtonIsDragging, This->Width, This->Height, Xcoordinate, Ycoordinate);
		break;

	case RightButtonIsDragging:
		//処理なし
		break;
	}
}

//マウスホイールが回転したときに呼ばれるコールバック関数
//Xoffsetは横方向に回転する特殊なホイールを持つマウスに対応するための引数．今回は使用していない．
//Yoffsetは通常のマウスについている縦方向に回転するホイールの回転量が入る．今回はこの値を使用する．
void DrawingWindow::MouseWheel(GLFWwindow* Window, double Xoffset, double Yoffset)
{
	// thisポインタのように，This->でメンバ変数やメンバ関数にアクセスできる
	DrawingWindow* This = (DrawingWindow*)glfwGetWindowUserPointer(Window);

	//cout << "Xoffset=" << Xoffset << endl;
	//cout << "Yoffset=" << Yoffset << endl;

	//座標は左上が原点であり，x軸は右向きを正，y軸は下向きを正としている．
	double Xcoordinate, Ycoordinate;
	glfwGetCursorPos(Window, &Xcoordinate, &Ycoordinate);
	This->Camera1.Motion(MouseWheelIsRotating, This->Width, This->Height, Xcoordinate, Ycoordinate, Yoffset);
}

// キーを押した時に呼び出される関数
void DrawingWindow::Keyboard(GLFWwindow* Window, int Key, int Scancode, int Action, int Mods)
{
	//int Keyの数字に対する割り当ては{ A,B,C,…,Z } = { 65,66,67,…,90 }である．
	//その他のキーのに割り当てられた数字は，
	//http://www.glfw.org/docs/latest/group__keys.html を参照．
	cout << "@Keyboard:" << Key << "が押されました．" << endl;
}

void DrawingWindow::SetColor(float Red, float Green, float Blue, float Alpha)
{
	// Ambient[4] ：環境光（R,G,B,A）,   Diffuse[4]  ：拡散光（R,G,B,A）
	// Specular[4]：鏡面光（R,G,B,A）,   Shininess[4]：放射光
	GLfloat Ambient[4], Diffuse[4], Specular[4], Shininess;

	Ambient[0] = Red   * 0.03f;
	Ambient[1] = Green * 0.03f;
	Ambient[2] = Blue  * 0.03f;
	Ambient[3] = Alpha;
	Diffuse[0] = Red   * 0.9f;
	Diffuse[1] = Green * 0.9f;
	Diffuse[2] = Blue  * 0.9f;
	Diffuse[3] = Alpha;
	Specular[0] = Red   * 0.02f;
	Specular[1] = Green * 0.02f;
	Specular[2] = Blue  * 0.02f;
	Specular[3] = Alpha;
	Shininess = 0.0f;

	//材質設定
	glMaterialfv(GL_FRONT, GL_AMBIENT, Ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, Diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, Specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, &Shininess);
}

//仮想物理環境の描画
void DrawingWindow::Draw(const vector<vector<dGeomID> >AgentsGeomIDs, const vector<array<double, 4> >Sounds,
	vector<vector<dGeomID> > AgentsBeamSensorGeomIDs, unordered_map<dGeomID, double> HitBeamSensorValues)
{
	DisplayInit();
	DrawGround();
	DrawAxes();
	DrawSounds(Sounds);
	DrawBeamSensor(AgentsBeamSensorGeomIDs, HitBeamSensorValues);
	DrawAgents(AgentsGeomIDs);
	Display();
}

//エージェントの描画
void DrawingWindow::DrawAgents(const vector<vector<dGeomID>>AgentsGeomIDs)
{
	for (unsigned int i = 0; i < AgentsGeomIDs.size(); i++) {
		for (unsigned int j = 0; j < AgentsGeomIDs[i].size(); j++) {
			dVector3 Sides;
			dGeomBoxGetLengths(AgentsGeomIDs[i][j], Sides);//直方体のサイズを取得
			DrawBox(Sides, dGeomGetRotation(AgentsGeomIDs[i][j]), dGeomGetPosition(AgentsGeomIDs[i][j]));
		}
	}
}

//直方体の描画
void DrawingWindow::DrawBox(dVector3 Sides, const dReal Attitude[], const dReal Position[])
{
	glPushMatrix();

	//平行移動
	glTranslated(Position[0], Position[1], Position[2]);

	//回転行列
	double OpenglRotation[16];
	ConvertOdeRotationToOpenGlRotation(Attitude, OpenglRotation);
	glMultMatrixd(OpenglRotation);

	//材質設定
	SetColor(1.0f, 1.0f, 1.0f, 0.2f);

	glBegin(GL_QUADS);
	//正面
	glNormal3d(0.0, 0.0, -1.0);//正面の面に対する法線ベクトルの設定
	glVertex3d(Sides[0] / 2, Sides[1] / 2, Sides[2] / 2);
	glVertex3d(-Sides[0] / 2, Sides[1] / 2, Sides[2] / 2);
	glVertex3d(-Sides[0] / 2, -Sides[1] / 2, Sides[2] / 2);
	glVertex3d(Sides[0] / 2, -Sides[1] / 2, Sides[2] / 2);

	//左
	glNormal3d(1.0, 0.0, 0.0);//左の面に対する法線ベクトルの設定
	glVertex3d(Sides[0] / 2, Sides[1] / 2, Sides[2] / 2);
	glVertex3d(Sides[0] / 2, Sides[1] / 2, -Sides[2] / 2);
	glVertex3d(Sides[0] / 2, -Sides[1] / 2, -Sides[2] / 2);
	glVertex3d(Sides[0] / 2, -Sides[1] / 2, Sides[2] / 2);

	//右
	glNormal3d(-1.0, 0.0, 0.0);//右の面に対する法線ベクトルの設定
	glVertex3d(-Sides[0] / 2, Sides[1] / 2, -Sides[2] / 2);
	glVertex3d(-Sides[0] / 2, Sides[1] / 2, Sides[2] / 2);
	glVertex3d(-Sides[0] / 2, -Sides[1] / 2, Sides[2] / 2);
	glVertex3d(-Sides[0] / 2, -Sides[1] / 2, -Sides[2] / 2);

	//後
	glNormal3d(0.0, 0.0, 1.0);//後ろの面に対する法線ベクトルの設定
	glVertex3d(Sides[0] / 2, Sides[1] / 2, -Sides[2] / 2);
	glVertex3d(-Sides[0] / 2, Sides[1] / 2, -Sides[2] / 2);
	glVertex3d(-Sides[0] / 2, -Sides[1] / 2, -Sides[2] / 2);
	glVertex3d(Sides[0] / 2, -Sides[1] / 2, -Sides[2] / 2);

	//上
	glNormal3d(0.0, 1.0, 0.0);//上の面に対する法線ベクトルの設定
	glVertex3d(Sides[0] / 2, Sides[1] / 2, Sides[2] / 2);
	glVertex3d(-Sides[0] / 2, Sides[1] / 2, Sides[2] / 2);
	glVertex3d(-Sides[0] / 2, Sides[1] / 2, -Sides[2] / 2);
	glVertex3d(Sides[0] / 2, Sides[1] / 2, -Sides[2] / 2);

	//下
	glNormal3d(0.0, -1.0, 0.0);//下の面に対する法線ベクトルの設定
	glVertex3d(Sides[0] / 2, -Sides[1] / 2, Sides[2] / 2);
	glVertex3d(-Sides[0] / 2, -Sides[1] / 2, Sides[2] / 2);
	glVertex3d(-Sides[0] / 2, -Sides[1] / 2, -Sides[2] / 2);
	glVertex3d(Sides[0] / 2, -Sides[1] / 2, -Sides[2] / 2);
	glEnd();

	glPopMatrix();
}

//線分の描画
void DrawingWindow::DrawLine(const std::array<double, 3> &StartingPoint,
	const std::array<double, 3> &EndingPoint, const std::array<float, 4> &Color,
	double Width)
{
	glLineWidth(GLfloat(Width));
	
	SetColor(Color[0], Color[1], Color[2], Color[3]);//R(赤)，G（緑），B（青），透明度（今後実装する予定）
	glBegin(GL_LINES);
	glVertex3d(StartingPoint[0], StartingPoint[1], StartingPoint[2]);
	glVertex3d(EndingPoint[0], EndingPoint[1], EndingPoint[2]);
	glEnd();
}

//ODEの回転行列をOpenGLの回転行列に変換
void DrawingWindow::ConvertOdeRotationToOpenGlRotation(const dReal OdeRotation[], double OpenGlRotation[])
{
	// ODEの回転行列は4x3行列を以下の順序で一次元配列に格納している
	//  | m[0] m[1] m[2]  m[3] |
	//	| m[4] m[5] m[6]  m[7] |
	//	| m[8] m[9] m[10] m[11] |
	//
	// 一方，OpenGLのモデルビュー変換行列は4x4行列を以下のの順序で一次元配列に格納している
	//  | m[0] m[4] m[8]  m[12] |
	//	| m[1] m[5] m[9]  m[13] |
	//	| m[2] m[6] m[10] m[14] |
	//	| m[3] m[7] m[11] m[15] |
	//
	// よって，以下の処理でODEの回転行列をOpenGLの仕様に変更する必要がある
	// 参考サイト：http://www.slis.tsukuba.ac.jp/~fujisawa.makoto.fu/cgi-bin/wiki/index.php?ODE
	// 参考サイト：https://www21.atwiki.jp/opengl/pages/319.html
	OpenGlRotation[0] = OdeRotation[0];
	OpenGlRotation[1] = OdeRotation[4];
	OpenGlRotation[2] = OdeRotation[8];
	OpenGlRotation[3] = 0.0;

	OpenGlRotation[4] = OdeRotation[1];
	OpenGlRotation[5] = OdeRotation[5];
	OpenGlRotation[6] = OdeRotation[9];
	OpenGlRotation[7] = 0.0;

	OpenGlRotation[8] = OdeRotation[2];
	OpenGlRotation[9] = OdeRotation[6];
	OpenGlRotation[10] = OdeRotation[10];
	OpenGlRotation[11] = 0.0;

	OpenGlRotation[12] = OdeRotation[3];
	OpenGlRotation[13] = OdeRotation[7];
	OpenGlRotation[14] = OdeRotation[11];
	OpenGlRotation[15] = 1.0;
}

//地面の描画
void DrawingWindow::DrawGround()
{
	glPushMatrix();
	//glDisable(GL_LIGHTING);
	glShadeModel(GL_FLAT);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	if (0) {
		glEnable(GL_TEXTURE_2D);
		//ground_texture->bind(0);
	}
	else {
		glDisable(GL_TEXTURE_2D);
		//材質設定

		SetColor(0.2f, 0.8f, 0.2f, 0.1f);

	}


	const double GroundSize = 1.0; // 地面は一辺がGroundSizeの正方形
	const double GroundTextureScale = 1.0 / 1.0;// (1.0 / 正方形のテクスチャの一辺の長さ）
	const double GroundElevation = 0.0;//地面の高さ．ODEの地面衝突に合わせて0.0で設定
	const double GroundTextureOffsetX = 0.5;
	const double GroundTextureOffsetY = 0.5;

	glBegin(GL_QUADS);
	glNormal3d(0, 0, 1);//地面に対する法線ベクトルを設定
	glTexCoord2f(-GroundSize*GroundTextureScale + GroundTextureOffsetX,
		-GroundSize*GroundTextureScale + GroundTextureOffsetY);
	//4回のglVertex3dで四角形1枚描画
	glVertex3d(-GroundSize, -GroundSize, GroundElevation);
	glTexCoord2f(GroundSize*GroundTextureScale + GroundTextureOffsetX,
		-GroundSize*GroundTextureScale + GroundTextureOffsetY);
	glVertex3d(GroundSize, -GroundSize, GroundElevation);
	glTexCoord2f(GroundSize*GroundTextureScale + GroundTextureOffsetX,
		GroundSize*GroundTextureScale + GroundTextureOffsetY);
	glVertex3d(GroundSize, GroundSize, GroundElevation);
	glTexCoord2f(-GroundSize*GroundTextureScale + GroundTextureOffsetX,
		GroundSize*GroundTextureScale + GroundTextureOffsetY);
	glVertex3d(-GroundSize, GroundSize, GroundElevation);
	glEnd();

	glDisable(GL_FOG);
	glPopMatrix();
}

//ワールド座標系の原点にワールド座標系の座標軸を配置
void DrawingWindow::DrawAxes()
{
	double Width = 5.0;

	// x軸
	std::array<double, 3> StartingPointX{ 0.0, 0.0, 0.0 }; // x軸の始点
	std::array<double, 3> EndingPointX{ 1.0, 0.0, 0.0 };   // x軸の終点
	std::array<float, 4> ColorX{ 3.0, 0.0, 0.0, 1.0 };     // 赤色
	DrawLine(StartingPointX, EndingPointX, ColorX, Width); // x軸を描画
														   	  
	// y軸												   	  
	std::array<double, 3> StartingPointY{ 0.0, 0.0, 0.0 }; // y軸の始点
	std::array<double, 3> EndingPointY{ 0.0, 1.0, 0.0 };   // y軸の終点
	std::array<float, 4> ColorY{ 0.0, 3.0, 0.0, 1.0 };     // 緑色
	DrawLine(StartingPointY, EndingPointY, ColorY, Width); // y軸を描画

	// z軸												   	  
	std::array<double, 3> StartingPointZ{ 0.0, 0.0, 0.0 }; // z軸の始点
	std::array<double, 3> EndingPointZ{ 0.0, 0.0, 1.0 };   // z軸の終点
	std::array<float, 4> ColorZ{ 0.0, 0.0, 3.0, 1.0 };     // 青色
	DrawLine(StartingPointZ, EndingPointZ, ColorZ, Width); // z軸を描画
}

//音源に音量に比例した線分を描画
void DrawingWindow::DrawSounds(const vector<array<double, 4> >Sounds)
{
	double Width = 4.0;
	std::array<float, 4> Color{ 2.0, 2.0, 0.0, 1.0 };//赤色
	for (unsigned int i = 0; i < Sounds.size(); i++) {
		std::array<double, 3> StartingPoint{ Sounds[i][0], Sounds[i][1], Sounds[i][2] };//始点
		std::array<double, 3> EndingPoint{ Sounds[i][0], Sounds[i][1], 1000 * Sounds[i][3] };//終点
		DrawLine(StartingPoint, EndingPoint, Color, Width);
	}
}

//ビームセンサーの描画
void DrawingWindow::DrawBeamSensor(vector<vector<dGeomID> > AgentsBeamSensorGeomIDs, unordered_map<dGeomID, double> HitBeamSensorValues)
{
	double Width = 5.0;

	for (unsigned int i = 0; i < AgentsBeamSensorGeomIDs.size(); i++) {
		for (unsigned int j = 0; j < AgentsBeamSensorGeomIDs[i].size(); j++) {
			dVector3 Origin, Direction;
			dGeomRayGet(AgentsBeamSensorGeomIDs[i][j], Origin, Direction);
			std::array<double, 3> StartingPoint {double(Origin[0]), double(Origin[1]), double(Origin[2]) };
			//もしRayGeomIDs[i][j]が物体を検知していなければビームセンサーはパラメータで与えられた長さで描画する
			std::array<double, 3> EndingPoint;
			if (HitBeamSensorValues.count(AgentsBeamSensorGeomIDs[i][j]) == 0) {
				EndingPoint[0] = StartingPoint[0] + (double(Direction[0]) * BeamSensorLength);
				EndingPoint[1] = StartingPoint[1] + (double(Direction[1]) * BeamSensorLength);
				EndingPoint[2] = StartingPoint[2] + (double(Direction[2]) * BeamSensorLength);
				std::array<float, 4> Color{ 0.0, 3.0, 1.0, 1.0 };
				DrawLine(StartingPoint, EndingPoint, Color, Width);
			}
			else {//ビームセンサーはその検知した物体までの長さで描画する
				double MeasuredDistance = HitBeamSensorValues.at(AgentsBeamSensorGeomIDs[i][j]);
				EndingPoint[0] = StartingPoint[0] + (double(Direction[0]) * MeasuredDistance);
				EndingPoint[1] = StartingPoint[1] + (double(Direction[1]) * MeasuredDistance);
				EndingPoint[2] = StartingPoint[2] + (double(Direction[2]) * MeasuredDistance);
				std::array<float, 4> Color{ 3.0, 0.0, 1.0, 1.0 };
				DrawLine(StartingPoint, EndingPoint, Color, Width);
			}
		}
	}
}


