#include "DrawingWindow.h"
#include "DynamicsWorld.h"
#include "GenePool.h"
#include <stdlib.h>
#include <iomanip>

//芋虫型生物を作成
Genotype CreateCaterpillar() {
	RandomOperation& RO = GetRandomOperation();

	//センサータイプをランダムに決定
	std::map<unsigned int, bool>SensorOnOff;
	unsigned int NeuroNumber = 2;
	for (auto itr = SensorNeuronNumber.begin(); itr != SensorNeuronNumber.end(); itr++) {
		unsigned int Type = itr->first; //センサータイプ
		unsigned int Number = itr->second; //ニューロンの数

		SensorOnOff[Type] = RO.MakeRandomBoolValue();
		NeuroNumber += Number;
	}

	//センサーニューロンに初期ニューロコネクションを追加
	std::vector<SensorNeuron>SensorNeuronVector;
	for (int i = 0; i < NeuroNumber; i++) {
		//各エフェクターニューロンに接続
		std::vector<NeuroConnection>NeuroConnectionVector;
		for (int i = -3; i < 0; i++) {
			NeuroConnectionVector.push_back(NeuroConnection(i));
		}
		SensorNeuronVector.push_back(SensorNeuron(NeuroConnectionVector));
	}

	//シンプルなニューラルネットワークを作成
	NeuralNetwork NN(SensorOnOff, SensorNeuronVector, std::vector<NormalNeuron>{});

	//垂直方向に動く関節を作るコネクション
	std::vector<Connection> ConnectionVector{ Connection(std::array<double, 2>{M_PI / 2.0, M_PI / 2.0}, std::array<double, 2>{M_PI / 2.0, 0.0}, std::array<double, 3>{0.0, 0.0, 0.0}, 1.0, false, 0),
		Connection(std::array<double, 2>{M_PI / 2.0, M_PI / 2.0}, std::array<double, 2>{0.0, M_PI / 2.0}, std::array<double, 3>{0.0, 0.0, 0.0}, 1.0, true, 1) };
	std::vector<Connection> ConnectionVector2{ Connection(std::array<double, 2>{M_PI / 2.0, M_PI / 2.0}, std::array<double, 2>{0.0,M_PI / 2.0}, std::array<double, 3>{0.0, 0.0, 0.0}, 1.0, true, 0) };

	//ヘッドノード
	HeadNode HN(std::array<double, 3>{0.5, 0.5, 0.5}, std::array<double, 3>{0.0, 0.0, 0.0}, NN, ConnectionVector);

	//再帰的に呼び出されるノーマルノード
	NormalNode NN0(std::array<double, 3>{0.3, 0.3, 0.3}, std::array<double, 3>{0.0, 0.0, 0.0}, NN, ConnectionVector, 1);
	NormalNode NN02(std::array<double, 3>{0.4, 0.4, 0.4}, std::array<double, 3>{0.0, 0.0, 0.0}, NN, ConnectionVector2, 0);
	//遺伝子型を作成
	Genotype G(HN, std::vector<NormalNode>{NN0,NN02});

	return G;
}

//距離に基づく適応度を計算
std::vector<double> CalcDistanceFitness(DynamicsWorld& World) {
	vector<array<double, 3> >FirstPosition = World.GetFirstAgentsPosition(); //各個体の開始時点での位置
	vector<array<double, 3> >LastPosition = World.GetLastAgentsPosition(); //各個体の終了時点での位置
	vector<double>MusicPosition = World.GetMusicPosition(); //音源の位置

	//std::cout << "FirstAgentsPosition:" << std::endl;
	//for (unsigned int i = 0; i < World.GetCreaturesVectorSize(); i++) {
	//	std::cout << "Creatures[" << i << "]=(" << FirstPosition[i][0] << ", " << FirstPosition[i][1] << ", " << FirstPosition[i][2] << ")" << std::endl;
	//}
	//std::cout << "LastAgentsPosition:" << std::endl;
	//for (unsigned int i = 0; i < World.GetCreaturesVectorSize(); i++) {
	//	std::cout << "Creatures[" << i << "]=(" << LastPosition[i][0] << ", " << LastPosition[i][1] << ", " << LastPosition[i][2] << ")" << std::endl;
	//}

	//各個体に関して適応度を計算する
	/*std::vector<double> Fitness;
	for (unsigned int i = 0; i < Population; i++) {
		Fitness.push_back(Distance3D(FirstPosition[i], LastPosition[i]));
	}*/
	//各個体に関して適応度を計算する
	std::vector<double> Fitness;
	for (unsigned int i = 0; i < Population; i++) {
		if (Distance3D(FirstPosition[i], { MusicPosition[0],MusicPosition[1],MusicPosition[2] }) > Distance3D({ MusicPosition[0],MusicPosition[1],MusicPosition[2] }, LastPosition[i])) {
			Fitness.push_back(Distance3D(FirstPosition[i], { MusicPosition[0],MusicPosition[1],MusicPosition[2] }) - Distance3D({ MusicPosition[0],MusicPosition[1],MusicPosition[2] }, LastPosition[i]));
		}
		else {
			Fitness.push_back(0.0);
		}
	}

	double Sum = SumVector(Fitness); //適応度の合計
	double Average = Sum / static_cast<double>(Fitness.size()); //平均適応度
	//double sum = SumVector(MusicPosition);
	double max = *max_element(Fitness.begin(), Fitness.end());
	std::cout << "Average fitness:" << std::setprecision(20) << Average << std::endl;
	std::cout << "Max fitness:" << std::setprecision(20) << max << std::endl;



	return Fitness;
}

//1世代分のシミュレーション
std::vector<double> Simulation(DrawingWindow& DrawingWindow, GenePool& GP, bool Drawing) {
	//仮想物理空間を生成
	DynamicsWorld World(GP.GetGenotypeArray().GetGenotypeVector());

	//評価時間分物理空間を進める
	while (World.GetSteps() < EvaluationTime) {
		//物理空間を進める
		World.OdeStepsUpdate();

		//描画がオンなら描画する
		if (Drawing) {
			DrawingWindow.Draw(World.GetAgentsGeomIDs(), World.GetSounds(), World.GetAgentsBeamSensorGeomIDs(), World.GetHitBeamSensorValues());
		}
	}

	//適応度を計算
	std::vector<double> Fitness = CalcDistanceFitness(World);

	//動力学世界の終了
	World.FinalizeDynamicsWorld();

	return Fitness;
}

int main(int argc, char *argv[]) {
	//芋虫型生物を個体数分作成
	vector<Genotype> GenotypeVector;
	for (unsigned int i = 0; i < Population; i++) {
		GenotypeVector.push_back(CreateCaterpillar());
	}

	//遺伝子プールの作成
	GenotypeArray GA(GenotypeVector);
	GenePool GP(GA);

	//描画ウィンドウクラスのインスタンスを生成
	DrawingWindow DrawingWindow(argc, argv);

	//全世代数分のシミュレーションを行う
	for (int g = 1; g <= Generation; g++) {
		//最終世代前で停止
		if (g == Generation) {
			system("pause");
		}

		//描画するかどうか
		//最終世代のみ描画する
		bool Drawing = g == Generation ? true : false;
		Drawing = true;
		//1世代分のシミュレーション
		std::cout << "Generation:" << g << std::endl;
		std::vector<double> Fitness = Simulation(DrawingWindow, GP, Drawing);

		//最終世代の遺伝子プールを保存
		if (g == Generation) {
			Serialize(GP, "./LastGeneration.json");
		}

		//遺伝的操作
		GP.GeneticOperation(Fitness);
	}

	//最終世代の遺伝子プールを読み込み
	Deserialize(GP, "./LastGeneration.json");

	//最終世代を再現
	Simulation(DrawingWindow, GP, true);

	system("pause");
	glfwTerminate(); //描画処理を終了
	SingletonFinalizer::finalize(); //シングルトンを終了
}