#include "NormalNeuron.h"

//コンストラクタ
NormalNeuron::NormalNeuron() {
	this->ActivationFunction = Sigmoid; //活性化関数はsigmoid
}

NormalNeuron::NormalNeuron(const std::vector<NeuroConnection> NeuroConnectionVector) {
	this->ActivationFunction = Sigmoid; //活性化関数はsigmoid
	this->NeuroConnectionVector = NeuroConnectionVector; //メンバ変数を初期化
}

//デストラクタ
NormalNeuron::~NormalNeuron() {
}

//突然変異
void NormalNeuron::Mutation(const unsigned int NormalNeuronNumber) {
	//突然変異によってコネクションを1つ削除
	DeleteNeuroConnection();

	//突然変異によってニューロコネクションを1つ追加
	AddNeuroConnection(NormalNeuronNumber);

	//ニューロコネクションに突然変異を加える
	MutateNeuroConnections();
}

//ニューロコネクションのニューロン番号を調整
void NormalNeuron::AdjustNeuroNumber(const unsigned int DeletedNeuroNumber) {
	for (auto& nc : NeuroConnectionVector) {
		int NeuronNumber = nc.GetNeuroNumber(); //ニューロン番号
		if (NeuronNumber > DeletedNeuroNumber) nc.DecreaseNeuronNumber(); //もし削除されたノーマルニューロンより大きいニューロン番号なら1つ減じる
	}
}

//突然変異によってニューロコネクションを1つ追加
void NormalNeuron::AddNeuroConnection(const unsigned int NormalNeuronNumber) {
	//突然変異が発生した場合
	RandomOperation& RO = GetRandomOperation();
	if (RO.IsMutationOccurred()) {
		//存在するエフェクターニューロンとノーマルニューロンの中からランダムに1つ選び削除する
		NeuroConnectionVector.push_back(NeuroConnection(RO.MakeRandomValue(-3, NormalNeuronNumber - 1)));
	}
}

//突然変異によってコネクションを1つ削除
void NormalNeuron::DeleteNeuroConnection() {
	//突然変異が発生してかつニューロコネクションがあった場合
	RandomOperation& RO = GetRandomOperation();
	if (RO.IsMutationOccurred() && !NeuroConnectionVector.empty()) {
		//今あるコネクションの中からランダムに1つ選び削除する
		RemoveVectorElement(NeuroConnectionVector, RO.MakeRandomValue(0, NeuroConnectionVector.size() - 1));
	}
}

//ニューロコネクションに突然変異を加える
void NormalNeuron::MutateNeuroConnections() {
	for (auto& nc : NeuroConnectionVector) {
		nc.Mutation();
	}
}