#include "RandomOperation.h"

//コンストラクタ
RandomOperation::RandomOperation() : MT(RandomSeed) {
}

//デストラクタ
RandomOperation::~RandomOperation() {
}

//任意の範囲の乱数を生成する
int RandomOperation::MakeRandomValue(const int Min, const int Max) {
	std::uniform_int_distribution<> RandomMaker(Min, Max);
	return RandomMaker(MT);
}

double RandomOperation::MakeRandomValue(const double Min, const double Max) {
	std::uniform_real_distribution<> RandomMaker(Min, Max);
	return RandomMaker(MT);
}

//trueまたはfalseをランダムに生成する
bool RandomOperation::MakeRandomBoolValue() {
	return static_cast<bool>(MakeRandomValue(0, 1));
}

//確率的事象が起きたかどうかを返す
bool RandomOperation::IsProbabilityEventOccurred(const double Probability) {
	double RandomValue = MakeRandomValue(0.0, 1.0); //0.0から1.0の間の乱数

	//与えられた確率より乱数が小さい場合
	//しかし確率が1.0の場合には1.0以下とする
	if (RandomValue < Probability || (Probability == 1.0 && RandomValue == 1.0)) {
		return true; //事象が発生した
	}
	return false; //事象は発生しなかった
}

//突然変異が発生したかどうかを返す
bool RandomOperation::IsMutationOccurred() {
	return IsProbabilityEventOccurred(MutationRate);
}

//-1または+1をランダムに生成する
int RandomOperation::MakeRandomPlusMinusOne() {
	if (MakeRandomBoolValue()) {
		return 1;
	}
	else {
		return -1;
	}
}

//スケールした微小値をランダムに生成する
double RandomOperation::MakeInfinitesimalValue(const double Scale) {
	return MakeRandomValue(-0.001 * Scale, 0.001 * Scale);
}

//突然変異を加える
int RandomOperation::AddMutation(int Value, const int Min, const int Max) {
	//突然変異が起きたら元の値をランダムにプラスマイナス1する
	if (IsMutationOccurred()) {
		Value += MakeRandomPlusMinusOne();
	}

	//値を範囲内に収める
	Value = AdjustValue(Value, Min, Max);

	return Value;
}


int RandomOperation::AddMutation(int Value, const int Min) {
	//突然変異が起きたら元の値をランダムにプラスマイナス1する
	if (IsMutationOccurred()) {
		Value += MakeRandomPlusMinusOne();
	}

	//値を範囲内に収める
	Value = AdjustValue(Value, Min);

	return Value;
}


double RandomOperation::AddMutation(double Value, const double Min, const double Max) {
	//突然変異が起きたら元の値に最大値-最小値でスケールした微小値を追加
	if (IsMutationOccurred()) {
		Value += MakeInfinitesimalValue(Max - Min);
	}

	//値を範囲内に収める
	Value = AdjustValue(Value, Min, Max);

	return Value;
}
double RandomOperation::AddMutation(double Value, const double Min) {
	//突然変異が起きたら元の値に微小値を追加
	if (IsMutationOccurred()) {
		Value += MakeInfinitesimalValue(1.0);
	}

	//値を範囲内に収める
	Value = AdjustValue(Value, Min);

	return Value;
}

double RandomOperation::AddMutation(double Value) {
	//突然変異が起きたら元の値に微小値を追加
	if (IsMutationOccurred()) {
		Value += MakeInfinitesimalValue(1.0);
	}

	return Value;
}

bool RandomOperation::AddMutation(bool Value) {
	//突然変異が発生したらブール値を反転する
	if (IsMutationOccurred()) {
		Value = FlipBoolValue(Value);
	}

	return Value;
}

//ルーレット
unsigned int RandomOperation::Roulette(const std::vector<double> Weights) {
	//重みの合計を出す
	double Sum = SumVector(Weights);

	//もし合計が0.0の場合ランダムに選ぶ
	if (Sum == 0.0) return MakeRandomValue(0, Weights.size() - 1);

	//ルーレットを回す
	double r; //乱数
	do {
		r = MakeRandomValue(0.0, Sum);
	} while (r == Sum); //rは0以上Sum未満

	//ルーレットがどこに当たったか調べる
	double Tmp = 0.0;
	for (unsigned int i = 0; i < Weights.size(); i++) {
		Tmp += Weights[i];
		if (Tmp > r) {
			return i;
		}
	}
}

//乱数処理クラスをシングルトンとしてインスタンス化して返す
RandomOperation& GetRandomOperation() {
	return singleton<RandomOperation>::get_instance();
}