#pragma once
#include "BaseNeuron.h"
#include "NeuroConnection.h"

/*! @brief 通常のニューロン
@details ベースニューロンを継承する。\n
ニューラルネットワークの主な構成要素である。\n
コネクションによってノーマルニューロンまたはエフェクターニューロンに接続する。\n
活性化関数にはシグモイド関数を用いる。
*/
class NormalNeuron : public BaseNeuron {
public:
	/*! @brief 通常のコンストラクタ。活性化関数を初期化する。*/
	NormalNeuron();

	/*! @brief 任意のノーマルニューロンを作成するコンストラクタ
	@param[out] NeuroConnectionVector ニューロコネクションの配列
	@details ノーマルニューロンの持つ全ての情報を与えてノーマルニューロンを作成する。
	*/
	NormalNeuron(const std::vector<NeuroConnection> NeuroConnectionVector);

	/*! @brief デストラクタ */
	virtual ~NormalNeuron();

	/*! @brief ニューロコネクションの配列を返す
	@return std::vector<NeuroConnection> NeuroConnectionVector*/
	std::vector<NeuroConnection> GetNeuroConnectionVector() { return NeuroConnectionVector; }

	/*! @brief ノーマルニューロンの持つ各情報に対して突然変異を発生させる
	@param[in] NormalNeuronNumber ニューラルネットワークの持つノーマルニューロンの数*/
	virtual void Mutation(const unsigned int NormalNeuronNumber);

	/*! @brief ニューロコネクションのインデックスを調整する
	@param [in] DeletedNeuroNumber 削除されたノーマルニューロンのインデックス
	@details ノーマルニューロンが削除された際にそれより大きいインデックスを持つ\n
	ニューロコネクションは全てインデックスを１つ前にずらす必要がある。（参照がずれないように）\n
	この関数では調節が必要なニューロコネクションにインデックスを１つ減らす処理を行う。\n
	*/
	void AdjustNeuroNumber(const unsigned int DeletedNeuroNumber);

private:
	/*! @brief 突然変異によってニューロコネクションを1つ追加
	@param[in] NormalNeuronNumber ニューラルネットワークの持つノーマルニューロンの数
	@details 突然変異が発生した場合、ランダムにノーマルニューロンまたは\n
	エフェクターニューロンに接続したニューロコネクションを1つ追加する。
	*/
	void AddNeuroConnection(const unsigned int NormalNeuronNumber);

	/*! @brief 突然変異によってニューロコネクションを1つ削除
	@details 突然変異が発生した場合、今保持しているニューロコネクションからランダムに1つ選び削除する。
	*/
	void DeleteNeuroConnection();

	/*! @brief 保持しているニューロコネクションに突然変異を加える*/
	void MutateNeuroConnections();

	//! ノーマルニューロンの持つニューロコネクションの配列
	std::vector<NeuroConnection> NeuroConnectionVector;

	friend class cereal::access;
	/*! @brief シリアライズ
	@details ノーマルニューロンの持つ全ての情報をシリアライズする関数*/
	template<class Archive>
	void serialize(Archive& archive) {
		archive(CEREAL_NVP(NeuroConnectionVector));
	}

};