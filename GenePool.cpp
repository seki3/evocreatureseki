#include "GenePool.h"

//コンストラクタ
GenePool::GenePool() : GA(MakeInitialPopulation()) {
}

GenePool::GenePool(const GenotypeArray GA) : GA(GA) {
}

//デストラクタ
GenePool::~GenePool() {
}

//遺伝的操作
void GenePool::GeneticOperation(const std::vector<double> Fitness) {
	std::vector<Genotype> NextGenotypeVector; //次世代の遺伝子型ベクトル

	//個体数分処理を行う
	RandomOperation& RO = GetRandomOperation();
	for (unsigned int i = 0; i < Population; i++) {
		if (RO.IsProbabilityEventOccurred(CrossOverRate) && !OnlyNeuralNetworkEvolves) {
			//交叉が発生してかつニューラルネットワークだけが進化しない場合
			unsigned int Parent1 = RO.Roulette(Fitness); //親1
			unsigned int Parent2 = RO.Roulette(Fitness); //親2

			//交叉するポイントを親1のノーマルノードベクトルから選ぶ
			unsigned int CrossPoint = RO.MakeRandomValue(0, GA[Parent1].GetNormalNodeVector().size());

			//親1と親2を交叉して新しい遺伝子型を作る
			NextGenotypeVector.push_back(GA.CrossOver(Parent1, Parent2, CrossPoint));
		}
		else {
			//交叉が無い場合ルーレット選択で親を選びそのままコピー
			NextGenotypeVector.push_back(GA[RO.Roulette(Fitness)]);
		}
	}

	//遺伝子型配列を更新
	GA = GenotypeArray(NextGenotypeVector);

	//突然変異
	GA.Mutation();
}

//初期個体群を作成
GenotypeArray GenePool::MakeInitialPopulation() {
	std::vector<Genotype> GenotypeVector; //遺伝子型ベクトル

	//個体数分遺伝子型を作成
	for (unsigned int i = 0; i < Population; i++) {
		GenotypeVector.push_back(Genotype());
	}

	return GenotypeArray(GenotypeVector);
}